﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dapper.Contrib.Extensions;

namespace Airmaster.Quotation.BusinessEntities
{
    public class ProductEntity
    {
        [Key]
        public int ProductID { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public Decimal CostPerUnit { get; set; }
        public DateTime CreationDate { get; set; }
        public int CreatedBy { get; set; }
        public DateTime UpdationDate { get; set; }
        public int UpdatedBy { get; set; }
    }
}
