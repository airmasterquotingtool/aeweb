﻿Project Guidelines

•	POCO objects to be used only as Data Transfer Objects between the consumer and the services
•	Classes should be named (suffixed) as “Entity” to distinguish business entity objects from other objects
