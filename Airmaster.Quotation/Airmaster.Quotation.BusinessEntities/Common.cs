﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dapper.Contrib.Extensions;

namespace Airmaster.Quotation.BusinessEntities.Common
{
    public class CodeDataEntity
    {
        public int CodeID { get; set; }
        public int CodeTypeID { get; set; }
        public string CodeName { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
    }

    public class DivisionDropDown
    {
        public int DivisionID {  get; set; }
        public string State {  get; set; }
        public string Divisions {  get; set; }
        public float DefaultMargin {  get; set; }
    }

    public class EquipmentDropDown
    {
        public int EquipmentID {  get; set; }
        public string LocationID {  get; set; }
        public string Code { get; set; }
        public string EquipmentType {  get; set; }
    }

    public class LabourClassDropDown
    {
        public int LabourClassID { get; set; }
        public string Code {  get; set; }
        public string Description {  get; set; }
        public string Rate {  get; set; }
        public string State {  get; set; }
    }

    public class VendorDropDown
    {
        public int VendorID { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string State { get; set; }
    }

    public class ContractorDropDown
    {
        public int ContractorID { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string State { get; set; }
    }

    public class TechnicianDropDown
    {
        public int TechnicianID { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
    }

    public class CustomerDropDown
    {
        public int CustomerID { set; get; }
        public string Code { set; get; }
        public string Name { set; get; }
    }

    public class LocationDropDown
    {
        public int LocationID { set; get; }
        public string Code { set; get; }
        public string Name { set; get; }
        public string State { set; get; }
    }

    public class ContactDropDown
    {
        public int ContactID { set; get; }
        public string FirstName { set; get; }
        public string LastName { set; get; }
        public string Email { set; get; }
    }

    public class ProductDropDown
    {
        public int ProductID { set; get; }
        public string Code { set; get; }
        public string Name { set; get; }
        public decimal CostPerUnit { set; get; }
    }
}
