﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dapper.Contrib.Extensions;

namespace Airmaster.Quotation.BusinessEntities
{
    public class CustomerEntity
    {
        [Key]
        public int? CustomerID { set; get; }
        public string Code { set; get; }
        public string Name { set; get; }
        public string AddressLine1 { set; get; }
        public string AddressLine2 { set; get; }
        public string AddressLine3 { set; get; }
        public string City { set; get; }
        public string StateCd { set; get; }
        public string Zip { set; get; }
        public int CountryCd { set; get; }
        public int CreatedBy { set; get; }
        public DateTime CreationDate { set; get; }
        public string UpdatedBy { set; get; }
        public DateTime UpdationDate { set; get; }
    }
}
