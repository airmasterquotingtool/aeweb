﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Airmaster.Quotation.BusinessEntities
{
    // Model Interface for Models that support CRUD Operation
    public interface IModelBase
    {
        DateTime CreationDate { get; set; }
        int CreatedBy { get; set; }
        DateTime UpdationDate { get; set; }
        int UpdatedBy { get; set; }
    }

    public abstract class ModelBase
    {
        public DateTime CreationDate { get; set; }
        public int CreatedBy { get; set; }
        public DateTime UpdationDate { get; set; }
        public int UpdatedBy { get; set; }
    }
}