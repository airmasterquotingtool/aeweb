﻿using AirMaster.Business.Models.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AirMaster.Quotation.Business.Common
{
   public class Vendor:ModelBase
    {
        public string Code { set; get; }
        public string Name { set; get; }
        public string State { set; get; }
    }
}
