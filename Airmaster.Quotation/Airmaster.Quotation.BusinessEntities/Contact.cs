﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dapper.Contrib.Extensions;

namespace Airmaster.Quotation.BusinessEntities
{
    public class ContactEntity
    {
        [Key]
        public int? ContactID { set; get; }
        public int LocationID { set; get; }
        public string FirstName { set; get; }
        public string LastName { set; get; }
        public string JobTitle { set; get; }
        public string Description { set; get; }
        public string Email { set; get; }
        public string MobilePhone { set; get; }
        public string BusinessPhone { set; get; }
        public int CreatedBy { set; get; }
        public DateTime CreationDate { set; get; }
        public string UpdatedBy { set; get; }
        public DateTime UpdationDate { set; get; }
    }
}
