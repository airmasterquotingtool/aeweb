﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Dapper.Contrib.Extensions;

namespace Airmaster.Quotation.BusinessEntities
{
    [Table("QTtQuote")]
    public class QuotationEntity 
    {
        [Key]
        public int? QuoteID { get; set; }
        public string QuoteReference { get; set; }
        public string Subject { get; set; }
        public string Description { get; set; }
        public int StatusCD { get; set; }
        public int Revision { get; set; }
        public int AccountManagerID { get; set; }
        public int CustomerID { get; set; }
        public int LocationID { get; set; }
        public int ContactID { get; set; }
        public int WorkStateCd { get; set; }
        public int DivisionID { get; set; }
        public decimal Margin { get; set; }
        public int PriorityCd { get; set; }
        public int QuoteTypeCd { get; set; }
        public string WorkRequestNo { get; set; }
        public int MemoSubmittedByID { get; set; }
        public int MemoTypeCd { get; set; }
        public DateTime EstimatedCloseDate { get; set; }
        public int ProbabilityCd { get; set; }
        public int StageCd { get; set; }
        public string Introduction { get; set; }
        public string SOW { get; set; }
        public string Exclusions { get; set; }
        public DateTime CreationDate { get; set; }
        public int CreatedBy { get; set; }
        public DateTime? UpdationDate { get; set; }
        public int? UpdatedBy { get; set; }
    }

    [Table("QTtQuoteSection")]
    public class QuotationSectonEntity
    {
        [Key]
        public int? QuoteSectionID { get; set; }
        public int QuoteID { get; set; }
        public string Title { get; set; }
        public int Preferred { get; set; }
        public int LocationID { get; set; }
        public string Equipment { get; set; }
        public string Description { get; set; }
        public DateTime CreationDate { get; set; }
        public int CreatedBy { get; set; }
        public DateTime? UpdationDate { get; set; }
        public int UpdatedBy { get; set; }
    }

    [Table("QTtQuoteLabour")]
    public class QuotationLabourEntity
    {
        [Key]
        public int? QuoteLabourID { set; get; }
        public int QuoteSectionID { set; get; }
        public string Class { set; get; }
        public Decimal CostPerUnit { set; get; }
        public Decimal Quantity { set; get; }
        public Decimal Margin { set; get; }
        public string Description { set; get; }
        public DateTime CreationDate { set; get; }
        public int CreatedBy { set; get; }
        public DateTime? UpdationDate { set; get; }
        public int UpdatedBy { set; get; }
    }

    [Table("QTtQuoteMaterial")]
    public class QuotationMaterialEntity
    {
        [Key]
        public int? QuoteMaterialID { set; get; }
        public int QuoteSectionID { set; get; }
        public string Product { set; get; }
        public Decimal CostPerUnit { set; get; }
        public Decimal Quantity { set; get; }
        public Decimal Margin { set; get; }
        public string Creditor { set; get; }
        public DateTime CreationDate { set; get; }
        public int CreatedBy { set; get; }
        public DateTime? UpdationDate { set; get; }
        public int UpdatedBy { set; get; }
    }

    [Table("QTtQuoteEquipment")]
    public class QuotationEquipmentEntity
    {
        [Key]
        public int? QuoteEquipmentID { set; get; }
        public int QuoteSectionID { set; get; }
        public string Description { set; get; }
        public Decimal CostPerUnit { set; get; }
        public Decimal Quantity { set; get; }
        public Decimal Margin { set; get; }
        public string Reference { set; get; }
        public string Creditor { set; get; }
        public DateTime CreationDate { set; get; }
        public int CreatedBy { set; get; }
        public DateTime? UpdationDate { set; get; }
        public int UpdatedBy { set; get; }
    }

    [Table("QTtQuoteContractor")]
    public class QuotationContractorEntity
    {
        [Key]
        public int? QuoteContractorID { set; get; }
        public int QuoteSectionID { set; get; }
        public string Description { set; get; }
        public Decimal CostPerUnit { set; get; }
        public Decimal Quantity { set; get; }
        public Decimal Margin { set; get; }
        public string Reference { set; get; }
        public string Creditor { set; get; }
        public DateTime CreationDate { set; get; }
        public int CreatedBy { set; get; }
        public DateTime? UpdationDate { set; get; }
        public int UpdatedBy { set; get; }
    }

    [Table("QTtQuoteOther")]
    public class QuotationOtherEntity
    {
        [Key]
        public int? QuoteOtherID { set; get; }
        public int QuoteSectionID { set; get; }
        public Decimal CostPerUnit { set; get; }
        public Decimal Quantity { set; get; }
        public Decimal Margin { set; get; }
        public string Description { set; get; }
        public string Reference { set; get; }
        public string Creditor { set; get; }
        public DateTime CreationDate { set; get; }
        public int CreatedBy { set; get; }
        public DateTime? UpdationDate { set; get; }
        public int UpdatedBy { set; get; }
    }

    [Table("QTtQuoteDocument")]
    public class QuotationDocumentEntity
    {
        [Key]
        public int? QuoteDocumentID { set; get; }
        public int QuoteID { set; get; }
        public string Title { set; get; }
        public string Description { set; get; }
        public string Document { set; get; }
    }

    public class QuotationList
    {
        public string QuoteReference { get; set; }
        public string Subject { get; set; }
        public string Customer { get; set; }
        public string Location { get; set; }
        public string Revision { get; set; }
        public string Probability { get; set; }
        public string QuoteType { get; set; }
        public string WorkState { get; set; }
        public string Contact { get; set; }
        public string CreationDate { get; set; }
        public string Status { get; set; }
        public DateTime EstimatedCloseDate { get; set; }

    }

}