"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Quote = (function () {
    function Quote() {
        this.EstimatedCloseDate = new Date();
        this.Sections = [];
        this.Pricing = new QuotePrice(0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
    }
    return Quote;
}());
exports.Quote = Quote;
var VendorDropDown = (function () {
    function VendorDropDown() {
    }
    return VendorDropDown;
}());
exports.VendorDropDown = VendorDropDown;
var CodeName;
(function (CodeName) {
    CodeName[CodeName["UserRole"] = 1] = "UserRole";
    CodeName[CodeName["QuoteStatus"] = 2] = "QuoteStatus";
    CodeName[CodeName["QuoteLostStatus"] = 3] = "QuoteLostStatus";
    CodeName[CodeName["QuotePriority"] = 4] = "QuotePriority";
    CodeName[CodeName["MemoType"] = 5] = "MemoType";
    CodeName[CodeName["QuoteProbability"] = 6] = "QuoteProbability";
    CodeName[CodeName["QuoteStage"] = 7] = "QuoteStage";
    CodeName[CodeName["QuoteType"] = 8] = "QuoteType";
    CodeName[CodeName["State"] = 9] = "State";
})(CodeName = exports.CodeName || (exports.CodeName = {}));
var CodeData = (function () {
    function CodeData() {
    }
    return CodeData;
}());
exports.CodeData = CodeData;
var Customer = (function () {
    function Customer() {
    }
    return Customer;
}());
exports.Customer = Customer;
var TechnicianEntity = (function () {
    function TechnicianEntity() {
    }
    return TechnicianEntity;
}());
exports.TechnicianEntity = TechnicianEntity;
var ProductDropDown = (function () {
    function ProductDropDown() {
    }
    return ProductDropDown;
}());
exports.ProductDropDown = ProductDropDown;
var ContactEntity = (function () {
    function ContactEntity() {
    }
    return ContactEntity;
}());
exports.ContactEntity = ContactEntity;
var Division = (function () {
    function Division() {
    }
    return Division;
}());
exports.Division = Division;
var EquipmentDropDown = (function () {
    function EquipmentDropDown() {
    }
    return EquipmentDropDown;
}());
exports.EquipmentDropDown = EquipmentDropDown;
var Location = (function () {
    function Location() {
    }
    return Location;
}());
exports.Location = Location;
var QuotePricing = (function () {
    function QuotePricing() {
    }
    return QuotePricing;
}());
exports.QuotePricing = QuotePricing;
var LabourClass = (function () {
    function LabourClass() {
    }
    return LabourClass;
}());
exports.LabourClass = LabourClass;
var QuotePrice = (function () {
    function QuotePrice(PriceLabours, PriceMaterials, PriceAdditional, PriceTotal_Ex_GST, WeightedTotal, GST_Total, PriceTotal_Inc_GST, MarginActual_Percentage, MarginActual, MarkUpActual) {
        this.PriceLabours = PriceLabours;
        this.PriceMaterials = PriceMaterials;
        this.PriceAdditional = PriceAdditional;
        this.PriceTotal_Ex_GST = PriceTotal_Ex_GST;
        this.WeightedTotal = WeightedTotal;
        this.GST_Total = GST_Total;
        this.PriceTotal_Inc_GST = PriceTotal_Inc_GST;
        this.MarginActual_Percentage = MarginActual_Percentage;
        this.MarginActual = MarginActual;
        this.MarkUpActual = MarkUpActual;
    }
    return QuotePrice;
}());
exports.QuotePrice = QuotePrice;
//# sourceMappingURL=quote.js.map