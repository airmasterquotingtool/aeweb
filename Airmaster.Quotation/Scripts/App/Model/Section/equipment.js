"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Equipment = (function () {
    function Equipment(SEQ, Description, CostPerUnit, EquipmentQuantity, CostSubTotal, Margin, PricePerUnit, PriceSubTotal, Reference, Creditor) {
        this.SEQ = SEQ;
        this.Description = Description;
        this.CostPerUnit = CostPerUnit;
        this.EquipmentQuantity = EquipmentQuantity;
        this.CostSubTotal = CostSubTotal;
        this.Margin = Margin;
        this.PricePerUnit = PricePerUnit;
        this.PriceSubTotal = PriceSubTotal;
        this.Reference = Reference;
        this.Creditor = Creditor;
    }
    return Equipment;
}());
exports.Equipment = Equipment;
//# sourceMappingURL=equipment.js.map