"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var QuotationMaterialEntity = (function () {
    function QuotationMaterialEntity(QuoteMaterialID, QuoteSectionID, Product, CostPerUnit, Quantity, CostSubTotal, Margin, PricePerUnit, PriceSubTotal, Creditor) {
        this.QuoteMaterialID = QuoteMaterialID;
        this.QuoteSectionID = QuoteSectionID;
        this.Product = Product;
        this.CostPerUnit = CostPerUnit;
        this.Quantity = Quantity;
        this.CostSubTotal = CostSubTotal;
        this.Margin = Margin;
        this.PricePerUnit = PricePerUnit;
        this.PriceSubTotal = PriceSubTotal;
        this.Creditor = Creditor;
    }
    return QuotationMaterialEntity;
}());
exports.QuotationMaterialEntity = QuotationMaterialEntity;
//# sourceMappingURL=material.js.map