"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Other = (function () {
    function Other(SEQ, Description, CostPerUnit, OthersQuantity, CostSubTotal, Margin, PricePerUnit, PriceSubTotal, Reference, Creditor) {
        if (CostPerUnit === void 0) { CostPerUnit = 0; }
        if (OthersQuantity === void 0) { OthersQuantity = 0; }
        if (CostSubTotal === void 0) { CostSubTotal = 0; }
        if (Margin === void 0) { Margin = 0; }
        if (PricePerUnit === void 0) { PricePerUnit = 0; }
        if (PriceSubTotal === void 0) { PriceSubTotal = 0; }
        this.SEQ = SEQ;
        this.Description = Description;
        this.CostPerUnit = CostPerUnit;
        this.OthersQuantity = OthersQuantity;
        this.CostSubTotal = CostSubTotal;
        this.Margin = Margin;
        this.PricePerUnit = PricePerUnit;
        this.PriceSubTotal = PriceSubTotal;
        this.Reference = Reference;
        this.Creditor = Creditor;
    }
    return Other;
}());
exports.Other = Other;
//# sourceMappingURL=others.js.map