"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
var http_interceptor_service_js_1 = require("ng-http-loader/http-interceptor.service.js");
var Observable_1 = require("rxjs/Observable");
require("rxjs/add/operator/catch");
require("rxjs/add/operator/map");
var CoreDataService = (function () {
    function CoreDataService(http) {
        this.http = http;
        this.baseUrl = "http://localhost:49759";
        this.getCustomersUrl = this.baseUrl + '/api/Common/GetCustomers';
        this.getDivisionUrl = this.baseUrl + '/api/Common/GetDivisions'; // URL to web API
        this.getContactsUrl = this.baseUrl + '/api/Common/GetContacts'; // URL to web API
        this.getProductsUrl = this.baseUrl + '/api/Common/GetProducts';
        this.getLocationUrl = this.baseUrl + '/api/Common/GetLocations';
        this.getCoreDataUrl = this.baseUrl + '/api/Common/GetCodeData'; //URL to get most common form field data
        this.getLabourClassUrl = this.baseUrl + "/api/Common/GetLabourClass";
        this.getTechniciansUrl = this.baseUrl + "/api/Common/GetTechnicians";
        this.getQuotationListUrl = this.baseUrl + "/api/Quote/GetQuotes";
        this.saveQuoteInfoUrl = this.baseUrl + "/api/Quote/SaveQuote";
        this.getQuoteTypeUrl = this.baseUrl + "/api/Quote/SaveQuote";
        this.getEquipmentsUrl = this.baseUrl + "/api/Common/GetEquipments";
        this.saveQuoteSectionInfoUrl = this.baseUrl + "/api/Quote/SaveSection";
        this.getCreditorsUrl = this.baseUrl + "/api/Common/GetVendors";
        this.getQuoteDetailUrl = this.baseUrl + "/api/Quote/GetQuoteDetail";
        this.getSectionsUrl = this.baseUrl + "/api/Quote/GetSections";
        this.saveLabourUrl = this.baseUrl + "/api/Quote/SaveLabour";
        this.getLaboursUrl = this.baseUrl + "/api/Quote/GetLabours";
        this.getMaterialsUrl = this.baseUrl + "/api/Quote/GetMaterials";
        this.saveMaterialUrl = this.baseUrl + "/api/Quote/SaveMaterial";
    }
    CoreDataService.prototype.getQuotationList = function (accountManagerID) {
        var params = new http_1.URLSearchParams();
        params.set('PageSize', '10');
        params.set('PageNumber', '1');
        params.set('SortBy', 'Name');
        params.set('OrderBy', 'ASC');
        return this.http.get(this.getQuotationListUrl, { search: params })
            .map(this.extractData)
            .catch(this.handleError);
    };
    CoreDataService.prototype.getQuoteDetail = function (quoteID) {
        var params = new http_1.URLSearchParams();
        params.set('QuoteID', quoteID);
        return this.http.get(this.getQuoteDetailUrl, { search: params })
            .map(this.extractData)
            .catch(this.handleError);
    };
    CoreDataService.prototype.getSections = function (quoteID) {
        var params = new http_1.URLSearchParams();
        params.set('QuoteID', quoteID);
        return this.http.get(this.getSectionsUrl, { search: params })
            .map(this.extractData)
            .catch(this.handleError);
    };
    CoreDataService.prototype.getLabours = function (quoteSectionID) {
        var params = new http_1.URLSearchParams();
        params.set('SectionID', quoteSectionID);
        return this.http.get(this.getLaboursUrl, { search: params })
            .map(this.extractData)
            .catch(this.handleError);
    };
    CoreDataService.prototype.getMaterials = function (quoteSectionID) {
        var params = new http_1.URLSearchParams();
        params.set('SectionID', quoteSectionID);
        return this.http.get(this.getMaterialsUrl, { search: params })
            .map(this.extractData)
            .catch(this.handleError);
    };
    CoreDataService.prototype.getCreditors = function (state) {
        var params = new http_1.URLSearchParams();
        params.set('state', state);
        return this.http.get(this.getCreditorsUrl, { search: params })
            .map(this.extractData)
            .catch(this.handleError);
    };
    CoreDataService.prototype.saveQuoteGeneralInfo = function (quote) {
        return this.http.post(this.saveQuoteInfoUrl, quote)
            .map(this.extractData)
            .catch(this.handleError);
    };
    CoreDataService.prototype.saveQuoteSectionInfo = function (section) {
        return this.http.post(this.saveQuoteSectionInfoUrl, section)
            .map(this.extractData)
            .catch(this.handleError);
    };
    CoreDataService.prototype.saveLabour = function (Labour) {
        return this.http.post(this.saveLabourUrl, Labour)
            .map(this.extractData)
            .catch(this.handleError);
    };
    CoreDataService.prototype.saveMaterial = function (Material) {
        return this.http.post(this.saveMaterialUrl, Material)
            .map(this.extractData)
            .catch(this.handleError);
    };
    CoreDataService.prototype.getCoreData = function () {
        return this.http.get(this.getCoreDataUrl)
            .map(this.extractData)
            .catch(this.handleError);
    };
    CoreDataService.prototype.getCustomers = function () {
        return this.http.get(this.getCustomersUrl)
            .map(this.extractData)
            .catch(this.handleError);
    };
    CoreDataService.prototype.getDivisions = function (state) {
        var params = new http_1.URLSearchParams();
        params.set('state', state);
        return this.http.get(this.getDivisionUrl, { search: params })
            .map(this.extractData)
            .catch(this.handleError);
    };
    CoreDataService.prototype.getContacts = function (locationID) {
        var params = new http_1.URLSearchParams();
        params.set('LocationID', locationID);
        return this.http.get(this.getContactsUrl, { search: params })
            .map(this.extractData)
            .catch(this.handleError);
    };
    CoreDataService.prototype.getProducts = function () {
        return this.http.get(this.getProductsUrl)
            .map(this.extractData)
            .catch(this.handleError);
    };
    CoreDataService.prototype.getLocation = function () {
        return this.http.get(this.getLocationUrl)
            .map(this.extractData)
            .catch(this.handleError);
    };
    CoreDataService.prototype.getLocationByCustomerID = function (customerID) {
        var params = new http_1.URLSearchParams();
        params.set('CustomerID', customerID);
        return this.http.get(this.getLocationUrl, { search: params })
            .map(this.extractData)
            .catch(this.handleError);
    };
    CoreDataService.prototype.getTechnicians = function () {
        return this.http.get(this.getTechniciansUrl)
            .map(this.extractData)
            .catch(this.handleError);
    };
    CoreDataService.prototype.getLabourClasses = function (state) {
        var params = new http_1.URLSearchParams();
        params.set('state', state);
        return this.http.get(this.getLabourClassUrl, { search: params })
            .map(this.extractData)
            .catch(this.handleError);
    };
    CoreDataService.prototype.getEquipments = function (locationID) {
        var params = new http_1.URLSearchParams();
        params.set('LocationID', locationID);
        return this.http.get(this.getEquipmentsUrl, { search: params })
            .map(this.extractData)
            .catch(this.handleError);
    };
    //common applicable  operations on http response object
    CoreDataService.prototype.extractData = function (res) {
        var body = res.json();
        return body || {};
    };
    CoreDataService.prototype.handleError = function (error) {
        //  will use a remote logging infrastructure 
        var errMsg;
        if (error instanceof http_1.Response) {
            var body = error.json() || '';
            var err = body.error || JSON.stringify(body);
            errMsg = error.status + " - " + (error.statusText || '') + " " + err;
        }
        else {
            errMsg = error.message ? error.message : error.toString();
        }
        console.error(errMsg);
        return Observable_1.Observable.throw(errMsg);
    };
    return CoreDataService;
}());
CoreDataService = tslib_1.__decorate([
    core_1.Injectable(),
    tslib_1.__metadata("design:paramtypes", [http_interceptor_service_js_1.HttpInterceptorService])
], CoreDataService);
exports.CoreDataService = CoreDataService;
//# sourceMappingURL=core-data.service.js.map