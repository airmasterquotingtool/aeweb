"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var core_1 = require("@angular/core");
var platform_browser_1 = require("@angular/platform-browser");
var animations_1 = require("@angular/platform-browser/animations");
var router_1 = require("@angular/router"); //routes packages live here
var ng_bootstrap_1 = require("@ng-bootstrap/ng-bootstrap"); //bootstrap components live here
var forms_1 = require("@angular/forms"); //<-- NgModel lives here
var http_1 = require("@angular/http");
var ng_http_loader_module_1 = require("ng-http-loader/ng-http-loader.module");
var kendo_angular_buttons_1 = require("@progress/kendo-angular-buttons");
var kendo_angular_grid_1 = require("@progress/kendo-angular-grid");
var kendo_angular_dateinputs_1 = require("@progress/kendo-angular-dateinputs");
var kendo_angular_inputs_1 = require("@progress/kendo-angular-inputs");
var ng2_ckeditor_1 = require("ng2-ckeditor");
var app_component_1 = require("./Components/app.component");
var menu_bar_component_1 = require("./Components/menu-bar.component");
var create_quote_component_1 = require("./Components/create-quote.component");
var quote_general_info_input_component_1 = require("./Components/quote-general-info-input.component");
var quote_section_component_1 = require("./Components/quote-section.component");
var section_general_info_input_component_1 = require("./Components/section-general-info-input.component");
var kendo_angular_dialog_1 = require("@progress/kendo-angular-dialog");
var forms_2 = require("@angular/forms");
var kendo_angular_dropdowns_1 = require("@progress/kendo-angular-dropdowns");
var labour_sub_section_editor_component_1 = require("./Components/labour-sub-section-editor.component");
var material_sub_section_editor_component_1 = require("./Components/material-sub-section-editor.component");
var equipment_sub_section_editor_1 = require("./Components/equipment-sub-section-editor");
var subcontractor_sub_section_editor_1 = require("./Components/subcontractor-sub-section-editor");
var others_sub_section_editor_1 = require("./Components/others-sub-section-editor");
var quote_list_component_1 = require("./Components/quote-list.component");
var quote_summary_component_1 = require("./Components/quote-summary.component");
var shared_data_service_1 = require("./Service/shared-data.service");
var core_data_service_1 = require("./Service/core-data.service");
var AppModule = (function () {
    function AppModule() {
    }
    return AppModule;
}());
AppModule = tslib_1.__decorate([
    core_1.NgModule({
        imports: [platform_browser_1.BrowserModule, forms_1.FormsModule, http_1.HttpModule, ng_http_loader_module_1.NgHttpLoaderModule, http_1.JsonpModule, animations_1.BrowserAnimationsModule,
            kendo_angular_buttons_1.ButtonsModule, kendo_angular_grid_1.GridModule, kendo_angular_dialog_1.DialogModule, kendo_angular_inputs_1.InputsModule,
            kendo_angular_dateinputs_1.DateInputsModule,
            ng2_ckeditor_1.CKEditorModule, kendo_angular_dropdowns_1.DropDownListModule, kendo_angular_dropdowns_1.AutoCompleteModule, kendo_angular_dropdowns_1.ComboBoxModule, forms_2.ReactiveFormsModule,
            ng_bootstrap_1.NgbModule.forRoot(), router_1.RouterModule.forRoot([
                {
                    path: 'CreateQuote',
                    component: create_quote_component_1.CreateQuoteComponent
                },
                {
                    path: 'ListOfQuotes',
                    component: quote_list_component_1.QuoteList
                }
            ])],
        declarations: [
            app_component_1.AppComponent,
            create_quote_component_1.CreateQuoteComponent,
            quote_general_info_input_component_1.QuoteGeneralInfoComponent,
            quote_section_component_1.QuoteSectionComponent,
            section_general_info_input_component_1.SectionGeneralInfo,
            menu_bar_component_1.MenuBarComponent,
            quote_list_component_1.QuoteList, quote_summary_component_1.QuoteSummary,
            labour_sub_section_editor_component_1.LabourSubSectionEditor,
            subcontractor_sub_section_editor_1.SubContractorSubSectionEditor,
            others_sub_section_editor_1.OtherSubSectionEditor,
            material_sub_section_editor_component_1.MaterialSubSectionEditor,
            equipment_sub_section_editor_1.EquipmentSubSectionEditor,
        ],
        providers: [
            shared_data_service_1.SharedDataService, core_data_service_1.CoreDataService
        ],
        bootstrap: [app_component_1.AppComponent, menu_bar_component_1.MenuBarComponent]
    })
], AppModule);
exports.AppModule = AppModule;
//# sourceMappingURL=app.module.js.map