"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var core_1 = require("@angular/core");
var forms_1 = require("@angular/forms");
var kendo_angular_grid_1 = require("@progress/kendo-angular-grid");
var core_data_service_1 = require("./../Service/core-data.service");
var equipment_1 = require("./../Model/Section/equipment");
var section_1 = require("./../Model/section");
var shared_data_service_1 = require("./../Service/shared-data.service");
var formGroup = function (dataItem) { return new forms_1.FormGroup({
    'SEQ': new forms_1.FormControl(dataItem.SEQ, forms_1.Validators.required),
    'Description': new forms_1.FormControl(dataItem.EquipmentClass, forms_1.Validators.required),
    'CostPerUnit': new forms_1.FormControl(dataItem.CostPerUnit, forms_1.Validators.required),
    'EquipmentQuantity': new forms_1.FormControl(dataItem.EquipmentQuantity, forms_1.Validators.required),
    'CostSubTotal': new forms_1.FormControl(dataItem.CostSubTotal, forms_1.Validators.required),
    'Margin': new forms_1.FormControl(dataItem.Margin, forms_1.Validators.required),
    'PricePerUnit': new forms_1.FormControl(dataItem.PricePerUnit, forms_1.Validators.required),
    'PriceSubTotal': new forms_1.FormControl(dataItem.PriceSubTotal, forms_1.Validators.required),
    'Reference': new forms_1.FormControl(dataItem.Description),
    'Creditor': new forms_1.FormControl(dataItem.Description)
}); };
var EquipmentSubSectionEditor = (function () {
    function EquipmentSubSectionEditor(coreDataService, sharedDataService) {
        this.coreDataService = coreDataService;
        this.sharedDataService = sharedDataService;
        this.formGroup = formGroup(new equipment_1.Equipment());
        this.seq = 1.00;
        this.gridState = {
            sort: [],
            skip: 0,
            take: 10
        };
        this.creditors = [];
        this.notify = new core_1.EventEmitter();
    }
    EquipmentSubSectionEditor.prototype.ngOnInit = function () {
        //this.selectedSectionEqp = new Section();
        this.creditors = this.sharedDataService.getCreditors();
    };
    //set creditor value based on dropdown template
    //public CreditorList(id: any): any {
    //    if (id != undefined) {
    //        return this.creditors.find(x => x.Code === id);
    //    }
    //}
    EquipmentSubSectionEditor.prototype.editHandler = function (_a) {
        var _this = this;
        var sender = _a.sender, rowIndex = _a.rowIndex, dataItem = _a.dataItem;
        this.closeEditor(sender);
        this.formGroup = formGroup(dataItem);
        this.formGroup.valueChanges.subscribe(function (data) {
            _this.formGroup.value.CostSubTotal = _this.formGroup.value.CostPerUnit * data.EquipmentQuantity;
            _this.formGroup.value.PricePerUnit = _this.formGroup.value.CostPerUnit;
            if (_this.formGroup.value.Margin > 0) {
                _this.formGroup.value.PricePerUnit = parseInt(_this.formGroup.value.CostPerUnit) + ((_this.formGroup.value.CostPerUnit * _this.formGroup.value.Margin) / 100);
            }
            _this.formGroup.value.PriceSubTotal = _this.formGroup.value.PricePerUnit * _this.formGroup.value.EquipmentQuantity;
        });
        this.editedRowIndex = rowIndex;
        sender.editRow(rowIndex, this.formGroup);
    };
    EquipmentSubSectionEditor.prototype.cancelHandler = function (_a) {
        var sender = _a.sender, rowIndex = _a.rowIndex;
        this.closeEditor(sender, rowIndex);
    };
    EquipmentSubSectionEditor.prototype.closeEditor = function (grid, rowIndex) {
        if (rowIndex === void 0) { rowIndex = this.editedRowIndex; }
        grid.closeRow(rowIndex);
        this.editedRowIndex = undefined;
        this.formGroup = undefined;
    };
    //save labour   
    EquipmentSubSectionEditor.prototype.saveHandler = function (_a) {
        var _this = this;
        var sender = _a.sender, rowIndex = _a.rowIndex, formGroup = _a.formGroup, isNew = _a.isNew;
        var labour = formGroup.value;
        //this.editService.save(product, isNew);
        if (isNew) {
            this.selectedSectionEqp.Equipments.push(this.formGroup.value);
        }
        else {
            Object.assign(this.selectedSectionEqp.Equipments.find(function (_a) {
                var SEQ = _a.SEQ;
                return SEQ === _this.formGroup.value.SEQ;
            }), this.formGroup.value);
        }
        //sum of labour costs for selected section
        this.selectedSectionEqp.PriceAdditional += this.selectedSectionEqp.Equipments.map(function (i) { return i.PriceSubTotal; }).reduce(function (a, b) { return a + b; }, 0);
        sender.closeRow(rowIndex);
        this.notify.emit(this.selectedSectionEqp);
    };
    //remove a labour list value from grid
    EquipmentSubSectionEditor.prototype.removeHandler = function (_a) {
        var dataItem = _a.dataItem;
        this.selectedSectionEqp.Equipments.splice(this.selectedSectionEqp.Equipments.indexOf(dataItem), 1);
    };
    //add new labour class row in grid
    EquipmentSubSectionEditor.prototype.addHandler = function (_a) {
        var _this = this;
        var sender = _a.sender;
        this.closeEditor(sender);
        if (this.selectedSectionEqp.Equipments == undefined) {
            this.selectedSectionEqp.Equipments = [];
        }
        if (this.selectedSectionEqp.Equipments.length > 0) {
            this.seq = this.selectedSectionEqp.Equipments[this.selectedSectionEqp.Equipments.length - 1].SEQ + 1;
        }
        this.formGroup = formGroup(new equipment_1.Equipment(this.seq, "", 0, 0, 0, this.selectedSectionEqp.MarginActualPercentage, 0, 0, ""));
        this.formGroup.valueChanges.subscribe(function (data) {
            _this.formGroup.value.CostSubTotal = _this.formGroup.value.CostPerUnit * data.EquipmentQuantity;
            _this.formGroup.value.PricePerUnit = _this.formGroup.value.CostPerUnit;
            if (_this.formGroup.value.Margin > 0) {
                _this.formGroup.value.PricePerUnit = parseInt(_this.formGroup.value.CostPerUnit) + ((_this.formGroup.value.CostPerUnit * _this.formGroup.value.Margin) / 100);
            }
            _this.formGroup.value.PriceSubTotal = _this.formGroup.value.PricePerUnit * _this.formGroup.value.EquipmentQuantity;
        });
        sender.addRow(this.formGroup);
    };
    return EquipmentSubSectionEditor;
}());
tslib_1.__decorate([
    core_1.Input(),
    tslib_1.__metadata("design:type", section_1.Section)
], EquipmentSubSectionEditor.prototype, "selectedSectionEqp", void 0);
tslib_1.__decorate([
    core_1.Input(),
    tslib_1.__metadata("design:type", String)
], EquipmentSubSectionEditor.prototype, "type", void 0);
tslib_1.__decorate([
    core_1.Output(),
    tslib_1.__metadata("design:type", core_1.EventEmitter)
], EquipmentSubSectionEditor.prototype, "notify", void 0);
tslib_1.__decorate([
    core_1.ViewChild(kendo_angular_grid_1.GridComponent),
    tslib_1.__metadata("design:type", kendo_angular_grid_1.GridComponent)
], EquipmentSubSectionEditor.prototype, "grid", void 0);
EquipmentSubSectionEditor = tslib_1.__decorate([
    core_1.Component({
        selector: 'equipment-sub-section-grid',
        template: "\n             <kendo-grid\n                      [data]=\"selectedSectionEqp.Equipments \"\n                      [height]=\"300\"\n                      [pageSize]=\"gridState.take\" [skip]=\"gridState.skip\" [sort]=\"gridState.sort\"\n                      [pageable]=\"true\" [sortable]=\"true\"\n                      (dataStateChange)=\"onStateChange($event)\"\n                      (edit)=\"editHandler($event)\" (cancel)=\"cancelHandler($event)\"\n                      (save)=\"saveHandler($event)\" (remove)=\"removeHandler($event)\"\n                      (add)=\"addHandler($event)\"\n            >\n            <ng-template kendoGridToolbarTemplate>\n                <button kendoGridAddCommand>Add new</button>\n            </ng-template>    \n           <kendo-grid-column field=\"SEQ\" title=\"SEQ\" [editable]=\"false\" width=\"50\"></kendo-grid-column>\n            <kendo-grid-column field=\"Description\" title=\"Description\"></kendo-grid-column>\n            <kendo-grid-column field=\"CostPerUnit\" editor=\"numeric\" title=\"Cost Per Unit\"></kendo-grid-column>\n            <kendo-grid-column field=\"EquipmentQuantity\" editor=\"numeric\" title=\"Qty\"></kendo-grid-column>\n            <kendo-grid-column field=\"CostSubTotal\" [editable]=\"false\" title=\"Cost Sub Total\"></kendo-grid-column>\n            <kendo-grid-column field=\"Margin\" title=\"Margin(%)\"  editor=\"numeric\" ></kendo-grid-column>\n            <kendo-grid-column field=\"PricePerUnit\" [editable]=\"false\" title=\"Price Per Unit\"></kendo-grid-column>\n            <kendo-grid-column field=\"PriceSubTotal\" [editable]=\"false\" title=\"Price Sub Total\"></kendo-grid-column>\n            <kendo-grid-column field=\"Reference\" title=\"Reference\"></kendo-grid-column>\n             <kendo-grid-column field=\"Creditor\" width=\"120\" title=\"Creditor\">\n                    <ng-template kendoGridCellTemplate let-dataItem>\n                    {{dataItem.Creditor}} \n                    </ng-template>\n                    <ng-template kendoGridEditTemplate \n                    let-dataItem=\"dataItem\"\n                    let-formGroup=\"formGroup\">\n                    <kendo-combobox [data]=\"creditors\"\n                    [allowCustom]=\"true\"\n                   \n                    [popupSettings]=\"{ width: 'auto' }\"\n                    [valuePrimitive]=\"true\"\n                    [formControl]=\"formGroup.get('Creditor')\">\n                    </kendo-combobox>\n                    </ng-template>\n               </kendo-grid-column>\n            <kendo-grid-command-column title=\"command\" width=\"220\">      \n                <ng-template kendoGridCellTemplate let-isNew=\"isNew\">\n                    <button kendoGridEditCommand class=\"k-primary\">Edit</button>\n                    <button kendoGridRemoveCommand>Remove</button>\n                    <button kendoGridSaveCommand [disabled]=\"formGroup?.invalid\">{{ isNew ? 'Add' : 'Update' }}</button>\n                    <button kendoGridCancelCommand>{{ isNew ? 'Discard changes' : 'Cancel' }}</button>\n                </ng-template>\n            </kendo-grid-command-column>\n        </kendo-grid>"
    }),
    tslib_1.__metadata("design:paramtypes", [core_data_service_1.CoreDataService, shared_data_service_1.SharedDataService])
], EquipmentSubSectionEditor);
exports.EquipmentSubSectionEditor = EquipmentSubSectionEditor;
//# sourceMappingURL=equipment-sub-section-editor.js.map