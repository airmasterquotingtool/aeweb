"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var core_1 = require("@angular/core");
var section_1 = require("./../Model/section");
var core_data_service_1 = require("./../Service/core-data.service");
var shared_data_service_1 = require("./../Service/shared-data.service");
var SectionGeneralInfo = (function () {
    function SectionGeneralInfo(coreDataService, sharedDataService) {
        this.coreDataService = coreDataService;
        this.sharedDataService = sharedDataService;
        this.equipments = [];
        this.errorMessage = '';
        this.notify = new core_1.EventEmitter();
        this.txtEditorConfig = { width: "90%", toolbar: 'Basic', disableNativeSpellChecker: true, scayt_autoStartup: true, filebrowserImageUploadUrl: '~/NodeModules' };
    }
    SectionGeneralInfo.prototype.ngOnInit = function () {
        if (this.section != undefined) {
            this.equipments = this.sharedDataService.getEquipments();
        }
    };
    SectionGeneralInfo.prototype.onClick = function () {
        this.saveSection(this.section);
        this.notify.emit(this.section);
    };
    SectionGeneralInfo.prototype.saveSection = function (section) {
        var _this = this;
        this.coreDataService.saveQuoteSectionInfo(section)
            .subscribe(function (QuoteSectionID) {
            _this.section.QuoteSectionID = QuoteSectionID;
        }, function (error) { return _this.errorMessage = error; });
    };
    SectionGeneralInfo.prototype.onChange = function () {
    };
    SectionGeneralInfo.prototype.onFocus = function () {
    };
    SectionGeneralInfo.prototype.onReady = function () {
    };
    SectionGeneralInfo.prototype.onBlur = function () {
    };
    return SectionGeneralInfo;
}());
tslib_1.__decorate([
    core_1.Input(),
    tslib_1.__metadata("design:type", section_1.Section)
], SectionGeneralInfo.prototype, "section", void 0);
tslib_1.__decorate([
    core_1.Output(),
    tslib_1.__metadata("design:type", core_1.EventEmitter)
], SectionGeneralInfo.prototype, "notify", void 0);
SectionGeneralInfo = tslib_1.__decorate([
    core_1.Component({
        selector: 'section-general-info',
        templateUrl: './../../App/Templates/section-general-info-input.component.html',
    }),
    tslib_1.__metadata("design:paramtypes", [core_data_service_1.CoreDataService, shared_data_service_1.SharedDataService])
], SectionGeneralInfo);
exports.SectionGeneralInfo = SectionGeneralInfo;
var LabourColDefs = ["SEQ", "Labour Class", "Cost Per Unit", "Labour", "Cost Sub Total",
    "Margin %", "Price Per Unit", "Price Sub Total", "Description"];
//# sourceMappingURL=section-general-info-input.component.js.map