"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var core_1 = require("@angular/core");
var forms_1 = require("@angular/forms");
var kendo_angular_grid_1 = require("@progress/kendo-angular-grid");
var core_data_service_1 = require("./../Service/core-data.service");
var shared_data_service_1 = require("./../Service/shared-data.service");
var others_1 = require("./../Model/Section/others");
var section_1 = require("./../Model/section");
var formGroup = function (dataItem) { return new forms_1.FormGroup({
    'SEQ': new forms_1.FormControl(dataItem.SEQ, forms_1.Validators.required),
    'Description': new forms_1.FormControl(dataItem.Description, forms_1.Validators.required),
    'CostPerUnit': new forms_1.FormControl(dataItem.CostPerUnit, forms_1.Validators.required),
    'OtherQuantity': new forms_1.FormControl(dataItem.OtherQuantity, forms_1.Validators.required),
    'CostSubTotal': new forms_1.FormControl(dataItem.CostSubTotal, forms_1.Validators.required),
    'Margin': new forms_1.FormControl(dataItem.Margin, forms_1.Validators.required),
    'PricePerUnit': new forms_1.FormControl(dataItem.PricePerUnit, forms_1.Validators.required),
    'PriceSubTotal': new forms_1.FormControl(dataItem.PriceSubTotal, forms_1.Validators.required),
    'Reference': new forms_1.FormControl(dataItem.Reference),
    'Creditor': new forms_1.FormControl(dataItem.Creditor)
}); };
var OtherSubSectionEditor = (function () {
    function OtherSubSectionEditor(coreDataService, sharedDataService) {
        this.coreDataService = coreDataService;
        this.sharedDataService = sharedDataService;
        this.formGroup = formGroup(new others_1.Other());
        this.creditors = [];
        this.seq = 1.00;
        this.gridState = {
            sort: [],
            skip: 0,
            take: 10
        };
        this.notify = new core_1.EventEmitter();
    }
    OtherSubSectionEditor.prototype.ngOnInit = function () {
        //this.selectedSectionOthers = new Section();
        this.creditors = this.sharedDataService.getCreditors();
    };
    OtherSubSectionEditor.prototype.editHandler = function (_a) {
        var _this = this;
        var sender = _a.sender, rowIndex = _a.rowIndex, dataItem = _a.dataItem;
        this.closeEditor(sender);
        this.formGroup = formGroup(dataItem);
        this.formGroup.valueChanges.subscribe(function (data) {
            _this.formGroup.value.CostSubTotal = _this.formGroup.value.CostPerUnit * data.OtherQuantity;
            _this.formGroup.value.PricePerUnit = _this.formGroup.value.CostPerUnit;
            if (_this.formGroup.value.Margin > 0) {
                _this.formGroup.value.PricePerUnit = parseInt(_this.formGroup.value.CostPerUnit) + ((_this.formGroup.value.CostPerUnit * _this.formGroup.value.Margin) / 100);
            }
            _this.formGroup.value.PriceSubTotal = _this.formGroup.value.PricePerUnit * _this.formGroup.value.OtherQuantity;
        });
        this.editedRowIndex = rowIndex;
        sender.editRow(rowIndex, this.formGroup);
    };
    OtherSubSectionEditor.prototype.cancelHandler = function (_a) {
        var sender = _a.sender, rowIndex = _a.rowIndex;
        this.closeEditor(sender, rowIndex);
    };
    OtherSubSectionEditor.prototype.closeEditor = function (grid, rowIndex) {
        if (rowIndex === void 0) { rowIndex = this.editedRowIndex; }
        grid.closeRow(rowIndex);
        this.editedRowIndex = undefined;
        this.formGroup = undefined;
    };
    //save labour   
    OtherSubSectionEditor.prototype.saveHandler = function (_a) {
        var _this = this;
        var sender = _a.sender, rowIndex = _a.rowIndex, formGroup = _a.formGroup, isNew = _a.isNew;
        var labour = formGroup.value;
        //this.editService.save(product, isNew);
        if (isNew) {
            this.selectedSectionOthers.Others.push(this.formGroup.value);
        }
        else {
            Object.assign(this.selectedSectionOthers.Others.find(function (_a) {
                var SEQ = _a.SEQ;
                return SEQ === _this.formGroup.value.SEQ;
            }), this.formGroup.value);
        }
        //sum of labour costs for selected section
        this.selectedSectionOthers.PriceAdditional += this.selectedSectionOthers.Others.map(function (i) { return i.PriceSubTotal; }).reduce(function (a, b) { return a + b; }, 0);
        sender.closeRow(rowIndex);
        this.notify.emit(this.selectedSectionOthers);
    };
    //remove a labour list value from grid
    OtherSubSectionEditor.prototype.removeHandler = function (_a) {
        var dataItem = _a.dataItem;
        this.selectedSectionOthers.Others.splice(this.selectedSectionOthers.Others.indexOf(dataItem), 1);
    };
    //add new labour class row in grid
    OtherSubSectionEditor.prototype.addHandler = function (_a) {
        var _this = this;
        var sender = _a.sender;
        this.closeEditor(sender);
        if (this.selectedSectionOthers.Others == undefined) {
            this.selectedSectionOthers.Others = [];
        }
        if (this.selectedSectionOthers.Others.length > 0) {
            this.seq = this.selectedSectionOthers.Others[this.selectedSectionOthers.Others.length - 1].SEQ + 1;
        }
        this.formGroup = formGroup(new others_1.Other(this.seq, "", 0, 0, 0, this.selectedSectionOthers.MarginActualPercentage, 0, 0, ""));
        this.formGroup.valueChanges.subscribe(function (data) {
            _this.formGroup.value.CostSubTotal = _this.formGroup.value.CostPerUnit * data.OtherQuantity;
            _this.formGroup.value.PricePerUnit = _this.formGroup.value.CostPerUnit;
            if (_this.formGroup.value.Margin > 0) {
                _this.formGroup.value.PricePerUnit = parseInt(_this.formGroup.value.CostPerUnit) + ((_this.formGroup.value.CostPerUnit * _this.formGroup.value.Margin) / 100);
            }
            _this.formGroup.value.PriceSubTotal = _this.formGroup.value.PricePerUnit * _this.formGroup.value.OtherQuantity;
        });
        sender.addRow(this.formGroup);
    };
    return OtherSubSectionEditor;
}());
tslib_1.__decorate([
    core_1.Input(),
    tslib_1.__metadata("design:type", section_1.Section)
], OtherSubSectionEditor.prototype, "selectedSectionOthers", void 0);
tslib_1.__decorate([
    core_1.Input(),
    tslib_1.__metadata("design:type", String)
], OtherSubSectionEditor.prototype, "type", void 0);
tslib_1.__decorate([
    core_1.Output(),
    tslib_1.__metadata("design:type", core_1.EventEmitter)
], OtherSubSectionEditor.prototype, "notify", void 0);
tslib_1.__decorate([
    core_1.ViewChild(kendo_angular_grid_1.GridComponent),
    tslib_1.__metadata("design:type", kendo_angular_grid_1.GridComponent)
], OtherSubSectionEditor.prototype, "grid", void 0);
OtherSubSectionEditor = tslib_1.__decorate([
    core_1.Component({
        selector: 'others-sub-section-grid',
        template: "\n           <kendo-grid\n                      [data]=\"selectedSectionOthers.Others \"\n                      [height]=\"300\"\n                      [pageSize]=\"gridState.take\" [skip]=\"gridState.skip\" [sort]=\"gridState.sort\"\n                      [pageable]=\"true\" [sortable]=\"true\"\n                      (dataStateChange)=\"onStateChange($event)\"\n                      (edit)=\"editHandler($event)\" (cancel)=\"cancelHandler($event)\"\n                      (save)=\"saveHandler($event)\" (remove)=\"removeHandler($event)\"\n                      (add)=\"addHandler($event)\"\n            >\n            <ng-template kendoGridToolbarTemplate>\n                <button kendoGridAddCommand>Add new</button>\n            </ng-template>    \n            <kendo-grid-column field=\"SEQ\" title=\"SEQ\" [editable]=\"false\"  width=\"50\"></kendo-grid-column>\n            <kendo-grid-column field=\"Description\" title=\"Description\"></kendo-grid-column>\n            <kendo-grid-column field=\"CostPerUnit\" editor=\"numeric\" title=\"Cost Per Unit\"></kendo-grid-column>\n            <kendo-grid-column field=\"OtherQuantity\" editor=\"numeric\" title=\"Qty\"></kendo-grid-column>\n            <kendo-grid-column field=\"CostSubTotal\" [editable]=\"false\"  title=\"Cost Sub Total\"></kendo-grid-column>\n            <kendo-grid-column field=\"Margin\" title=\"Margin(%)\" editor=\"numeric\"></kendo-grid-column>\n            <kendo-grid-column field=\"PricePerUnit\"[editable]=\"false\"  title=\"Price Per Unit\"></kendo-grid-column>\n            <kendo-grid-column field=\"PriceSubTotal\" [editable]=\"false\"  title=\"Price Sub Total\"></kendo-grid-column>\n            <kendo-grid-column field=\"Reference\" title=\"Reference\"></kendo-grid-column>\n            <kendo-grid-column field=\"Creditor\" width=\"120\" title=\"Creditor\">\n                    <ng-template kendoGridCellTemplate let-dataItem>\n                    {{dataItem.Creditor}} \n                    </ng-template>\n                    <ng-template kendoGridEditTemplate \n                    let-dataItem=\"dataItem\"\n                    let-formGroup=\"formGroup\">\n                    <kendo-combobox [data]=\"creditors\"\n                    [allowCustom]=\"true\"\n                   \n                    [popupSettings]=\"{ width: 'auto' }\"\n                    [valuePrimitive]=\"true\"\n                    [formControl]=\"formGroup.get('Creditor')\">\n                    </kendo-combobox>\n                    </ng-template>\n                    </kendo-grid-column>\n            <kendo-grid-command-column title=\"command\" width=\"220\">\n            <ng-template kendoGridCellTemplate let-isNew=\"isNew\">\n                <button kendoGridEditCommand class=\"k-primary\">Edit</button>\n                <button kendoGridRemoveCommand>Remove</button>\n                <button kendoGridSaveCommand [disabled]=\"formGroup?.invalid\">{{ isNew ? 'Add' : 'Update' }}</button>\n                <button kendoGridCancelCommand>{{ isNew ? 'Discard changes' : 'Cancel' }}</button>\n            </ng-template>\n        </kendo-grid-command-column>\n        </kendo-grid> "
    }),
    tslib_1.__metadata("design:paramtypes", [core_data_service_1.CoreDataService, shared_data_service_1.SharedDataService])
], OtherSubSectionEditor);
exports.OtherSubSectionEditor = OtherSubSectionEditor;
//# sourceMappingURL=others-sub-section-editor.js.map