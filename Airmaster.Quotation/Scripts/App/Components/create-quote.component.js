"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var core_1 = require("@angular/core");
var shared_data_service_1 = require("./../Service/shared-data.service");
var quote_1 = require("./../Model/quote");
var core_data_service_1 = require("./../Service/core-data.service");
var CreateQuoteComponent = (function () {
    function CreateQuoteComponent(coreDataService, sharedDataServcie) {
        this.coreDataService = coreDataService;
        this.sharedDataServcie = sharedDataServcie;
        this.quote = new quote_1.Quote();
        var sharedQuote = this.sharedDataServcie.getData();
        if (sharedQuote != undefined) {
            this.quote = sharedQuote;
            this.quoteReference = this.quote.WorkStateCd + this.quote.CreatedBy + '000' + this.quote.QuoteID;
        }
    }
    CreateQuoteComponent.prototype.onNotify = function (newQuote) {
        this.quote = newQuote;
        if (this.quote.Pricing != undefined) {
            this.quote.Pricing.PriceLabours = this.quote.Sections.map(function (i) { return i.PriceLabour; }).reduce(function (a, b) { return a + b; }, 0);
            this.quote.Pricing.PriceMaterials = this.quote.Sections.map(function (i) { return i.PriceMaterial; }).reduce(function (a, b) { return a + b; }, 0);
            this.quote.Pricing.PriceAdditional = this.quote.Sections.map(function (i) { return i.PriceAdditional; }).reduce(function (a, b) { return a + b; }, 0);
            this.quote.Pricing.PriceTotal_Ex_GST = this.quote.Pricing.PriceLabours
                + this.quote.Pricing.PriceMaterials
                + this.quote.Pricing.PriceAdditional;
            this.quote.Pricing.WeightedTotal = this.quote.Pricing.PriceTotal_Ex_GST - this.quote.Pricing.PriceMaterials;
        }
        else {
            this.quote.Pricing = new quote_1.QuotePrice(0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
        }
    };
    CreateQuoteComponent.prototype.ngOnInit = function () {
        if (this.quote.QuoteID != undefined) {
            this.GetSections(this.quote.QuoteID);
        }
    };
    CreateQuoteComponent.prototype.GetSections = function (quoteID) {
        var _this = this;
        this.coreDataService.getSections(quoteID)
            .subscribe(function (Sections) {
            _this.quote.Sections = Sections;
            if (_this.quote.Sections.length > 0) {
                for (var _i = 0, _a = _this.quote.Sections; _i < _a.length; _i++) {
                    var section = _a[_i];
                    _this.GetLabours(section.QuoteSectionID);
                }
            }
            _this.onNotify(_this.quote);
        }, function (error) { return _this.errorMessage = error; });
    };
    CreateQuoteComponent.prototype.GetLabours = function (QuoteSectionID) {
        var _this = this;
        this.coreDataService.getLabours(QuoteSectionID)
            .subscribe(function (labours) {
            _this.quote.Sections.filter(function (obj) { return obj.QuoteSectionID == QuoteSectionID; })[0].Labours = labours;
        }, function (error) { return _this.errorMessage = error; });
    };
    CreateQuoteComponent.prototype.onChange = function () {
    };
    CreateQuoteComponent.prototype.onFocus = function () {
    };
    CreateQuoteComponent.prototype.onReady = function () {
    };
    CreateQuoteComponent.prototype.onBlur = function () {
    };
    return CreateQuoteComponent;
}());
CreateQuoteComponent = tslib_1.__decorate([
    core_1.Component({
        selector: 'create-quote',
        templateUrl: './../../App/Templates/create-quote.component.html',
    }),
    tslib_1.__metadata("design:paramtypes", [core_data_service_1.CoreDataService, shared_data_service_1.SharedDataService])
], CreateQuoteComponent);
exports.CreateQuoteComponent = CreateQuoteComponent;
//# sourceMappingURL=create-quote.component.js.map