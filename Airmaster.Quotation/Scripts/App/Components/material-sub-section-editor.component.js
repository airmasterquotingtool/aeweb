"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var core_1 = require("@angular/core");
var forms_1 = require("@angular/forms");
var kendo_angular_grid_1 = require("@progress/kendo-angular-grid");
var core_data_service_1 = require("./../Service/core-data.service");
var material_1 = require("./../Model/Section/material");
var section_1 = require("./../Model/section");
var shared_data_service_1 = require("./../Service/shared-data.service");
var formGroup = function (dataItem) { return new forms_1.FormGroup({
    'QuoteMaterialID': new forms_1.FormControl(dataItem.QuoteMaterialID, forms_1.Validators.required),
    'Product': new forms_1.FormControl(dataItem.Product, forms_1.Validators.required),
    'CostPerUnit': new forms_1.FormControl(dataItem.CostPerUnit, forms_1.Validators.compose([forms_1.Validators.required])),
    'Quantity': new forms_1.FormControl(dataItem.Quantity, forms_1.Validators.compose([forms_1.Validators.required])),
    'CostSubTotal': new forms_1.FormControl(dataItem.CostSubTotal, forms_1.Validators.compose([forms_1.Validators.required])),
    'Margin': new forms_1.FormControl(dataItem.Margin, forms_1.Validators.compose([forms_1.Validators.required])),
    'PricePerUnit': new forms_1.FormControl(dataItem.PricePerUnit, forms_1.Validators.compose([forms_1.Validators.required])),
    'PriceSubTotal': new forms_1.FormControl(dataItem.PriceSubTotal, forms_1.Validators.compose([forms_1.Validators.required])),
    'Creditor': new forms_1.FormControl(dataItem.Creditor)
}); };
var MaterialSubSectionEditor = (function () {
    function MaterialSubSectionEditor(coreDataService, sharedDataService) {
        this.coreDataService = coreDataService;
        this.sharedDataService = sharedDataService;
        this.products = [];
        this.creditors = [];
        this.formGroup = formGroup(new material_1.QuotationMaterialEntity());
        this.seq = 1.00;
        this.gridState = {
            sort: [],
            skip: 0,
            take: 10
        };
        this.notify = new core_1.EventEmitter();
    }
    MaterialSubSectionEditor.prototype.ngOnInit = function () {
        this.getProducts();
        this.creditors = this.sharedDataService.getCreditors();
    };
    MaterialSubSectionEditor.prototype.getProducts = function () {
        var _this = this;
        this.coreDataService.getProducts()
            .subscribe(function (products) {
            _this.products = products;
            _this.sharedDataService.setProductList(_this.products);
        }, function (error) { return _this.errorMessage = error; });
    };
    MaterialSubSectionEditor.prototype.ProductList = function (id) {
        if (id != undefined) {
            return this.products.find(function (x) { return x.ProductID === id; });
        }
    };
    //public CreditorList(id: any): any {
    //    if (id != undefined) {
    //        return this.creditors.find(x => x.Code === id);
    //    }
    //}
    MaterialSubSectionEditor.prototype.editHandler = function (_a) {
        var _this = this;
        var sender = _a.sender, rowIndex = _a.rowIndex, dataItem = _a.dataItem;
        this.closeEditor(sender);
        this.formGroup = formGroup(dataItem);
        this.formGroup.valueChanges.subscribe(function (data) {
            if (data.Product != "") {
                _this.formGroup.value.CostPerUnit = _this.products.find(function (x) { return x.ProductID === _this.formGroup.value.Product; }).CostPerUnit;
            }
            _this.formGroup.value.CostSubTotal = _this.formGroup.value.CostPerUnit * data.Quantity;
            _this.formGroup.value.PricePerUnit = _this.formGroup.value.CostPerUnit;
            if (_this.formGroup.value.Margin > 0) {
                _this.formGroup.value.PricePerUnit = parseInt(_this.formGroup.value.CostPerUnit) + ((_this.formGroup.value.CostPerUnit * _this.formGroup.value.Margin) / 100);
            }
            _this.formGroup.value.PriceSubTotal = _this.formGroup.value.PricePerUnit * _this.formGroup.value.Quantity;
        });
        this.editedRowIndex = rowIndex;
        sender.editRow(rowIndex, this.formGroup);
    };
    MaterialSubSectionEditor.prototype.cancelHandler = function (_a) {
        var sender = _a.sender, rowIndex = _a.rowIndex;
        this.closeEditor(sender, rowIndex);
    };
    MaterialSubSectionEditor.prototype.closeEditor = function (grid, rowIndex) {
        if (rowIndex === void 0) { rowIndex = this.editedRowIndex; }
        grid.closeRow(rowIndex);
        this.editedRowIndex = undefined;
        this.formGroup = undefined;
    };
    //save labour   
    MaterialSubSectionEditor.prototype.saveHandler = function (_a) {
        var _this = this;
        var sender = _a.sender, rowIndex = _a.rowIndex, formGroup = _a.formGroup, isNew = _a.isNew;
        var material = formGroup.value;
        this.saveMaterial(material);
        if (isNew) {
            this.selectedSectionMtrl.Materials.push(this.formGroup.value);
        }
        else {
            Object.assign(this.selectedSectionMtrl.Materials.find(function (_a) {
                var QuoteMaterialID = _a.QuoteMaterialID;
                return QuoteMaterialID === _this.formGroup.value.QuoteMaterialID;
            }), this.formGroup.value);
        }
        //sum of labour costs for selected section
        this.selectedSectionMtrl.PriceMaterial = this.selectedSectionMtrl.Materials.map(function (i) { return i.PriceSubTotal; }).reduce(function (a, b) { return a + b; }, 0);
        sender.closeRow(rowIndex);
        this.notify.emit(this.selectedSectionMtrl);
    };
    MaterialSubSectionEditor.prototype.saveMaterial = function (material) {
        var _this = this;
        this.coreDataService.saveMaterial(material)
            .subscribe(function (seq) {
        }, function (error) { return _this.errorMessage = error; });
    };
    //remove a labour list value from grid
    MaterialSubSectionEditor.prototype.removeHandler = function (_a) {
        var dataItem = _a.dataItem;
        this.selectedSectionMtrl.Materials.splice(this.selectedSectionMtrl.Materials.indexOf(dataItem), 1);
    };
    //add new labour class row in grid
    MaterialSubSectionEditor.prototype.addHandler = function (_a) {
        var _this = this;
        var sender = _a.sender;
        this.closeEditor(sender);
        if (this.selectedSectionMtrl.Materials == undefined) {
            this.selectedSectionMtrl.Materials = [];
        }
        if (this.selectedSectionMtrl.Materials.length > 0) {
            this.seq = this.selectedSectionMtrl.Materials[this.selectedSectionMtrl.Materials.length - 1].QuoteMaterialID + 1;
        }
        this.formGroup = formGroup(new material_1.QuotationMaterialEntity(this.seq, this.selectedSectionMtrl.QuoteSectionID, "", 0, 0, 0, this.selectedSectionMtrl.MarginActualPercentage, 0, 0, ""));
        this.formGroup.value.CostPerUnit = 0;
        this.formGroup.valueChanges.subscribe(function (data) {
            if (data.Product != "") {
                _this.formGroup.value.CostPerUnit = _this.products.find(function (x) { return x.ProductID === _this.formGroup.value.Product; }).CostPerUnit;
            }
            _this.formGroup.value.CostSubTotal = _this.formGroup.value.CostPerUnit * data.Quantity;
            _this.formGroup.value.PricePerUnit = _this.formGroup.value.CostPerUnit;
            if (_this.formGroup.value.Margin > 0) {
                _this.formGroup.value.PricePerUnit = parseInt(_this.formGroup.value.CostPerUnit) + ((_this.formGroup.value.CostPerUnit * _this.formGroup.value.Margin) / 100);
            }
            _this.formGroup.value.PriceSubTotal = _this.formGroup.value.PricePerUnit * _this.formGroup.value.Quantity;
        });
        sender.addRow(this.formGroup);
    };
    return MaterialSubSectionEditor;
}());
tslib_1.__decorate([
    core_1.Input(),
    tslib_1.__metadata("design:type", section_1.Section)
], MaterialSubSectionEditor.prototype, "selectedSectionMtrl", void 0);
tslib_1.__decorate([
    core_1.Input(),
    tslib_1.__metadata("design:type", String)
], MaterialSubSectionEditor.prototype, "type", void 0);
tslib_1.__decorate([
    core_1.Output(),
    tslib_1.__metadata("design:type", core_1.EventEmitter)
], MaterialSubSectionEditor.prototype, "notify", void 0);
tslib_1.__decorate([
    core_1.ViewChild(kendo_angular_grid_1.GridComponent),
    tslib_1.__metadata("design:type", kendo_angular_grid_1.GridComponent)
], MaterialSubSectionEditor.prototype, "grid", void 0);
MaterialSubSectionEditor = tslib_1.__decorate([
    core_1.Component({
        selector: 'material-sub-section-grid',
        template: "\n            <kendo-grid\n                      [data]=\"selectedSectionMtrl.Materials\"\n                      [height]=\"300\"\n                      [pageSize]=\"gridState.take\" [skip]=\"gridState.skip\" [sort]=\"gridState.sort\"\n                      [pageable]=\"true\" [sortable]=\"true\"\n                      (dataStateChange)=\"onStateChange($event)\"\n                      (edit)=\"editHandler($event)\" (cancel)=\"cancelHandler($event)\"\n                      (save)=\"saveHandler($event)\" (remove)=\"removeHandler($event)\"\n                      (add)=\"addHandler($event)\" >\n                    <ng-template kendoGridToolbarTemplate>\n                    <button kendoGridAddCommand>Add new</button>\n                    </ng-template>    \n           \n                    <kendo-grid-column field=\"QuoteMaterialID\" [editable]=\"false\" title=\"SEQ\"  width=\"50\"></kendo-grid-column>\n\n                    <kendo-grid-column field=\"Product\"  title=\"Product\" width=\"200\">\n                    <ng-template kendoGridCellTemplate let-dataItem>\n                    {{ProductList(dataItem.Product)?.Code}} \n                    </ng-template>\n                    <ng-template kendoGridEditTemplate \n                    let-dataItem=\"dataItem\"\n                    let-formGroup=\"formGroup\">\n                    <kendo-dropdownlist \n                    [data]=\"products\"\n                    textField=\"Code\"\n                    valueField=\"ProductID\"\n                    [valuePrimitive]=\"true\"\n                    [formControl]=\"formGroup.get('Product')\">                \n                    </kendo-dropdownlist>\n                    </ng-template>\n                    </kendo-grid-column>           \n                    <kendo-grid-column field=\"CostPerUnit\" editor=\"numeric\" title=\"Cost Per Unit\"></kendo-grid-column>\n                    <kendo-grid-column field=\"Quantity\" editor=\"numeric\" title=\"Qty\"></kendo-grid-column>\n                    <kendo-grid-column field=\"CostSubTotal\"[editable]=\"false\" title=\"Cost Sub Total\"></kendo-grid-column>\n                    <kendo-grid-column field=\"Margin\" title=\"Margin(%)\"  editor=\"numeric\"></kendo-grid-column>\n                    <kendo-grid-column field=\"PricePerUnit\" [editable]=\"false\" title=\"Price Per Unit\"></kendo-grid-column>\n                    <kendo-grid-column field=\"PriceSubTotal\"[editable]=\"false\" title=\"Price Sub Total\"></kendo-grid-column>\n                    <kendo-grid-column field=\"Creditor\" width=\"120\" title=\"Creditor\">\n                    <ng-template kendoGridCellTemplate let-dataItem>\n                    {{dataItem.Creditor}} \n                    </ng-template>\n                    <ng-template kendoGridEditTemplate \n                    let-dataItem=\"dataItem\"\n                    let-formGroup=\"formGroup\">\n                    <kendo-combobox [data]=\"creditors\"\n                    [allowCustom]=\"true\"\n                    [popupSettings]=\"{ width: 'auto' }\"\n                    [valuePrimitive]=\"true\"\n                    [formControl]=\"formGroup.get('Creditor')\">\n                    </kendo-combobox>\n                    </ng-template>\n                    </kendo-grid-column>\n                    <kendo-grid-command-column title=\"command\" width=\"220\">\n                    <ng-template kendoGridCellTemplate let-isNew=\"isNew\">\n                    <button kendoGridEditCommand class=\"k-primary\">Edit</button>\n                    <button kendoGridRemoveCommand>Remove</button>\n                    <button kendoGridSaveCommand [disabled]=\"formGroup?.invalid\">{{ isNew ? 'Add' : 'Update' }}</button>\n                    <button kendoGridCancelCommand>{{ isNew ? 'Discard changes' : 'Cancel' }}</button>\n                    </ng-template>\n                    </kendo-grid-command-column>\n        </kendo-grid>"
    }),
    tslib_1.__metadata("design:paramtypes", [core_data_service_1.CoreDataService, shared_data_service_1.SharedDataService])
], MaterialSubSectionEditor);
exports.MaterialSubSectionEditor = MaterialSubSectionEditor;
//# sourceMappingURL=material-sub-section-editor.component.js.map