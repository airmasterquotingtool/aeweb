﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Airmaster.Quotation.DataModels
{
    public class Quote
    {
        public int QuoteID { get; set; }
        public string QuoteNumber { get; set; }
        public int StatusCD { get; set; }
        public int Revision { get; set; }
        public int AccountManagerID { get; set; }
        public int CustomerID { get; set; }
        public int LocationID { get; set; }
        public int ContactID { get; set; }
        public int WorkStateCd { get; set; }
        public string Division { get; set; }
        public decimal Margin { get; set; }
        public int PriorityCd { get; set; }
        public string WorkRequestNo { get; set; }
        public string MemoSubmittedBy { get; set; }
        public int MemoTypeCd { get; set; }
        public DateTime EstimatedCloseDate { get; set; }
        public int ProbabilityCd { get; set; }
        public int StageCd { get; set; }
        public string Introduction { get; set; }
        public string SOW { get; set; }
        public string Exclusions { get; set; }
    }

    public class QuoteSection
    {
        public int QuoteSectionID { get; set; }
        public string Title { get; set; }
        public bool Preferred { get; set; }
        public int LocationID { get; set; }
        public string EquipmentID { get; set; }
        public string Description { get; set; }
    }

    public class QuoteLabourClass
    {
        public int QuoteLabourID { get; set; }
        public int QuoteSectionID { get; set; }
        public string Class { get; set; }
        public Decimal CostPerUnit { get; set; }
        public Decimal Quantity { get; set; }
        public Decimal Margin { get; set; }
        public string Description { get; set; }
    }


    public class QuoteMaterial
    {
        public int QuoteMaterialID { get; set; }
        public int QuoteSectionID { get; set; }
        public string Product { get; set; }
        public Decimal CostPerUnit { get; set; }
        public Decimal Quantity { get; set; }
        public Decimal Margin { get; set; }
        public string Creditor { get; set; }
    }

    public class QuoteEquipment
    { 
        public int QuoteEquipmentID { get; set; }
        public int QuoteSectionID { get; set; }
        public string Equipment { get; set; }
        public Decimal CostPerUnit { get; set; }
        public Decimal Quantity { get; set; }
        public Decimal Margin { get; set; }
        public string Description { get; set; }
        public string Reference { get; set; }
        public string Creditor { get; set; }
    }

    public class QuoteContractor
    {
        public int QuoteContractorID { get; set; }
        public int QuoteSectionID { get; set; }
        public Decimal CostPerUnit { get; set; }
        public Decimal Quantity { get; set; }
        public Decimal Margin { get; set; }
        public string Description { get; set; }
        public string Reference { get; set; }
        public string Creditor { get; set; }
    }

    public class QuoteOther
    {
        public int QuoteOtherID { get; set; }
        public int QuoteSectionID { get; set; }
        public Decimal CostPerUnit { get; set; }
        public Decimal Quantity { get; set; }
        public Decimal Margin { get; set; }
        public string Description { get; set; }
        public string Reference { get; set; }
        public string Creditor { get; set; }
    }

    public class QuoteDocument
    {
        public int QuoteDocumentID { get; set; }
        public int QuoteID { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string Document { get; set; }
    }

    public class GetQuoteSectonsResponse
    {
        List<QuoteSection> QuoteSections;
    }

    public class GetQuoteLabourClassResponse
    {
        List<QuoteLabourClass> QuoteLabourClass;
    }

    public class GetQuoteMaterialResponse
    {
        List<QuoteLabourClass> QuoteMaterials;
    }

    public class GetQuoteEquipmentResponse
    {
        List<QuoteEquipment> QuoteEquipments;
    }

    public class GetQuoteContractorResponse
    {
        List<QuoteContractor> QuoteContractors;
    }

    public class GetQuoteOtherResponse
    {
        List<QuoteOther> QuoteOthers;
    }

    public class GetQuoteDocumentResponse
    {
        List<QuoteDocument> QuoteDocuments;
    }

    public class AddUpdateQuoteRequest
    {

    }

    public class AddUpdateQuoteSectionRequest
    {
        public int QuoteSectionID { get; set; }
        public int QuoteID { set; get; }
        public string Title { set; get; }
        public bool Preferred { get; set; }
        public int LocationID { get; set; }
        public string EquipmentID { get; set; }
        public string Description { get; set; }
    }

    public class AddUpdateQuoteLabourRequest
    {
        public int QuoteLabourID { set; get; }
        public int QuoteSectionID { set; get; }
        public int ClassID { set; get; }
        public Decimal CostPerUnit { set; get; }
        public Decimal Quantity { set; get; }
        public Decimal Margin { set; get; }
        public string Description { set; get; }
    }

    public class AddUpdateQuoteMaterialRequest
    {
        public int QuoteMaterialID { set; get; }
        public int QuoteSectionID { set; get; }
        public string Product { set; get; }
        public Decimal CostPerUnit { set; get; }
        public Decimal Quantity { set; get; }
        public Decimal Margin { set; get; }
        public string Creditor { set; get; }

    }

    public class AddUpdateQuoteEquipmentRequest
    {
        public int QuoteEquipmentID { set; get; }
        public int QuoteSectionID { set; get; }
        public string Equipment { set; get; }
        public Decimal CostPerUnit { set; get; }
        public Decimal Quantity { set; get; }
        public Decimal Margin { set; get; }
        public string Description { set; get; }
        public string Reference { set; get; }
        public string Creditor { set; get; }
    }

    public class AddUpdateQuoteContractorRequest
    {
        public int QuoteContractorID { set; get; }
        public int QuoteSectionID { set; get; }
        public Decimal CostPerUnit { set; get; }
        public Decimal Quantity { set; get; }
        public Decimal Margin { set; get; }
        public string Description { set; get; }
        public string Reference { set; get; }
        public string Creditor { set; get; }
    }

    public class AddUpdateQuoteOther
    {
        public int QuoteOtherID { set; get; }
        public int QuoteSectionID { set; get; }
        public Decimal CostPerUnit { set; get; }
        public Decimal Quantity { set; get; }
        public Decimal Margin { set; get; }
        public string Description { set; get; }
        public string Reference { set; get; }
        public string Creditor { set; get; }
    }
}