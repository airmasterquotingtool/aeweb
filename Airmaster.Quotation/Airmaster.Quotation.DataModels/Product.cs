﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Airmaster.Quotation.DataModels
{
    public class AddUpdateProductRequest
    {
        public int ProductID { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public Decimal CostPerUnit { get; set; }
    }

    public class GetProductDetailResponse
    {
        public int ProductID { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public Decimal CostPerUnit { get; set; }
    }
   
    public class GetProductDropDownResponse
    {
        public int ProductID { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
    }

    public class GetAllProductsResponse
    {
        public int ProductID { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
    }

    //public class GetProductrDropDown
    //{
    //    List<ProductDropDown> ProductList;
    //}
}