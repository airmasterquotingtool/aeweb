﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Airmaster.Quotation.DataModels
{
    public class AddUpdateLocationRequest
    {
        public int LocationID { set; get; }
        public int CustomerID { set; get; }
        public string Code { set; get; }
        public string Name { set; get; }
        public string AddressLine1 { set; get; }
        public string City { set; get; }
        public string Statecd { set; get; }
        public string Pincocde { set; get; }
    }

    public class GetLocationDetailResponse
    {
        public int LocationID { set; get; }
        public int CustomerID { set; get; }
        public string Code { set; get; }
        public string Name { set; get; }
        public string AddressLine1 { set; get; }
        public string City { set; get; }
        public string Statecd { set; get; }
        public string Pincocde { set; get; }
    }

    public class GetLocationDropDownResponse
    {
        public int LocationID { set; get; }
        public string Code { set; get; }
        public string Name { set; get; }
    }

    public class GetAllLocationsResponse
    {   
        //public List<Location> Locations;
    }

    //public class GetLocationrDropDown
    //{
    //    List<LocationDropDown> LocationList;
    //}

}