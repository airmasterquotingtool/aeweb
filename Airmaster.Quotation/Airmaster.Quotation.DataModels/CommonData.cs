﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Airmaster.Quotation.DataModels
{
    public class CodeData
    {
        public int CodeID { get; set; }
        public int CodeTypeID { get; set; }
        public string CodeName { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
    }

    public class Division
    {
        public int DivisionID { get; set; }
        public string State { get; set; }
        public string Divisions { get; set; }
        public float DefaultMargin { get; set; }
    }

    public class Equipment
    {
        public int EquipmentID { get; set; }
        public string LocationID { get; set; }
        public string Code { get; set; }
        public string EquipmentType { get; set; }
    }

    public class LabourClass
    {
        public int LabourClassID { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public string Rate { get; set; }
        public string State { get; set; }
    }

    public class Vendor
    {
        public int VendorID { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string State { get; set; }
    }

    public class Contractor
    {
        public int ContractorID { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string State { get; set; }
    }

    public class GetCodeDataResponse
    {
        public List<CodeData> CodeData; 
    }

    public class GetDivisionResponse
    {
         public List<Division> Divisions { get; set; }
    }

    public class GetEquipmentResponse
    {
        public List<Equipment> Equipments;
    }

    public class GetLabourClassResponse
    {
        public List<LabourClass> LabourClass;
    }

    public class GetVendorResponse
    {
        public List<Vendor> Vendors;
    }

    public class GetContractorResponse
    {
        public List<Contractor> Contractors;
    }
}