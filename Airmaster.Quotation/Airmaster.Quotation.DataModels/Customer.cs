﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Airmaster.Quotation.DataModels
{

    public class AddUpdateCustomerRequest
    {
        public int CustomerID { set; get; }
        public string Code { set; get; }
        public string Name { set; get; }
        public string AddressLine1 { set; get; }
        public string AddressLine2 { set; get; }
        public string AddressLine3 { set; get; }
        public string City { set; get; }
        public string StateCd { set; get; }
        public string Zip { set; get; }
        public int CountryCd { set; get; }
    }

    public class GetCustomerDetailResponse
    {
        public int CustomerID { set; get; }
        public string Code { set; get; }
        public string Name { set; get; }
        public string AddressLine1 { set; get; }
        public string AddressLine2 { set; get; }
        public string AddressLine3 { set; get; }
        public string City { set; get; }
        public string StateCd { set; get; }
        public string Zip { set; get; }
        public int CountryCd { set; get; }
    }

    public class GetAllCustomersResponse
    {
        public int CustomerID { get; }
        public string Code { get; }
        public string Name { get; }
        public string AddressLine1 { get; }
        public string City { get; }
        public string StateCd { set; get; }
    }

    class GetCustomerDropDownResponse
    {
        public int CustomerID { set; get; }
        public string Code { set; get; }
        public string Name { set; get; }
    }

    //public class GetCustomerDropDownResponse
    //{
    //    List<CustomerDropDown> CustomerList;
    //}
}