﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Threading.Tasks;
using Airmaster.Quotation.BusinessEntities.Common;

namespace Airmaster.Quotation.BusinessServices.Contracts
{
    public interface ICommonData
    {
        IEnumerable<CodeDataEntity> GetCodes();
        IEnumerable<DivisionDropDown> GetDivisions(string state);
        IEnumerable<VendorDropDown> GetVendors(string state);
        IEnumerable<ContractorDropDown> GetContractors(string state);
        IEnumerable<LabourClassDropDown> GetLabourClass(string state);
        IEnumerable<EquipmentDropDown> GetEquipments(int? locationID);
        IEnumerable<TechnicianDropDown> GetTechnicians();
        IEnumerable<CustomerDropDown> GetCustomers();
        IEnumerable<LocationDropDown> GetLocations(int CustomerID);
        IEnumerable<ContactDropDown> GetContacts(int LocationID);
        IEnumerable<ProductDropDown> GetProducts();
    }
}