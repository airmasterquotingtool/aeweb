﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Threading.Tasks;
using Airmaster.Quotation.DataModel.Repository;
using Airmaster.Quotation.BusinessEntities;
using Airmaster.Shared.Infrastructure.DataAccess.DataProvider;

namespace Airmaster.Quotation.BusinessServices
{
    public class LocationManagement
    {
        private GenericRepository<LocationEntity> _LocationRepository;

        public LocationManagement(IDbConnection dbContext)
        {
            _LocationRepository = new GenericRepository<LocationEntity>(dbContext);
        }

        public Task<int?> Add(LocationEntity Location)
        {
            return _LocationRepository.InsertAsync(Location);
        }

        //public Task<int> Update(LocationEntity Location)
        //{
        //    return _LocationRepository.Update(Location);
        //}

        public Task<LocationEntity> GetLocationDetail(int id)
        {
            return _LocationRepository.GetAsync(id);
        }

        //public async Task<LocationEntity> GetAllLocations()
        //{
        //    return await _LocationRepository.GetAsync(id);
        //}
    }
}