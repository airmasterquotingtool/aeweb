﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Threading.Tasks;
using System.Diagnostics;
using Airmaster.Quotation.DataModel.Repository;
using Airmaster.Quotation.BusinessEntities;
using Airmaster.Shared.Infrastructure.DataAccess.DataProvider;

namespace Airmaster.Quotation.BusinessServices
{
    public class QuotationManagement : IDisposable
    {
        private GenericRepository<QuotationEntity> _QuotationRepo;
        private GenericRepository<QuotationSectonEntity> _SectionRepo;
        private GenericRepository<QuotationLabourEntity> _LabourRepo;
        private GenericRepository<QuotationContractorEntity> _ContractorRepo;
        private GenericRepository<QuotationEquipmentEntity> _EquipmentRepo;
        private GenericRepository<QuotationMaterialEntity> _MaterialRepo;
        private GenericRepository<QuotationOtherEntity> _OtherRepo;

        IDbConnection dbContext;

        public QuotationManagement()
        {
            ConnectionFactory factory = new ConnectionFactory("AEADatabase");

            dbContext = factory.GetConnection;

            _QuotationRepo = new GenericRepository<QuotationEntity>(dbContext);
            _SectionRepo = new GenericRepository<QuotationSectonEntity>(dbContext);
            _LabourRepo = new GenericRepository<QuotationLabourEntity>(dbContext);
            _ContractorRepo = new GenericRepository<QuotationContractorEntity>(dbContext);
            _EquipmentRepo = new GenericRepository<QuotationEquipmentEntity>(dbContext);
            _MaterialRepo = new GenericRepository<QuotationMaterialEntity>(dbContext);
            _OtherRepo = new GenericRepository<QuotationOtherEntity>(dbContext);
        }
        
        public int? SaveQuote(QuotationEntity Quote)
        {
            int? id = 0;
            if (Quote.QuoteID == null)
            {
                Quote.CreatedBy = 1;
                Quote.CreationDate = DateTime.Now;
                Quote.UpdatedBy = 1;
                Quote.UpdationDate = DateTime.Now;
                id = _QuotationRepo.Insert(Quote);
            }
            else
            {
                Quote.CreatedBy = 1;
                Quote.CreationDate = DateTime.Now;
                Quote.UpdatedBy = 1;
                Quote.UpdationDate = DateTime.Now;
                id = _QuotationRepo.Update(Quote);
            }
            return id;
        }

        public int? SaveSection(QuotationSectonEntity Section)
        {
            int? id = 0;
            if (Section.QuoteSectionID == null)
            {
                Section.CreatedBy = 1;
                Section.CreationDate = DateTime.Now;
                Section.UpdatedBy = 1;
                Section.UpdationDate = DateTime.Now;
                id = _SectionRepo.Insert(Section);
            }
            else
            {
                Section.CreatedBy = 1;
                Section.CreationDate = DateTime.Now;
                Section.UpdatedBy = 1;
                Section.UpdationDate = DateTime.Now;
                id = _SectionRepo.Update(Section);
            }
            return id;
        }

        public int? SaveLabour(QuotationLabourEntity Labour)
        {
            int? id = 0;
            if (Labour.QuoteLabourID == null)
            {
                Labour.CreatedBy = 1;
                Labour.CreationDate = DateTime.Now;
                Labour.UpdatedBy = 1;
                Labour.UpdationDate = DateTime.Now;
                id = _LabourRepo.Insert(Labour);
            }
            else
            {
                Labour.CreatedBy = 1;
                Labour.CreationDate = DateTime.Now;
                Labour.UpdatedBy = 1;
                Labour.UpdationDate = DateTime.Now;
                id = _LabourRepo.Update(Labour);
            }
            return id;
        }

        public int? SaveMaterial(QuotationMaterialEntity Material)
        {
            int? id = 0;
            if (Material.QuoteMaterialID == null)
            {
                Material.CreatedBy = 1;
                Material.CreationDate = DateTime.Now;
                Material.UpdatedBy = 1;
                Material.UpdationDate = DateTime.Now;
                id = _MaterialRepo.Insert(Material);
            }
            else
            {
                Material.CreatedBy = 1;
                Material.CreationDate = DateTime.Now;
                Material.UpdatedBy = 1;
                Material.UpdationDate = DateTime.Now;
                id = _MaterialRepo.Update(Material);
            }
            return id;
        }

        public int? SaveEquipment(QuotationEquipmentEntity Equipment)
        {
            int? id = 0;
            if (Equipment.QuoteEquipmentID == null)
            {
                Equipment.CreatedBy = 1;
                Equipment.CreationDate = DateTime.Now;
                Equipment.UpdatedBy = 1;
                Equipment.UpdationDate = DateTime.Now;
                id = _EquipmentRepo.Insert(Equipment);
            }
            else
            {
                Equipment.CreatedBy = 1;
                Equipment.CreationDate = DateTime.Now;
                Equipment.UpdatedBy = 1;
                Equipment.UpdationDate = DateTime.Now;
                id = _EquipmentRepo.Update(Equipment);
            }
            return id;
        }

        public int? SaveContractor(QuotationContractorEntity Contractor)
        {
            int? id = 0;
            if (Contractor.QuoteContractorID == null)
            {
                Contractor.CreatedBy = 1;
                Contractor.CreationDate = DateTime.Now;
                Contractor.UpdatedBy = 1;
                Contractor.UpdationDate = DateTime.Now;

                id = _ContractorRepo.Insert(Contractor);
            }
            else
            {
                Contractor.CreatedBy = 1;
                Contractor.CreationDate = DateTime.Now;
                Contractor.UpdatedBy = 1;
                Contractor.UpdationDate = DateTime.Now;

                id = _ContractorRepo.Update(Contractor);
            }

            return id;
        }

        public int? SaveOther(QuotationOtherEntity Other)
        {
            int? id = 0;
            if (Other.QuoteOtherID == null)
            {
                Other.CreatedBy = 1;
                Other.CreationDate = DateTime.Now;
                Other.UpdatedBy = 1;
                Other.UpdationDate = DateTime.Now;

                id = _OtherRepo.Insert(Other);
            }
            else
            {
                Other.CreatedBy = 1;
                Other.CreationDate = DateTime.Now;
                Other.UpdatedBy = 1;
                Other.UpdationDate = DateTime.Now;
                id = _OtherRepo.Update(Other);
            }
            return id;
        }

        public Task<QuotationEntity> GetQuoteDetail(int QuotationID)
        {
            return _QuotationRepo.GetAsync(QuotationID);
        }

        public Task<IEnumerable<QuotationSectonEntity>> GetSections(int QuotationID)
        {
            return  _SectionRepo.GetListAsync("where QuoteID =" + QuotationID);
        }

        public Task<IEnumerable<QuotationLabourEntity>> GetLabours(int QuoteSectionID)
        {
            return  _LabourRepo.GetListAsync("where QuoteSectionID=" + QuoteSectionID);
        }

        public Task<IEnumerable<QuotationEquipmentEntity>> GetEquipments(int QuoteSectionID)
        {
            return  _EquipmentRepo.GetListAsync("where QuoteSectionID=" + QuoteSectionID);
        }

        public Task<IEnumerable<QuotationMaterialEntity>> GetMaterials(int QuoteSectionID)
        {
            return  _MaterialRepo.GetListAsync("where QuoteSectionID=" + QuoteSectionID);
        }

        public Task<IEnumerable<QuotationContractorEntity>> GetContractors(int QuoteSectionID)
        {
            return  _ContractorRepo.GetListAsync("where QuoteSectionID=" + QuoteSectionID);
        }

        public Task<IEnumerable<QuotationOtherEntity>> GetOthers(int QuoteSectionID)
        {
            return  _OtherRepo.GetListAsync("where QuoteSectionID=" + QuoteSectionID);
        }

        public Task<IEnumerable<object>> GetQuotes(int PageSize, int PageNumber, string SortBy, string OrderBy)
        {
            var result =_QuotationRepo.GetGenericPagedListAsync("[QTpGetQuotes]", 1, PageNumber, PageSize, "Location", "ASC");
            return result;            
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    Debug.WriteLine("Quotation managemnet is being disposed");
                    _QuotationRepo.Dispose();
                    _SectionRepo.Dispose();
                    _LabourRepo.Dispose();
                    _ContractorRepo.Dispose();
                    _EquipmentRepo.Dispose();
                    _MaterialRepo.Dispose();
                    _OtherRepo.Dispose();
                    dbContext.Dispose();
    }
}
            this.disposed = true;
        }

        /// <summary>
        /// Dispose method
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}