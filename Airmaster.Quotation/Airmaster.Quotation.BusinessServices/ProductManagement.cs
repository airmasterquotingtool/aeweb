﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Threading.Tasks;
using Airmaster.Quotation.DataModel.Repository;
using Airmaster.Quotation.BusinessEntities;
using Airmaster.Shared.Infrastructure.DataAccess.DataProvider;

namespace Airmaster.Quotation.BusinessServices
{
    public class ProductManagement
    {
        private GenericRepository<ProductEntity> _ProductRepository;

        public ProductManagement(IDbConnection dbContext)
        {
            _ProductRepository = new GenericRepository<ProductEntity>(dbContext);
        }

        public Task<int?> Add(ProductEntity Product)
        {
            return _ProductRepository.InsertAsync(Product);
        }

        //public Task<int> Update(ProductEntity Product)
        //{
        //    return _ProductRepository.Update(Product);
        //}

        public Task<ProductEntity> GetProductDetail(int id)
        {
            return _ProductRepository.GetAsync(id);
        }

        //public async Task<ProductEntity> GetProducts()
        //{
        //    return await _ProductRepository.GetListAsync();
        //}
    }
}