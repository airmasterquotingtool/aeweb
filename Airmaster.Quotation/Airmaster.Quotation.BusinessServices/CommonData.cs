﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Web;
using System.Threading.Tasks;
using System.Diagnostics;
using AutoMapper;
using Airmaster.Quotation.DataModel.Repository;
using Airmaster.Quotation.BusinessEntities.Common;
using Airmaster.Quotation.BusinessServices.Contracts;
using Airmaster.Shared.Infrastructure.DataAccess.DataProvider;

namespace Airmaster.Quotation.BusinessServices
{
    public class CommonData :  ICommonData, IDisposable
    {
        internal CommonDataRepository _commonRepository;

        public CommonData()
        {
            ConnectionFactory factory = new ConnectionFactory("AEADatabase");
            _commonRepository = new CommonDataRepository(factory.GetConnection);
        }

        public IEnumerable<CodeDataEntity> GetCodes()
        {
            return _commonRepository.GetCodes();
        }

        public IEnumerable<DivisionDropDown> GetDivisions(string state)
        {
            return _commonRepository.GetDivisions(state);
        }

        public IEnumerable<VendorDropDown> GetVendors(string state)
        {
            return _commonRepository.GetVendors(state);
        }

        public IEnumerable<ContractorDropDown> GetContractors(string state)
        {
            return _commonRepository.GetContractors(state);
        }

        public IEnumerable<LabourClassDropDown> GetLabourClass(string state)
        {
            return _commonRepository.GetLabourClass(state);
        }

        public IEnumerable<EquipmentDropDown> GetEquipments(int? locationID)
        {
            return _commonRepository.GetEquipments(locationID);
        }

        public IEnumerable<TechnicianDropDown> GetTechnicians()
        {
            return _commonRepository.GetTechnicians();
        }

        public IEnumerable<CustomerDropDown> GetCustomers()
        {
            return _commonRepository.GetCustomers();
        }

        public IEnumerable<LocationDropDown> GetLocations(int CustomerID)
        {
            return _commonRepository.GetLocations(CustomerID);
        }

        public IEnumerable<ContactDropDown> GetContacts(int LocationID)
        {
            return _commonRepository.GetContacts(LocationID);
        }

        public IEnumerable<ProductDropDown> GetProducts()
        {
            return _commonRepository.GetProducts();
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    Debug.WriteLine("Common data is being disposed");
                    _commonRepository.Dispose();
                }
            }
            this.disposed = true;
        }

        /// <summary>
        /// Dispose method
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}