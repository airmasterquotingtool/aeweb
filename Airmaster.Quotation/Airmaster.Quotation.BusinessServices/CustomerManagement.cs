﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Threading.Tasks;
using Airmaster.Quotation.DataModel.Repository;
using Airmaster.Quotation.BusinessEntities;
using Airmaster.Shared.Infrastructure.DataAccess.DataProvider;

namespace Airmaster.Quotation.BusinessServices
{
    public class CustomerManagement
    {
        private GenericRepository<CustomerEntity> _customerRepository;

        public CustomerManagement()
        {
            ConnectionFactory factory = new ConnectionFactory("AEADatabase");
            _customerRepository = new GenericRepository<CustomerEntity>(factory.GetConnection);
        }

        public int? Save(CustomerEntity Customer)
        {
            if(Customer.CustomerID == null)
                return _customerRepository.Insert(Customer);
            else
                return _customerRepository.Update(Customer);
        }

        public int? Update(CustomerEntity customer)
        {
            return _customerRepository.Update(customer);
        }

        public Task<CustomerEntity> GetCustomerDetail(int id)
        {
            return _customerRepository.GetAsync(id);
        }

        public async Task<List<CustomerEntity>> GetAllCustomers()
        {
            var result = await _customerRepository.GetListAsync();
            return result.ToList();
        }
    }
}