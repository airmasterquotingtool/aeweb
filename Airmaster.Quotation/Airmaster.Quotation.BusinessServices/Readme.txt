﻿Project Guidelines

•	Interface classes to define all the CRUD operation	
•   Implementation for the Interface
•	May contain UnitofWork object which will be initialised with the constructor
•	Uses mapper to map Business entity classes to DB entities classes
