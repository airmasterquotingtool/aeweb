﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Threading.Tasks;

namespace Airmaster.Quotation.DataModel.Repository
{
    //Generic Repository for Entities which need CRUD+ functionality
    public interface IGenericRepository<TEntity> : IDisposable where TEntity : class
    {
        //IEnumerable<TEntity> GetAll();
        //Task<IEnumerable<TEntity>> GetAllAsync();

        //IEnumerable<TEntity> GetData(string qry, object parameters);
        Task<IEnumerable<TEntity>> GetListAsync(string qry, object parameters);

        //TEntity Get(object pksFields);
        //Task<TEntity> GetAsync(object pksFields);
        Task<TEntity> GetAsync(object ID);

        //int Add(TEntity entity);
        Task<int?> InsertAsync(TEntity entity);

        //int Add(TEntity entity);
        int? Insert(TEntity entity);

        //int Update(TEntity entity, object pks);
        int Update(TEntity entity);

        //void Delete(object key);
        Task<int> Delete(object key);

    }
}