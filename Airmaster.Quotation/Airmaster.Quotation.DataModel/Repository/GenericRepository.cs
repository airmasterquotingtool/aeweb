﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Dapper;
using System.Threading.Tasks;

namespace Airmaster.Quotation.DataModel.Repository
{
    /// <summary>
    /// Generic Repository class for Entity Operations
    /// </summary>
    /// <typeparam name="TEntity"></typeparam>
    public class GenericRepository<TEntity> : IDisposable , IGenericRepository<TEntity> where TEntity: class 
    {
        internal IDbConnection Context;

        public GenericRepository(IDbConnection context)
        {
            this.Context = context;
        }

        public virtual async Task<IEnumerable<TEntity>> GetDataAsync(string sqlQuery, object parameters)
        {
            DynamicParameters dp = new DynamicParameters();
            dp.AddDynamicParams(parameters);
            return await Context.QueryAsync<TEntity>(sqlQuery, dp, null, null, CommandType.StoredProcedure);
        }

        // Show all Records
        public virtual async Task<IEnumerable<TEntity>> GetListAsync()
        {
            return await Context.GetListAsync<TEntity>();
        }

        //Gets list of all records matching the where options
        public virtual async Task<IEnumerable<TEntity>> GetListAsync(string conditions)
        {
            return await Context.GetListAsync<TEntity>(conditions);
        }

        // Gets list of all records matching the conditions
        public virtual async Task<IEnumerable<TEntity>> GetListAsync(string conditions , object parameters)
        {
            return await Context.GetListAsync<TEntity>(conditions,parameters);
        }

        // Gets paged list of all records matching the conditions
        public virtual async Task<IEnumerable<object>> GetGenericPagedListAsync(string Command, int UserID, int PageNumber, int PageSize, 
            string SortBy,string OrderBy)
        {
            //return await Context.GetListPagedAsync<TEntity>(pageNumber, recordsPerPage, condition, orderBy, parameters);
            DynamicParameters dp = new DynamicParameters();
            dp.Add("@USERID", UserID);
            dp.Add("@PageSize", PageSize);
            dp.Add("@PageNumber", PageNumber);
            dp.Add("@SortBy", SortBy);
            dp.Add("@OrderBy", OrderBy);
            return await Context.QueryAsync<object>(sql: Command, param: dp, commandType: CommandType.StoredProcedure);
        }

        public virtual Task<TEntity> GetAsync(object id)
        {
            return Context.GetAsync<TEntity>(id);
        }

        public virtual Task<int?> InsertAsync(TEntity entity)
        {
            return Context.InsertAsync(entity);
        }

        public virtual int? Insert(TEntity entity)
        {
           return Context.Insert(entity);
        }

        public virtual int Update(TEntity entityToUpdate)
        {
            return Context.Update(entityToUpdate);  
        }

        public virtual Task<int> Delete(object id)
        {
            return Context.DeleteAsync(id);
        }

        //public virtual IEnumerable<TEntity> GetMany(Func<TEntity, bool> where)
        //{
        //    return DbSet.Where(where).ToList();
        //}

        //public virtual IQueryable<TEntity> GetManyQueryable(Func<TEntity, bool> where)
        //{
        //    return DbSet.Where(where).AsQueryable();
        //}

        //public TEntity GetAsync(Func<TEntity, Boolean> where)
        //{
        //    return DbSet.Where(where).FirstOrDefault<TEntity>();
        //}

        //public void Delete(Func<TEntity, Boolean> where)
        //{
        //    IQueryable<TEntity> objects = DbSet.Where<TEntity>(where).AsQueryable();
        //    foreach (TEntity obj in objects)
        //        DbSet.Remove(obj);
        //}

        //public IQueryable<TEntity> GetWithInclude(
        //    System.Linq.Expressions.Expression<Func<TEntity,
        //    bool>> predicate, params string[] include)
        //{
        //    IQueryable<TEntity> query = this.DbSet;
        //    query = include.Aggregate(query, (current, inc) => current.Include(inc));
        //    return query.Where(predicate);
        //}

        //public bool Exists(object primaryKey)
        //{
        //    return DbSet.Find(primaryKey) != null;
        //}

        //public TEntity GetSingle(Func<TEntity, bool> predicate)
        //{
        //    return DbSet.Single<TEntity>(predicate);
        //}

        //public TEntity GetFirst(Func<TEntity, bool> predicate)
        //{
        //    return DbSet.First<TEntity>(predicate);
        //}

        public void Dispose()
        {
            //container.Dispose();
        }
    }
}