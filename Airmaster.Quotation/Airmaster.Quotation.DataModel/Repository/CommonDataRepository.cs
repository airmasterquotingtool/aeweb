﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Threading.Tasks;
using System.Diagnostics;
using Dapper;
using Airmaster.Quotation.BusinessEntities.Common;

namespace Airmaster.Quotation.DataModel.Repository
{
    public class CommonDataRepository : IDisposable
    {   
        internal IDbConnection Context;

        public CommonDataRepository(IDbConnection context)
        {
            this.Context = context;
        }

        public List<CodeDataEntity> GetCodes()
        {
            var result = Context.Query<CodeDataEntity>("QTpGetCodes", commandType: CommandType.StoredProcedure);
            return result.ToList();
        }

        public List<DivisionDropDown> GetDivisions(string state=null)
        {
            DynamicParameters dp = new DynamicParameters();
            dp.Add("state", state);
            var result = Context.Query<DivisionDropDown>("QTpGetDivisions", param: dp, commandType: CommandType.StoredProcedure);
            return result.ToList();
        }

        public List<VendorDropDown> GetVendors(string state = null)
        {
            DynamicParameters dp = new DynamicParameters();
            dp.Add("state", state);
            var result = Context.Query<VendorDropDown>("QTpGetVendors", param: dp, commandType: CommandType.StoredProcedure);
            return result.ToList();
        }

        public List<ContractorDropDown> GetContractors(string state = null)
        {
            DynamicParameters dp = new DynamicParameters();
            dp.Add("state", state);
            var result = Context.Query<ContractorDropDown>("QTpGetContractors", param: dp, commandType: CommandType.StoredProcedure);
            return result.ToList();
        }

        public List<LabourClassDropDown> GetLabourClass(string state = null)
        {
            DynamicParameters dp = new DynamicParameters();
            dp.Add("state", state);
            var result = Context.Query<LabourClassDropDown>("QTpGetLabourClass", param: dp, commandType: CommandType.StoredProcedure);
            return result.ToList();
        }

        public List<EquipmentDropDown> GetEquipments(int? LocationID)
        {
            DynamicParameters dp = new DynamicParameters();
            dp.Add("LocationID", LocationID);
            var result = Context.Query<EquipmentDropDown>("QTpGetEquipments", param: dp, commandType: CommandType.StoredProcedure);
            return result.ToList();
        }

        public List<TechnicianDropDown> GetTechnicians()
        {
            var result = Context.Query<TechnicianDropDown>("QTpGetTechnicians",null, commandType: CommandType.StoredProcedure);
            return result.ToList();
        }

        public List<CustomerDropDown> GetCustomers()
        {
            var result = Context.Query<CustomerDropDown>("QTpGetCustomers", null, commandType: CommandType.StoredProcedure);
            return result.ToList();
        }

        public List<LocationDropDown> GetLocations(int CustomerID)
        {
            DynamicParameters dp = new DynamicParameters();
            dp.Add("CustomerID", CustomerID);
            var result = Context.Query<LocationDropDown>("QTpGetLocations", dp, commandType: CommandType.StoredProcedure);
            return result.ToList();
        }

        public List<ContactDropDown> GetContacts(int LocationID)
        {
            DynamicParameters dp = new DynamicParameters();
            dp.Add("LocationID", LocationID);
            var result = Context.Query<ContactDropDown>("[QTpGetContacts]", dp, commandType: CommandType.StoredProcedure);
            return result.ToList();
        }

        public List<ProductDropDown> GetProducts()
        {
            var result = Context.Query<ProductDropDown>("[QTpGetProducts]", null, commandType: CommandType.StoredProcedure);
            return result.ToList();
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    Debug.WriteLine("Common Context is being disposed");
                    Context.Dispose();
                }
            }
            this.disposed = true;
        }

        /// <summary>
        /// Dispose method
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}