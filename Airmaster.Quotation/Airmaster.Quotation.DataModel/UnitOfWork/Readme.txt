﻿Project Guidelines

•	Will have reference to Repository (using DI) and will have operations to manage transactions etc
•	Create only if satisfies any of the following: 
	o	To manage transactions.
	o	To order the database inserts, deletes, and updates.
	o	To prevent duplicate updates. Inside a single usage of a Unit of Work object, different parts of the code may mark the same Invoice object as changed, but the Unit of Work class will only issue a single UPDATE command to the database.
