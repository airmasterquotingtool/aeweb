﻿import { Component, Injectable, Input, Output, EventEmitter } from '@angular/core';
import { Quote, ProductDropDown, CodeData, VendorDropDown,EquipmentDropDown } from './../Model/quote';
@Injectable()
export class SharedDataService {
    sharingData: Quote = new Quote();
    states: Array<CodeData>;
    creditors: Array<string>;
    equipments: Array<String> = [];


    productList = Array<ProductDropDown>();
    saveData(quote) {
        this.sharingData = quote;
    }
    setProductList(products) {
        this.productList = products;
    }
    setCreditors(creditors: VendorDropDown[]) {
        this.creditors = creditors.map(function (obj) { return obj.Code });
    }
    public getCreditors() {

        return this.creditors;
    }
    setEquipments(equipments: EquipmentDropDown[]) {
        this.equipments = equipments.map(function (obj) { return obj.Code });
    }
    public getEquipments() {

        return this.equipments;
    }


    public getProductList() {
        return this.productList;
    }
    public getData() {
        return this.sharingData;
    }
} 