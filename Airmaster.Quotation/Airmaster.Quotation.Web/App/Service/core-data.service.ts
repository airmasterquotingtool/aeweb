﻿import { Injectable } from '@angular/core';
import { Http, Response, URLSearchParams, RequestOptions } from '@angular/http';
import { HttpInterceptorService} from 'ng-http-loader/http-interceptor.service.js';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import {
    Division, Location, CodeData, Customer, LabourClass, Quote,
    ContactEntity, TechnicianEntity, ProductDropDown, EquipmentDropDown, VendorDropDown
} from './../Model/quote';
import { Section } from './../Model/section';
import { QuotationLabourEntity } from './../Model/Section/labour';

@Injectable()
export class CoreDataService {
    private baseUrl = "http://localhost:49759";
    private getCustomersUrl = this.baseUrl +'/api/Common/GetCustomers';
    private getDivisionUrl = this.baseUrl + '/api/Common/GetDivisions';  // URL to web API
    private getContactsUrl = this.baseUrl + '/api/Common/GetContacts';  // URL to web API
    private getProductsUrl=this.baseUrl +'/api/Common/GetProducts';
    private getLocationUrl = this.baseUrl + '/api/Common/GetLocations';  
    private getCoreDataUrl = this.baseUrl +'/api/Common/GetCodeData';  //URL to get most common form field data
    private getLabourClassUrl = this.baseUrl + "/api/Common/GetLabourClass";
    private getTechniciansUrl = this.baseUrl + "/api/Common/GetTechnicians";
    private getQuotationListUrl = this.baseUrl + "/api/Quote/GetQuotes";
    private saveQuoteInfoUrl = this.baseUrl + "/api/Quote/SaveQuote";
    private getQuoteTypeUrl = this.baseUrl + "/api/Quote/SaveQuote";
    private getEquipmentsUrl = this.baseUrl +"/api/Common/GetEquipments";
    private saveQuoteSectionInfoUrl = this.baseUrl + "/api/Quote/SaveSection";
    private getCreditorsUrl = this.baseUrl + "/api/Common/GetVendors";
    private getQuoteDetailUrl = this.baseUrl + "/api/Quote/GetQuoteDetail";
    private getSectionsUrl = this.baseUrl + "/api/Quote/GetSections";
    private saveLabourUrl = this.baseUrl + "/api/Quote/SaveLabour";
    private getLaboursUrl = this.baseUrl + "/api/Quote/GetLabours";
    private getMaterialsUrl = this.baseUrl + "/api/Quote/GetMaterials";
    private saveMaterialUrl = this.baseUrl + "/api/Quote/SaveMaterial";
    constructor(private http: HttpInterceptorService) { }

    getQuotationList(accountManagerID): Observable<Quote[]> {
        let params: URLSearchParams = new URLSearchParams();
        params.set('PageSize', '10');
        params.set('PageNumber', '1');
        params.set('SortBy', 'Name');
        params.set('OrderBy', 'ASC');
        return this.http.get(this.getQuotationListUrl, {search:params})
            .map(this.extractData)
            .catch(this.handleError);
    }
    getQuoteDetail(quoteID): Observable<Quote> {
        let params: URLSearchParams = new URLSearchParams();
        params.set('QuoteID', quoteID);       
        return this.http.get(this.getQuoteDetailUrl, { search: params })
            .map(this.extractData)
            .catch(this.handleError);
    }

    getSections(quoteID): Observable<Section[]> {
        let params: URLSearchParams = new URLSearchParams();
        params.set('QuoteID', quoteID);
        return this.http.get(this.getSectionsUrl, { search: params })
            .map(this.extractData)
            .catch(this.handleError);
    }
    getLabours(quoteSectionID): Observable<QuotationLabourEntity[]> {
        let params: URLSearchParams = new URLSearchParams();
        params.set('SectionID', quoteSectionID);
     
        return this.http.get(this.getLaboursUrl, { search: params })
            .map(this.extractData)
            .catch(this.handleError);
    }
    getMaterials(quoteSectionID): Observable<QuotationLabourEntity[]> {
        let params: URLSearchParams = new URLSearchParams();
        params.set('SectionID', quoteSectionID);

        return this.http.get(this.getMaterialsUrl, { search: params })
            .map(this.extractData)
            .catch(this.handleError);
    }


    getCreditors(state): Observable<VendorDropDown[]> {
        let params: URLSearchParams = new URLSearchParams();
        params.set('state', state);       
        return this.http.get(this.getCreditorsUrl, { search: params })
            .map(this.extractData)
            .catch(this.handleError);
    }


    saveQuoteGeneralInfo(quote): Observable<number> { 
        return this.http.post(this.saveQuoteInfoUrl, quote)
            .map(this.extractData)
            .catch(this.handleError);
    }

    saveQuoteSectionInfo(section): Observable<number> {
        return this.http.post(this.saveQuoteSectionInfoUrl, section)
            .map(this.extractData)
            .catch(this.handleError);
    }
    saveLabour(Labour): Observable<number> {
        return this.http.post(this.saveLabourUrl, Labour)
            .map(this.extractData)
            .catch(this.handleError);
    }
    saveMaterial(Material): Observable<number> {
        return this.http.post(this.saveMaterialUrl, Material)
            .map(this.extractData)
            .catch(this.handleError);
    }
    //get common list values 
    codeData: any;
    getCoreData(): Observable<CodeData[]> {
        return this.http.get(this.getCoreDataUrl)
            .map(this.extractData)
            .catch(this.handleError);
    }

    getCustomers(): Observable<Customer[]> {
        return this.http.get(this.getCustomersUrl)
            .map(this.extractData)
            .catch(this.handleError);
    }

    getDivisions(state): Observable<Division[]> {
        let params: URLSearchParams = new URLSearchParams();
        params.set('state', state);
        return this.http.get(this.getDivisionUrl, { search: params })
            .map(this.extractData)
            .catch(this.handleError);
    }

    getContacts(locationID): Observable<ContactEntity[]> {
        let params: URLSearchParams = new URLSearchParams();
        params.set('LocationID', locationID);
        return this.http.get(this.getContactsUrl, { search: params })
            .map(this.extractData)
            .catch(this.handleError);
    }
    getProducts(): Observable<ProductDropDown[]> {
      
        return this.http.get(this.getProductsUrl)
            .map(this.extractData)
            .catch(this.handleError);
    }


    getLocation(): Observable<Location[]> {
        return this.http.get(this.getLocationUrl)
            .map(this.extractData)
            .catch(this.handleError);
    }

    getLocationByCustomerID(customerID): Observable<Location[]> {
        let params: URLSearchParams = new URLSearchParams();
        params.set('CustomerID', customerID);
      
        return this.http.get(this.getLocationUrl, {search:params} )
            .map(this.extractData)
            .catch(this.handleError);
    }
    getTechnicians(): Observable<TechnicianEntity[]> {
      
        return this.http.get(this.getTechniciansUrl)
            .map(this.extractData)
            .catch(this.handleError);
    }




    getLabourClasses(state): Observable<LabourClass[]> {
        let params: URLSearchParams = new URLSearchParams();
        params.set('state', state);
        return this.http.get(this.getLabourClassUrl, { search: params } )
            .map(this.extractData)
            .catch(this.handleError);
    }


    getEquipments(locationID): Observable<EquipmentDropDown[]> {
        let params: URLSearchParams = new URLSearchParams();
        params.set('LocationID', locationID);
        return this.http.get(this.getEquipmentsUrl, { search: params })
            .map(this.extractData)
            .catch(this.handleError);
    }

    //common applicable  operations on http response object
    private extractData(res: Response) {
        let body = res.json();
        return body || {};
    }

    private handleError(error: Response | any) {
        //  will use a remote logging infrastructure 
        let errMsg: string;
        if (error instanceof Response) {
            const body = error.json() || '';
            const err = body.error || JSON.stringify(body);
            errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
        } else {
            errMsg = error.message ? error.message : error.toString();
        }
        console.error(errMsg);
        return Observable.throw(errMsg);
    }
}