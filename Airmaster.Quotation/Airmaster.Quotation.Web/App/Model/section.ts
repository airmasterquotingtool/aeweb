﻿import { QuotationLabourEntity } from './../Model/Section/labour';
import { QuotationMaterialEntity } from './../Model/Section/material';
import { Equipment } from './../Model/Section/equipment';
import { SubContractor } from './../Model/Section/sub-contractors';
import { Other } from './../Model/Section/others';

//export class QuotationSectonEntity {
//    /*[Key]*/
//    public QuoteSectionID: number;
//    public QuoteID: number;
//    public Title: string;
//    public Preferred: number;
//    public LocationID: number;
//    public Equipment: string;
//    public Description: string;
//    public CreationDate: Date;
//    public CreatedBy: number;
//    public UpdationDate: Date;
//    public UpdatedBy: number;
//}


export class Section {   
    constructor(
        public QuoteSectionID?: number,
        public QuoteID?: number,
        public Title?: string,
        public Preferred?: boolean,
        public LocationID?: number,
        public Equipment?: string,
        public Description?: string,
        public WorkState?:number,
       
        /**calucated values**/
        public PriceLabour: number=0,
        public PriceMaterial: number=0,
        public PriceAdditional: number=0,
        public PriceTotal_Ex_GST?: number,
        public GST_Total?: number,
        public PriceTotal_Inc_GST?: number,
        public MarginActualPercentage?: number,
        public MarginActualAmount?: number,
        /**end caculated values**/
        public Labours: Array<QuotationLabourEntity> = [],
        public Materials: Array<QuotationMaterialEntity> = [],
        public Equipments: Array<Equipment> = [],
        public SubContractors: Array<SubContractor> = [],
        public Others: Array<Other> = [],
        public CreationDate?: Date,
        public CreatedBy?: number,
        public UpdationDate?: Date,
        public UpdatedBy?: number,
      
    ) { }
}
