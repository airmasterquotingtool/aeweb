﻿//generic class to handle list values for drop down throughout application's typescript
export class DropDownModel {
    constructor(
        public itemList: Array<Item>,
        public id?: string,
        public cssClass?: string,
        public SelectedItem?: string
        
    ) {  }    
}
export class Item {
    constructor(
        public value: string,
        public text?: string
    ) {  }   
}
//status of a quote under draft
export const QuoteStatus: DropDownModel = new DropDownModel(
    [
        new Item("Draft", ""),
        new Item("Awaiting Approval", ""),
        new Item("Canceled", ""),
        new Item("Revised", ""),
        new Item("Won", ""),
        new Item("Lost", "")
    ],
    "ddlQuoteStatus", "", "");
    
//status of lost quote
export const LostQuoteStatus: DropDownModel = new DropDownModel(
    [
        new Item("Price", "Price"),
        new Item("Relationship", "Relationship"),
        new Item("Reduction in price", "Reduction in price"),
        new Item("Contract Review", "Contract Review - Not resale"),
        new Item("Canceled", "Canceled"),
        new Item("Revised", "Revised")
    ],
    "ddlLostQuoteStatus", "", "");


//priority levels of quote
export const QuotePriority: DropDownModel = new DropDownModel(
    [
        new Item("L", "Low"),
        new Item("M", "Medium"),
        new Item("H", "High"),
        new Item("C", "Compliance"),      
    ],
    "ddlQuotePriority", "", "");

//memo type 
export const MemoType: DropDownModel = new DropDownModel(
    [
    new Item("RFI", "Response for Information"),
    new Item("RFQ", "Response for Quote")
       
    ],
    "ddlMemoType", "", "");

//probability percentage
export const Probability: DropDownModel = new DropDownModel(
    [
        new Item("0 %", "0 %"),
        new Item("10 %", "10 %"),
        new Item("25 %", "25 %"),
        new Item("50 %", "50 %"),
        new Item("75 %", "75 %"),
        new Item("90 %", "90 %"),
        new Item("100 %", "100 %"),
    ],
    "ddlProbability", "", "");

//quote stages
export const Stage: DropDownModel = new DropDownModel(
    [
    new Item("TN", ""),
    new Item("10 %", "Tender Notification"),
    new Item("SB", "Site Briefing"),
    new Item("RFI", "Response for information (RFI)"),
    new Item("QT", "Quotation (Maitnenance Proposal)"),
    new Item("PS", "Proposal Submission"),
    new Item("PC", "Procurement Clarifications"),
    new Item("FN", "Final Negotiations"),
    new Item("NL", "Notification Letter (Win / Lost)"),
    ],
    "ddlStages", "", "");

//Location
export const Location: DropDownModel = new DropDownModel(
    [
        new Item("RMIT", "RMIT"),
        new Item("AU", "AU"),
       
    ],
    "ddlLocation", "", "");

//States
export const States: DropDownModel = new DropDownModel(
    [
        new Item("SampleState1", "SampleState1"),
        new Item("SampleState2", "SampleState2"),

    ],
    "ddlStates", "", "");
//quote type
export const Types: DropDownModel = new DropDownModel(
    [
        new Item("1", "Service Quoted Work"),
        new Item("2", "Maintance Related"),

    ],
    "ddlStates", "", "");