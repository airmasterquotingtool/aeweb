﻿export class MenuItem {
    public MenuText: string;
    public MenuUrl: string;    
}
export class QuotationMenus {    
    constructor(
        public quotationMenus: Array<MenuItem>
    ) { }
}
