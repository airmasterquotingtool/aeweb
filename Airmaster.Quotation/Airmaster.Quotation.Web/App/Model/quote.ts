﻿import { Section } from './../Model/section';


export class Quote {
    /*[Key]*/    
    public QuoteID: number;
    public QuoteReference: string;
    public Subject: string;
    public Description: string;
    public StatusCD: number;
    public Revision: number;
    public AccountManagerID: number;
    public CustomerID: number;
    public LocationID: number;
    public ContactID: number;
    public WorkStateCd: number;
    public DivisionID: number;
    public Margin: number;
    public PriorityCd: number;
    public QuoteTypeCd: number;
    public WorkRequestNo: string;
    public MemoSubmittedByID: number;
    public MemoTypeCd: number;
    public EstimatedCloseDate: Date = new Date();
    public ProbabilityCd: number;
    public StageCd: number;
    public Introduction: string;
    public Preferred: number;
    public SOW: string;
    public Exclusions: string;
    public CreationDate: Date;
    public CreatedBy: number;
    public UpdationDate: Date;
    public UpdatedBy: number;
    public Sections: Array<Section> = [];
    public Pricing: QuotePrice;
    constructor() {
        this.Pricing = new QuotePrice(0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
    }
}

export class VendorDropDown {
    public VendorID: number;
    public Code: string;
    public Name: string;
    public State: string;
}

export enum CodeName {
    UserRole = 1,
    QuoteStatus = 2,
    QuoteLostStatus = 3,
    QuotePriority = 4,
    MemoType = 5,
    QuoteProbability=6,
    QuoteStage = 7,
    QuoteType=8,
    State=9
}
export class CodeData  {
    public CodeID: number;
    public CodeTypeID: number;
    public CodeName: string;
    public Code: string;
    public Description: string;
}
export class Customer {
    public CustomerID: number;
    public Code: string;
    public Name: string;
}
export class TechnicianEntity {
    public TechnicianID: number;
    public Code: string;
    public Name: string;
}
export class ProductDropDown {
    public ProductID: number;
    public Code: string;
    public Name: string;
    public CostPerUnit: number;
}
export class ContactEntity {
    /*[Key]*/
    public ContactID: number;
    public Code: string;
    public Name: string;
    public AddressLine1: string;
    public AddressLine2: string;
    public AddressLine3: string;
    public City: string;
    public StateCd: string;
    public Zip: string;
    public CountryCd: number;
    public CreatedBy: number;
    public CreationDate: Date;
    public UpdatedBy: string;
    public UpdationDate: Date;
}

export class Division  {
    public DivisionID: number;
    public State: string;
    public Divisions: string;
    public DefaultMargin: number;
}
export class EquipmentDropDown {
    public EquipmentID: number;
    public LocationID: string;
    public Code: string;
    public EquipmentType: string;
}
export class Location  {
    public LocationID: number;
    public CustomerCode: string;
    public Code: string;
    public Name: string;
    public AddressLine1: string;
    public AddressLine2: string;
    public State: string;
    public Pincocde: string;
    public Country: string;
}

export class QuotePricing {
    public PriceLabour: number;
    public PriceMaterials: number;
    public PriceAdditional: number;
    public PriceTotal_Ex_GST: number;
    public WeightedTotal: number;
    public GST_Total: number;
    public PriceTotal_Inc_GST: number;
    public MargingActual_Percentage: number;
    public MargingActual: number;

}
export class LabourClass  {
    public Code: string;
    public Description: string;
    public Rate: number;
    public State: string;
}
export class QuotePrice {
    constructor(
        public PriceLabours?: number,
        public PriceMaterials?: number,
        public PriceAdditional?: number,
        public PriceTotal_Ex_GST?: number,
        public WeightedTotal?: number,   
         public GST_Total?: number,
        public PriceTotal_Inc_GST?: number,
        public MarginActual_Percentage?: number,
        public MarginActual?: number,
        public MarkUpActual?: number
        
        ) { }
}