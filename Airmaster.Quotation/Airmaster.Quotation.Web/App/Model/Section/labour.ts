﻿export class QuotationLabourEntity { 
    constructor(
        public QuoteLabourID?: number,
        public QuoteSectionID?: number,
        public Class?: string,
        public CostPerUnit?: number,
        public Quantity?: number,
        public CostSubTotal?: number,
        public Margin?: number,
        public PricePerUnit?: number,
        public PriceSubTotal?: number,
        public Description?: String,


    ) { }
}

