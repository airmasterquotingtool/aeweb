﻿export class SubContractor {
    constructor(
        public SEQ?: number,
        public Description?: String,
        public CostPerUnit: number=0,
        public SubContractorQuantity: number=0,
        public CostSubTotal: number=0,
        public Margin: number=0,
        public PricePerUnit: number=0,
        public PriceSubTotal: number=0,
        public Reference?: String,
        public Creditor?: String


    ) { }

}