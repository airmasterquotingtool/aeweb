﻿export class QuotationMaterialEntity {
    constructor(
        public QuoteMaterialID?: number,
        public QuoteSectionID?: number,
        public Product?: String,
        public CostPerUnit?: number,
        public Quantity?: number,
        public CostSubTotal?: number,
        public Margin?: number,
        public PricePerUnit?: number,
        public PriceSubTotal?: number,
        public Creditor?: String,


    ) { }

}


