﻿export class Equipment {
    constructor(
        public SEQ?: number,
        public Description?: String,
        public CostPerUnit?: number,
        public EquipmentQuantity?: number,
        public CostSubTotal?: number,
        public Margin?: number,
        public PricePerUnit?: number,
        public PriceSubTotal?: number,
        public Reference?: string,
        public Creditor?: String ) { }
}

