﻿import { Component, OnInit } from '@angular/core';
import { Router} from '@angular/router';
import { process, State } from '@progress/kendo-data-query';
import { SharedDataService } from "./../Service/shared-data.service";
import { CoreDataService } from './../Service/core-data.service';
import { Quote, QuotePrice } from './../Model/quote';
import { Section } from './../Model/section';
import {
    GridComponent,
    GridDataResult,
    DataStateChangeEvent
} from '@progress/kendo-angular-grid';

@Component({
    selector: 'quote-list',
    template: `
<div class="row no-gutter quoteList">
<kendo-grid
        [data]="gridData"
        [pageSize]="state.take"
        [skip]="state.skip"
        [sort]="state.sort"
        [filter]="state.filter"
        [sortable]="true"
        [pageable]="true"
        [filterable]="true"
        [selectable]="true"       
        (dataStateChange)="dataStateChange($event)">

    <kendo-grid-column field="Subject"     title="Subject" width="240" >
        <ng-template kendoGridCellTemplate let-dataItem let-rowIndex="rowIndex">
             <span (click)="EditQuote(dataItem)" 
            style="color:blue;text-decoration:underline;cursor:pointer;"
            title={{dataItem.Subject}}>{{dataItem.Subject}}</span>
        </ng-template>

      
    </kendo-grid-column>
    <kendo-grid-column field="Customer" title="Customer"  width="180">
    </kendo-grid-column>
    <kendo-grid-column field="Location" title="Location" width="100" >
    </kendo-grid-column>
    <kendo-grid-column field="QuoteID" title="QuoteID" width="70" >
    </kendo-grid-column>
    <kendo-grid-column field="Revision" title="Revision" width="70">
    </kendo-grid-column>
    <kendo-grid-column field="EstimatedCloseDate" title="Est.Close Date" width="100" filter="date" format="{0:d}" >
    </kendo-grid-column>
    <kendo-grid-column field="EstimatedCloseDate_Quote" title="Est.CloseDate(Quote)" width="100" filter="date" format="{0:d}">
    </kendo-grid-column>
    <kendo-grid-column field="Probability" title="Probability" width="70" filter="numeric" format="{0:c}">
    </kendo-grid-column>
    <kendo-grid-column field="QuoteWeightedValue" title="Quote Weighted Value" width="100" filter="numeric" format="{0:c}">
    </kendo-grid-column>
    <kendo-grid-column field="SaleType" title="SaleType" width="70" >
    </kendo-grid-column>
    <kendo-grid-column field="Variation" title="Variation" width="50" >
    </kendo-grid-column>
    <kendo-grid-column field="WorkState" title="WorkState" width="80">
    </kendo-grid-column>
    <kendo-grid-column field="QuotePriceTotal" title="Quote Price Total" width="100" filter="numeric" format="{0:c}">
    </kendo-grid-column>
    <kendo-grid-column field="StatusReason" title="Status Reason" width="70" >
    </kendo-grid-column>
    <kendo-grid-column field="Owner" title="Owner" width="80" >
    </kendo-grid-column>
    <kendo-grid-column field="CreatedOn" title="Created On" width="100" filter="date" format="{0:d}">
    </kendo-grid-column>
    <kendo-grid-column field="ModifiedOn" title="Modified On" width="100" filter="date" format="{0:d}">
    </kendo-grid-column>    
    </kendo-grid>
</div>
`
  
})
export class QuoteList implements OnInit  {
   
    private state: State = {
        skip: 0,
        take:10
    };
    public errorMessage: string;
    public selectedQuote: Quote = new Quote();
    quoteList: Array<Quote>= [];
    private gridData: GridDataResult = process(this.quoteList, this.state);

    constructor(
        private coreDataService: CoreDataService, private _router: Router, private sharedDataService:SharedDataService
    ) { }

    ngOnInit() {
        this.sharedDataService.sharingData = null;
        this.getQuoteList(); 
    }
    getQuoteList() {
        this.coreDataService.getQuotationList(0)
            .subscribe(
            quoteList => { this.quoteList = quoteList;
                this.gridData = process(this.quoteList, this.state);
            },
            error => this.errorMessage = <any>error);
    }
    EditQuote(val)
    {

        //alert("Edit");
        //let price= new QuotePrice(0,0,0,0,0,0,0,0,0,0);
        //val.Pricing = price;
        //let sections = [new Section(1)];
        //val.Sections = sections;
        //this.sharedDataService.saveData(val);
   
        this.getQuoteDetail(val.QuoteID);
      
    }

    getQuoteDetail(quoteID) {
        this.coreDataService.getQuoteDetail(quoteID)
            .subscribe(
            quote => {
                quote.Sections = [];
                this.sharedDataService.saveData(quote); 
                this._router.navigate(["CreateQuote"]);
            },
            error => this.errorMessage = <any>error);
    }

    protected dataStateChange(state: DataStateChangeEvent): void {
        this.state = state;
        this.gridData = process(this.quoteList, this.state);
    }
}

