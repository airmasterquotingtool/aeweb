﻿import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { NgbModule, NgbDatepicker } from '@ng-bootstrap/ng-bootstrap';
import { CoreDataService } from './../Service/core-data.service';
import { SharedDataService } from './../Service/shared-data.service';
import {
    Quote, Location, CodeData, CodeName, Customer, Division,
    ContactEntity, ProductDropDown, VendorDropDown,
    TechnicianEntity
} from './../Model/quote';
import { Types } from './../Model/shared';
@Component(
    {
        selector: 'quote-general-info-input',
        styles: ['.customers { width: 300px; }'],
        templateUrl: './../../App/Templates/quote-general-info-input.component.html'
     
    }
)
export class QuoteGeneralInfoComponent implements OnInit {
    errorMessage: string;
    codeData: CodeData[];
    locations: Location[];
    mode = 'Observable';
    @Input() quote: Quote;
    @Output() notify: EventEmitter<Quote> = new EventEmitter<Quote>();
    //assign form constants to local variables to be used  in input form
    creditors: Array<VendorDropDown> = [];
    customerData = Array<Customer>();
    contacts = Array<ContactEntity>();
    technicians = Array<TechnicianEntity>();
    customers = Array<Customer>();
    divisions = Array<Division>();
    priority = Array<CodeData>();
    stage = Array<CodeData>();
    status = Array<CodeData>();
    quoteTypes = Array<CodeData>();
    probability = Array<CodeData>();
    memoType = Array<CodeData>();;
    states = Array<CodeData>();
    min = new Date();
    clickMessage = '';
    value = new Date();
    model = new Quote();
    type = Types;
    submitted = false;
    public isDisabledLocations: boolean = false;
    public defaultLocation: Location = { Code: null, LocationID: null, State: null, CustomerCode: null, Name: "Select Location", Country: null, AddressLine1: null, AddressLine2: null, Pincocde: null };
    //inject coredataservice
    constructor(   private coreDataService: CoreDataService,private sharedDataService:SharedDataService
    ) {
        this.getCustomers();
        this.getCoreData();
        this.getTechnicians();
       
    }

    ngOnInit() {
        //if (this.quote.CustomerID != undefined)
        //{
        //    this.getLocationByCustomerID(this.quote.CustomerID);
           
        //}
        
    }
    //on customer change fetch location by customer code using core-data service
    onCustomerChange(value) {
        if (value != undefined) {

            this.quote.CustomerID = value;

            this.quote.LocationID = undefined;
            if (value == this.defaultLocation.LocationID) {
               // this.isDisabledLocations = true;
                this.locations = [];
            } else {
               // this.isDisabledLocations = false;
                this.getLocationByCustomerID(value);
            }
        }
    }
    onContactChange(value) {
        if (value != undefined) {

            this.quote.ContactID = value;
        }
    }


    onLocationChange(value) {
      
            if (value != undefined) {               
                this.getContacts(value);
                let locState = this.locations.filter(function (o) { return o.LocationID == value })[0].State;
                let wState = this.states.filter(function (obj) { return obj.Code == locState })[0];
                if (wState != undefined)
                {
                    this.quote.WorkStateCd = wState.CodeID;
                    this.getDivisions(this.quote.WorkStateCd);
                }
                this.getEquipments(value);
            }
        
    }
    //filter change 
    onCustomerFilterChange(value) {
        if (value == "" || value == undefined) {
            this.customerData = this.customers;
        }
        else {
            this.customerData = this.customers.filter((s) => s.Name.toLowerCase().indexOf(value.toLowerCase()) !== -1);
        }
    }

    onWorkStateChange(value) {
        if (value != undefined) {
            this.getDivisions(value);
            let stateCode = this.states.filter(function (obj) { return obj.CodeID === value })[0].Code;
            this.getCreditors(stateCode);
        }
    }

    onDivisionChange(value) {
        if (value != undefined) {
            this.quote.DivisionID = value;

            this.quote.Margin = this.divisions.filter(function (obj) { return obj.DivisionID === value })[0].DefaultMargin;
            this.quote.Preferred = this.quote.Margin;

        }
    }
    //fill up common dropdown form fields
    
    getCustomers() {
        this.coreDataService.getCustomers()
            .subscribe(
            customers => {
                this.customers = customers;
                this.customerData = this.customers;
                if (this.quote.CustomerID != undefined) {
                    this.getLocationByCustomerID(this.quote.CustomerID);
                }
            },
            error => this.errorMessage = <any>error);
    }


    getEquipments(locationID) {
        this.coreDataService.getEquipments(locationID)
            .subscribe(
            equipments => {
                this.sharedDataService.setEquipments(equipments);
            },
            error => this.errorMessage = <any>error);
    }

    getTechnicians() {
        this.coreDataService.getTechnicians()
            .subscribe(
            technicians => this.technicians = technicians,
            error => this.errorMessage = <any>error);
    }
    getContacts(locationID) {
       
        this.coreDataService.getContacts(locationID)
            .subscribe(
            contacts => this.contacts = contacts,
            error => this.errorMessage = <any>error);
    }


    getDivisions(state) {
        if (state != undefined) {
            let stateCode = this.states.filter(function (obj) { return obj.CodeID == state })[0].Code;
            this.coreDataService.getDivisions(stateCode)
                .subscribe(
                divisions => this.divisions = divisions,
                error => this.errorMessage = <any>error);
        }
    }
    //get dropdown list values for quote creation
    getCoreData() {
        this.coreDataService.getCoreData()
            .subscribe(
            codeData => {
                this.codeData = codeData;
                this.stage = this.codeData.filter(function (obj) { return obj.CodeTypeID == CodeName.QuoteStage });
                this.priority = this.codeData.filter(function (obj) { return obj.CodeTypeID == CodeName.QuotePriority });
                this.status = this.codeData.filter(function (obj) { return obj.CodeTypeID == CodeName.QuoteStatus });
                this.probability = this.codeData.filter(function (obj) { return obj.CodeTypeID == CodeName.QuoteProbability });
                this.memoType = this.codeData.filter(function (obj) { return obj.CodeTypeID == CodeName.MemoType });               
                this.states = this.codeData.filter(function (obj) { return obj.CodeTypeID == CodeName.State });
                this.quoteTypes = this.codeData.filter(function (obj) { return obj.CodeTypeID == CodeName.QuoteType });
                if (this.quote.QuoteID != undefined) {
                    this.getDivisions(this.quote.WorkStateCd);
                    let stateID = this.quote.WorkStateCd;
                    let stateCode = this.states.filter(function (obj) { return obj.CodeID === stateID })[0].Code;
                    this.getCreditors(stateCode);
                    if (this.quote.EstimatedCloseDate != undefined) {
                    }
                }
            },
            error => this.errorMessage = <any>error);
    }
    //get location based on customer code 
    getLocationByCustomerID(customerID) {
        this.coreDataService.getLocationByCustomerID(customerID)
            .subscribe(
            locations => {
            this.locations = locations;
            if (this.quote.LocationID != undefined) { this.getContacts(this.quote.LocationID);}
            },
            error => this.errorMessage = <any>error);
    }



    getCreditors(state) {
        this.coreDataService.getCreditors(state)
            .subscribe(
            creditors => {
                this.sharedDataService.setCreditors(creditors);
            },
            error => this.errorMessage = <any>error);
    }

    //emmit quote to parent component
    onQuoteGeneralInfoSubmit() {
        //this.quote.QuoteID = "A12145";
      
        this.saveQuoteGeneralInfo(this.quote);
        this.notify.emit(this.quote);
    }
    saveQuoteGeneralInfo(quote) {
        this.coreDataService.saveQuoteGeneralInfo(quote)
            .subscribe(
            quoteID => {

                if (this.quote.QuoteID == undefined)
                    this.quote.QuoteID = quoteID;
            },
            error => this.errorMessage = <any>error);
    }

    // TODO: Remove this when we're done
    get diagnostic() { return JSON.stringify(this.quote); }
}

