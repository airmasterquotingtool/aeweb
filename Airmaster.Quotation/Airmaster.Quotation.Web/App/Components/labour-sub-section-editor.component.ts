﻿import { Component, OnInit, Inject, Input, Output, EventEmitter, ElementRef, ViewChild } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AddEvent, EditEvent, GridComponent } from '@progress/kendo-angular-grid';
import { State, aggregateBy } from '@progress/kendo-data-query';
import { CoreDataService } from './../Service/core-data.service';
import { QuotationLabourEntity } from './../Model/Section/labour';
import { LabourClass } from './../Model/quote';
import { Section } from './../Model/section';
const formGroup = dataItem => new FormGroup({
    'QuoteLabourID': new FormControl(dataItem.QuoteLabourID, Validators.required),
    'Class': new FormControl(dataItem.Class, Validators.required),
    'CostPerUnit': new FormControl(dataItem.CostPerUnit),
    'Quantity': new FormControl(dataItem.Quantity, Validators.required),
    'CostSubTotal': new FormControl(dataItem.CostSubTotal),
    'Margin': new FormControl(dataItem.Margin, Validators.required),
    'PricePerUnit': new FormControl(dataItem.PricePerUnit),
    'PriceSubTotal': new FormControl(dataItem.PriceSubTotal),
    'Description': new FormControl(dataItem.Description)
});

@Component({
    selector: 'labour-sub-section-grid',
    template: `
            <kendo-grid
                      [data]="selectedSection.Labours "
                      [height]="300"
                      [pageSize]="gridState.take" [skip]="gridState.skip" [sort]="gridState.sort"
                      [pageable]="true" [sortable]="true"
                      (dataStateChange)="onStateChange($event)"
                      (edit)="editHandler($event)" (cancel)="cancelHandler($event)"
                      (save)="saveHandler($event)" (remove)="removeHandler($event)"
                      (add)="addHandler($event)"
            >
            <ng-template kendoGridToolbarTemplate>
                <button kendoGridAddCommand>Add new</button>
            </ng-template>    
            <kendo-grid-column field="QuoteLabourID" title="SEQ" [editable]="false" width="50">              
            </kendo-grid-column>
            <kendo-grid-column field="Class"  title="Labour Class" width="200">
              <ng-template kendoGridCellTemplate let-dataItem>
                {{LabourClassList(dataItem.Class)?.Description}}
              </ng-template>
              <ng-template kendoGridEditTemplate 
                let-dataItem="dataItem"
                let-formGroup="formGroup">
                <kendo-dropdownlist 
                  [data]="labourClassList"
                  textField="Description"
                  valueField="Code"
                  [valuePrimitive]="true"
                  [formControl]="formGroup.get('Class')">                
                </kendo-dropdownlist>
              </ng-template>
            </kendo-grid-column>
            <kendo-grid-column field="CostPerUnit" [editable]="false" title="Cost Per Unit">
                <ng-template kendoGridCellTemplate let-dataItem>
                {{dataItem.CostPerUnit}}
              </ng-template>
            <ng-template kendoGridEditTemplate 
                let-dataItem="dataItem"
                let-formGroup="formGroup">
                    <kendo-numerictextbox 
                    [value]="0"
                    [min]="0"
                    [max]="100"
                    [autoCorrect]="false"
                    name="CostPerUnit"
                    [formControl]="formGroup.get('CostPerUnit')"
                    >
                    </kendo-numerictextbox>
              </ng-template>
            </kendo-grid-column>
            <kendo-grid-column field="Quantity" editor="numeric" title="Labour Qty"></kendo-grid-column>
            <kendo-grid-column field="CostSubTotal" [editable]="false"  title="Cost Sub Total">   
            </kendo-grid-column>
            <kendo-grid-column field="Margin" title="Margin(%)" editor="numeric"></kendo-grid-column>
            <kendo-grid-column field="PricePerUnit" [editable]="false" title="Price Per Unit"></kendo-grid-column>
            <kendo-grid-column field="PriceSubTotal"[editable]="false" title="Price Sub Total"></kendo-grid-column>
            <kendo-grid-column field="Description" title="Description"></kendo-grid-column>
            <kendo-grid-command-column title="command" width="220">
              <ng-template kendoGridCellTemplate let-isNew="isNew">
                <button kendoGridEditCommand class="k-primary">Edit</button>
                <button kendoGridRemoveCommand>Remove</button>
                <button kendoGridSaveCommand [disabled]="formGroup?.invalid">{{ isNew ? 'Add' : 'Update' }}</button>
                <button kendoGridCancelCommand>{{ isNew ? 'Discard changes' : 'Cancel' }}</button>
              </ng-template>
            </kendo-grid-command-column>             
        </kendo-grid> `
})

export class LabourSubSectionEditor implements OnInit {
    public labourClassList: Array<LabourClass> = [];
   
    public errorMessage: string;
    public formGroup: FormGroup = formGroup(new QuotationLabourEntity());
    private editedRowIndex: number;
    private seq: number = 1.00;
    public gridState: State = {
        sort: [],
        skip: 0,
        take: 10
    };
    @Input() selectedSection: Section;
    @Input() type: string;
    @Output() notify: EventEmitter<Section> = new EventEmitter<Section>();
    @ViewChild(GridComponent) private grid: GridComponent;

    constructor(private coreDataService: CoreDataService) {
        
    }

    public ngOnInit(): void {
        //this.selectedSection = new Section();
        this.getLabourClasses(this.selectedSection.WorkState);
    }

    getLabourClasses(state) {
        this.coreDataService.getLabourClasses(state)
            .subscribe(
            labourClasses => this.labourClassList = labourClasses,
            error => this.errorMessage = <any>error);
    }

    //provide template data  for selection from dropdown
    public LabourClassList(id: String): any {
        return this.labourClassList.find(x => x.Code === id);
    }
    
    protected editHandler({ sender, rowIndex, dataItem }) {
        this.closeEditor(sender);

        this.formGroup = formGroup(dataItem);
        this.formGroup.valueChanges.subscribe(data => {
            if (data.LabourClass != "") {
                this.formGroup.value.CostPerUnit = this.labourClassList.find(x => x.Code === this.formGroup.value.Class).Rate;
            }
            this.formGroup.value.CostSubTotal = this.formGroup.value.CostPerUnit * data.Quantity;
            this.formGroup.value.PricePerUnit = this.formGroup.value.CostPerUnit;
            if (this.formGroup.value.Margin > 0) {
                this.formGroup.value.PricePerUnit = parseInt(this.formGroup.value.CostPerUnit) + ((this.formGroup.value.CostPerUnit * this.formGroup.value.Margin) / 100);
            }
            this.formGroup.value.PriceSubTotal = this.formGroup.value.PricePerUnit * this.formGroup.value.Quantity;

        })
        this.editedRowIndex = rowIndex;
        sender.editRow(rowIndex, this.formGroup);       
    }

    protected cancelHandler({ sender, rowIndex }) {
        this.closeEditor(sender, rowIndex);
    }

    private closeEditor(grid, rowIndex = this.editedRowIndex) {
        grid.closeRow(rowIndex);
        this.editedRowIndex = undefined;
        this.formGroup = undefined;
    }
    //save labour   
    protected saveHandler({ sender, rowIndex, formGroup, isNew }) {
        const labour: QuotationLabourEntity = formGroup.value;
        this.saveLabour(labour);
        //this.editService.save(product, isNew);
        if (isNew)
        { this.selectedSection.Labours.push(this.formGroup.value); }
        else {
            Object.assign(
                this.selectedSection.Labours.find(({ QuoteLabourID }) => QuoteLabourID === this.formGroup.value.QuoteLabourID),
                this.formGroup.value
            );
        }
        //sum of labour costs for selected section
        this.selectedSection.PriceLabour = this.selectedSection.Labours.map(i => i.PriceSubTotal).reduce((a, b) => a + b, 0);
        sender.closeRow(rowIndex);     
        this.notify.emit(this.selectedSection);
    }

 

    //remove a labour list value from grid
    protected removeHandler({ dataItem }) {
        this.selectedSection.Labours.splice(this.selectedSection.Labours.indexOf(dataItem), 1);
    }

    //add new labour class row in grid
    protected addHandler({ sender }) {
        this.closeEditor(sender);
        if (this.selectedSection.Labours == undefined) {
            this.selectedSection.Labours =  [];
        }
        if (this.selectedSection.Labours.length > 0) {
            this.seq = this.selectedSection.Labours[this.selectedSection.Labours.length - 1].QuoteLabourID + 1;
        }
        this.formGroup = formGroup(new QuotationLabourEntity(this.seq,this.selectedSection.QuoteSectionID, "", 0, 0, 0, this.selectedSection.MarginActualPercentage, 0, 0, ""));
        this.formGroup.valueChanges.subscribe(data => {
            if (data.LabourClass != "") {
                this.formGroup.value.CostPerUnit = this.labourClassList.find(x => x.Code === this.formGroup.value.Class).Rate;
            }
            this.formGroup.value.CostSubTotal = this.formGroup.value.CostPerUnit * data.Quantity;
            this.formGroup.value.PricePerUnit = this.formGroup.value.CostPerUnit;
            if (this.formGroup.value.Margin > 0) {
                this.formGroup.value.PricePerUnit = parseInt(this.formGroup.value.CostPerUnit) + ((this.formGroup.value.CostPerUnit * this.formGroup.value.Margin) / 100);
            }
            this.formGroup.value.PriceSubTotal = this.formGroup.value.PricePerUnit * this.formGroup.value.Quantity;
           
        })
        sender.addRow(this.formGroup);
    }

    saveLabour(labour) {
        this.coreDataService.saveLabour(labour)
            .subscribe(
            seq => {

            },
            error => this.errorMessage = <any>error);
    }

}
