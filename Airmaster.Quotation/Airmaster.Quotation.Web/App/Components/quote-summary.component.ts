﻿import { Component, Input, OnInit } from '@angular/core';
import { Quote } from './../Model/quote';
@Component({
    selector: 'quote-summary',
    template: `
 <table class="table table-striped priceSummary" *ngIf="quote.Pricing">
           
            <tbody>
                <tr>
                    <td>Price Labour</td>
                    <td>{{quote.Pricing.PriceLabours |  currency:'USD':true}}</td>
                </tr>
                <tr>
                    <td>Price Materials</td>
                    <td>{{quote.Pricing.PriceMaterials |  currency:'USD':true}}</td>
                </tr>
                <tr>
                    <td>Price Additional</td>
                    <td> {{quote.Pricing.PriceAdditional |  currency:'USD':true}}</td>
                </tr>
                <tr>
                    <td>Price Total(Ex GST)</td>
                    <td> {{quote.Pricing.PriceTotal_Ex_GST |  currency:'USD':true}}</td>
                </tr>
                <tr>
                    <td>Weighted Total</td>
                    <td> {{quote.Pricing.WeightedTotal |  currency:'USD':true}}</td>
                </tr>
                <tr>
                    <td>GST Total</td>
                    <td> {{quote.Pricing.GST_Total |  currency:'USD':true}}</td>
                </tr>
                <tr>
                    <td>Price Total(Inc GST)</td>
                    <td> {{quote.Pricing.PriceTotal_Inc_GST |  currency:'USD':true}}</td>
                </tr>
                <tr>
                    <td>Margin Actual(%)</td>
                    <td> {{quote.Pricing.MarginActual_Percentage}}</td>
                </tr>
                <tr>
                    <td>Margin Actual($)</td>
                    <td> {{quote.Pricing.MarginActual |  currency:'USD':true}}</td>
                </tr>
                <tr>
                    <td>Markup Actual(%)</td>
                    <td> {{quote.Pricing.MarkUpActual}}</td>
                </tr>
            </tbody>
        </table>
`
})
export class QuoteSummary implements OnInit {
    @Input() quote: Quote;
    public ngOnInit(): void {

    }

}


