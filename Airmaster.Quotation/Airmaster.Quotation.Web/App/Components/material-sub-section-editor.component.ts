﻿import { Component, OnInit, Inject, Input, Output, EventEmitter, ElementRef, ViewChild } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AddEvent, EditEvent, GridComponent } from '@progress/kendo-angular-grid';
import { State } from '@progress/kendo-data-query';
import { ProductDropDown } from './../Model/quote';
import { CoreDataService } from './../Service/core-data.service';

import { QuotationMaterialEntity } from './../Model/Section/material';
import { Section } from './../Model/section';
import { SharedDataService } from './../Service/shared-data.service';

const formGroup = dataItem => new FormGroup({
    'QuoteMaterialID': new FormControl(dataItem.QuoteMaterialID, Validators.required),
    'Product': new FormControl(dataItem.Product, Validators.required),
    'CostPerUnit': new FormControl(dataItem.CostPerUnit, Validators.compose([Validators.required])),
    'Quantity': new FormControl(dataItem.Quantity, Validators.compose([Validators.required])),
    'CostSubTotal': new FormControl(dataItem.CostSubTotal, Validators.compose([Validators.required])),
    'Margin': new FormControl(dataItem.Margin, Validators.compose([Validators.required])),
    'PricePerUnit': new FormControl(dataItem.PricePerUnit, Validators.compose([Validators.required])),
    'PriceSubTotal': new FormControl(dataItem.PriceSubTotal, Validators.compose([Validators.required])),
    'Creditor': new FormControl(dataItem.Creditor)
});

@Component({
    selector: 'material-sub-section-grid',
    template: `
            <kendo-grid
                      [data]="selectedSectionMtrl.Materials"
                      [height]="300"
                      [pageSize]="gridState.take" [skip]="gridState.skip" [sort]="gridState.sort"
                      [pageable]="true" [sortable]="true"
                      (dataStateChange)="onStateChange($event)"
                      (edit)="editHandler($event)" (cancel)="cancelHandler($event)"
                      (save)="saveHandler($event)" (remove)="removeHandler($event)"
                      (add)="addHandler($event)" >
                    <ng-template kendoGridToolbarTemplate>
                    <button kendoGridAddCommand>Add new</button>
                    </ng-template>    
           
                    <kendo-grid-column field="QuoteMaterialID" [editable]="false" title="SEQ"  width="50"></kendo-grid-column>

                    <kendo-grid-column field="Product"  title="Product" width="200">
                    <ng-template kendoGridCellTemplate let-dataItem>
                    {{ProductList(dataItem.Product)?.Code}} 
                    </ng-template>
                    <ng-template kendoGridEditTemplate 
                    let-dataItem="dataItem"
                    let-formGroup="formGroup">
                    <kendo-dropdownlist 
                    [data]="products"
                    textField="Code"
                    valueField="ProductID"
                    [valuePrimitive]="true"
                    [formControl]="formGroup.get('Product')">                
                    </kendo-dropdownlist>
                    </ng-template>
                    </kendo-grid-column>           
                    <kendo-grid-column field="CostPerUnit" editor="numeric" title="Cost Per Unit"></kendo-grid-column>
                    <kendo-grid-column field="Quantity" editor="numeric" title="Qty"></kendo-grid-column>
                    <kendo-grid-column field="CostSubTotal"[editable]="false" title="Cost Sub Total"></kendo-grid-column>
                    <kendo-grid-column field="Margin" title="Margin(%)"  editor="numeric"></kendo-grid-column>
                    <kendo-grid-column field="PricePerUnit" [editable]="false" title="Price Per Unit"></kendo-grid-column>
                    <kendo-grid-column field="PriceSubTotal"[editable]="false" title="Price Sub Total"></kendo-grid-column>
                    <kendo-grid-column field="Creditor" width="120" title="Creditor">
                    <ng-template kendoGridCellTemplate let-dataItem>
                    {{dataItem.Creditor}} 
                    </ng-template>
                    <ng-template kendoGridEditTemplate 
                    let-dataItem="dataItem"
                    let-formGroup="formGroup">
                    <kendo-combobox [data]="creditors"
                    [allowCustom]="true"
                    [popupSettings]="{ width: 'auto' }"
                    [valuePrimitive]="true"
                    [formControl]="formGroup.get('Creditor')">
                    </kendo-combobox>
                    </ng-template>
                    </kendo-grid-column>
                    <kendo-grid-command-column title="command" width="220">
                    <ng-template kendoGridCellTemplate let-isNew="isNew">
                    <button kendoGridEditCommand class="k-primary">Edit</button>
                    <button kendoGridRemoveCommand>Remove</button>
                    <button kendoGridSaveCommand [disabled]="formGroup?.invalid">{{ isNew ? 'Add' : 'Update' }}</button>
                    <button kendoGridCancelCommand>{{ isNew ? 'Discard changes' : 'Cancel' }}</button>
                    </ng-template>
                    </kendo-grid-command-column>
        </kendo-grid>`
})

export class MaterialSubSectionEditor implements OnInit {
    products: Array<ProductDropDown> = [];
   
    creditors: Array<String> = [];
    public errorMessage: string;
    public formGroup: FormGroup = formGroup(new QuotationMaterialEntity());
    private editedRowIndex: number;
    private seq: number = 1.00;
    public gridState: State = {
        sort: [],
        skip: 0,
        take: 10
    };
    @Input() selectedSectionMtrl: Section;
    @Input() type: string;
    @Output() notify: EventEmitter<Section> = new EventEmitter<Section>();
    @ViewChild(GridComponent) private grid: GridComponent;

    constructor(private coreDataService: CoreDataService, private sharedDataService: SharedDataService) {
    }

    public ngOnInit(): void {
      
        this.getProducts();
        this.creditors = this.sharedDataService.getCreditors();
      
    }

    getProducts() {
        this.coreDataService.getProducts()
            .subscribe(
            products => {
                this.products = products;
                this.sharedDataService.setProductList(this.products);
            },
            error => this.errorMessage = <any>error);
    }

    public ProductList(id: number): any {
        if (id != undefined) {
            return this.products.find(x => x.ProductID === id);
        }
    }

    //public CreditorList(id: any): any {
    //    if (id != undefined) {
    //        return this.creditors.find(x => x.Code === id);
    //    }
    //}

   
    protected editHandler({ sender, rowIndex, dataItem }) {
        this.closeEditor(sender);

        this.formGroup = formGroup(dataItem);
        this.formGroup.valueChanges.subscribe(data => {
            if (data.Product != "") {
                
                this.formGroup.value.CostPerUnit  = this.products.find(x => x.ProductID === this.formGroup.value.Product).CostPerUnit;
                
            }
            this.formGroup.value.CostSubTotal = this.formGroup.value.CostPerUnit * data.Quantity;
            this.formGroup.value.PricePerUnit = this.formGroup.value.CostPerUnit;
            if (this.formGroup.value.Margin > 0) {
                this.formGroup.value.PricePerUnit = parseInt(this.formGroup.value.CostPerUnit) + ((this.formGroup.value.CostPerUnit * this.formGroup.value.Margin) / 100);
            }
            this.formGroup.value.PriceSubTotal = this.formGroup.value.PricePerUnit * this.formGroup.value.Quantity;

        })
        this.editedRowIndex = rowIndex;
        sender.editRow(rowIndex, this.formGroup);
    }

    protected cancelHandler({ sender, rowIndex }) {
        this.closeEditor(sender, rowIndex);
    }

    private closeEditor(grid, rowIndex = this.editedRowIndex) {
        grid.closeRow(rowIndex);
        this.editedRowIndex = undefined;
        this.formGroup = undefined;
    }
    //save labour   
    protected saveHandler({ sender, rowIndex, formGroup, isNew }) {
        const material: QuotationMaterialEntity = formGroup.value;
        this.saveMaterial(material);
        if (isNew)
        { this.selectedSectionMtrl.Materials.push(this.formGroup.value); }
        else {
            Object.assign(
                this.selectedSectionMtrl.Materials.find(({ QuoteMaterialID }) => QuoteMaterialID === this.formGroup.value.QuoteMaterialID),
                this.formGroup.value
            );
        }
        //sum of labour costs for selected section
        this.selectedSectionMtrl.PriceMaterial = this.selectedSectionMtrl.Materials.map(i => i.PriceSubTotal).reduce((a, b) => a + b, 0);
        sender.closeRow(rowIndex);
        this.notify.emit(this.selectedSectionMtrl);
    }

    saveMaterial(material) {
        this.coreDataService.saveMaterial(material)
            .subscribe(
            seq => {

            },
            error => this.errorMessage = <any>error);
    }

    //remove a labour list value from grid
    protected removeHandler({ dataItem }) {
        this.selectedSectionMtrl.Materials.splice(this.selectedSectionMtrl.Materials.indexOf(dataItem), 1);
    }

    //add new labour class row in grid
    protected addHandler({ sender }) {
        this.closeEditor(sender);
        if (this.selectedSectionMtrl.Materials == undefined) {
            this.selectedSectionMtrl.Materials = [];
        }
        if (this.selectedSectionMtrl.Materials.length > 0) {
            this.seq = this.selectedSectionMtrl.Materials[this.selectedSectionMtrl.Materials.length - 1].QuoteMaterialID + 1;
        }
        this.formGroup = formGroup(new QuotationMaterialEntity(this.seq,this.selectedSectionMtrl.QuoteSectionID, "", 0, 0, 0, this.selectedSectionMtrl.MarginActualPercentage, 0, 0, ""));
        this.formGroup.value.CostPerUnit = 0;
        this.formGroup.valueChanges.subscribe(data => {
            if (data.Product != "") {
                this.formGroup.value.CostPerUnit = this.products.find(x => x.ProductID === this.formGroup.value.Product).CostPerUnit;
            }
            this.formGroup.value.CostSubTotal = this.formGroup.value.CostPerUnit * data.Quantity;
            this.formGroup.value.PricePerUnit = this.formGroup.value.CostPerUnit;
            if (this.formGroup.value.Margin > 0) {
                this.formGroup.value.PricePerUnit = parseInt(this.formGroup.value.CostPerUnit) + ((this.formGroup.value.CostPerUnit * this.formGroup.value.Margin) / 100);
            }
            this.formGroup.value.PriceSubTotal = this.formGroup.value.PricePerUnit * this.formGroup.value.Quantity;

        })
        sender.addRow(this.formGroup);
    }
}
