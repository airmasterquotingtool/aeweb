﻿import { Component } from '@angular/core';
import { RouterModule } from '@angular/router';
import { MenuItem,QuotationMenus } from './../Model/quotation-menus'
@Component({
    selector: 'menu-bar',
    templateUrl: `./../../App/Templates/menu-bar.component.html`,
})
export class MenuBarComponent {
   menuViewModel = new QuotationMenus(menus);
}
const menus: MenuItem[] = [
    { MenuText: "List", MenuUrl: 'ListOfQuotes' },
    { MenuText: "New", MenuUrl: 'CreateQuote' },
    { MenuText: "Delete", MenuUrl: 'Quotation/CreateQuote' },
    { MenuText: "Activate Quote", MenuUrl: 'Quotation/CreateQuote' },
    { MenuText: "Copy Or Revise Quote", MenuUrl: 'Quotation/CreateQuote' },
    { MenuText: "MultiQuote", MenuUrl: 'Quotation/CreateQuote' },
    { MenuText: "Set Margin Actual(%)", MenuUrl: 'Quotation/CreateQuote' },
    { MenuText: "Set Price Total(%)", MenuUrl: 'Quotation/CreateQuote' },
    { MenuText: "Quote Document", MenuUrl: 'Quotation/CreateQuote' },
    { MenuText: "Refresh Totals", MenuUrl: 'Quotation/CreateQuote' },    
];