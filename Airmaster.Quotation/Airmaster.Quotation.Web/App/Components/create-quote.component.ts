﻿import { Component, OnInit} from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { SharedDataService } from './../Service/shared-data.service';
import { Quote, QuotePrice} from './../Model/quote';
import { Section } from './../Model/section';
import { CoreDataService } from './../Service/core-data.service';
@Component(
    {
        selector: 'create-quote',
        templateUrl: './../../App/Templates/create-quote.component.html',
       
    }
)
export class CreateQuoteComponent implements OnInit {
    quote: Quote = new Quote();
    quoteReference: string;
    errorMessage: string;
    onNotify(newQuote: Quote): void {   
      
        this.quote = newQuote;
        if (this.quote.Pricing != undefined) {

            this.quote.Pricing.PriceLabours = this.quote.Sections.map(i => i.PriceLabour).reduce((a, b) => a + b, 0);
            this.quote.Pricing.PriceMaterials = this.quote.Sections.map(i => i.PriceMaterial).reduce((a, b) => a + b, 0);
            this.quote.Pricing.PriceAdditional = this.quote.Sections.map(i => i.PriceAdditional).reduce((a, b) => a + b, 0);
            this.quote.Pricing.PriceTotal_Ex_GST = this.quote.Pricing.PriceLabours
                + this.quote.Pricing.PriceMaterials
                + this.quote.Pricing.PriceAdditional;
            this.quote.Pricing.WeightedTotal = this.quote.Pricing.PriceTotal_Ex_GST - this.quote.Pricing.PriceMaterials;
        }
        else {
            this.quote.Pricing = new QuotePrice(0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
        }
    }
    constructor(private coreDataService: CoreDataService,private sharedDataServcie: SharedDataService)
    {
        let sharedQuote = this.sharedDataServcie.getData();
        if (sharedQuote != undefined)
        {
            this.quote = sharedQuote;
            this.quoteReference = this.quote.WorkStateCd + this.quote.CreatedBy + '000' + this.quote.QuoteID;
        }
    }
    ngOnInit()
    {
        if (this.quote.QuoteID != undefined) {
            this.GetSections(this.quote.QuoteID);
        }
    }
    GetSections(quoteID) {
        this.coreDataService.getSections(quoteID)
            .subscribe(
            Sections => {
                this.quote.Sections = Sections;
                if (this.quote.Sections.length > 0) {
                    for (let section of this.quote.Sections) {
                        this.GetLabours(section.QuoteSectionID);
                    }
                }
                this.onNotify(this.quote);
            },
            error => this.errorMessage = <any>error);
    }

    GetLabours(QuoteSectionID) {
        this.coreDataService.getLabours(QuoteSectionID)
            .subscribe(
            labours => {
                this.quote.Sections.filter(obj => {return obj.QuoteSectionID == QuoteSectionID})[0].Labours=labours;
                
            },
            error => this.errorMessage = <any>error);
    }

    onChange() {

    }
    onFocus() {

    }
    onReady() {

    }
    onBlur() {

    }
}