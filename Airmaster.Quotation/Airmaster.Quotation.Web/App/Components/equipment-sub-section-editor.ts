﻿import { Component, OnInit, Inject, Input, Output, EventEmitter, ElementRef, ViewChild } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AddEvent, EditEvent, GridComponent } from '@progress/kendo-angular-grid';
import { State, aggregateBy } from '@progress/kendo-data-query';
import { CoreDataService } from './../Service/core-data.service';
import { Equipment } from './../Model/Section/equipment';
import { Section } from './../Model/section';
import { VendorDropDown } from './../Model/quote';
import { SharedDataService } from './../Service/shared-data.service';
const formGroup = dataItem => new FormGroup({
    'SEQ': new FormControl(dataItem.SEQ, Validators.required),
    'Description': new FormControl(dataItem.EquipmentClass, Validators.required),
    'CostPerUnit': new FormControl(dataItem.CostPerUnit, Validators.required),
        'EquipmentQuantity': new FormControl(dataItem.EquipmentQuantity, Validators.required),
        'CostSubTotal': new FormControl(dataItem.CostSubTotal, Validators.required),
        'Margin': new FormControl(dataItem.Margin, Validators.required),
        'PricePerUnit': new FormControl(dataItem.PricePerUnit, Validators.required),
        'PriceSubTotal': new FormControl(dataItem.PriceSubTotal, Validators.required),
    'Reference': new FormControl(dataItem.Description),
    'Creditor': new FormControl(dataItem.Description)
});

@Component({
    selector: 'equipment-sub-section-grid',
    template: `
             <kendo-grid
                      [data]="selectedSectionEqp.Equipments "
                      [height]="300"
                      [pageSize]="gridState.take" [skip]="gridState.skip" [sort]="gridState.sort"
                      [pageable]="true" [sortable]="true"
                      (dataStateChange)="onStateChange($event)"
                      (edit)="editHandler($event)" (cancel)="cancelHandler($event)"
                      (save)="saveHandler($event)" (remove)="removeHandler($event)"
                      (add)="addHandler($event)"
            >
            <ng-template kendoGridToolbarTemplate>
                <button kendoGridAddCommand>Add new</button>
            </ng-template>    
           <kendo-grid-column field="SEQ" title="SEQ" [editable]="false" width="50"></kendo-grid-column>
            <kendo-grid-column field="Description" title="Description"></kendo-grid-column>
            <kendo-grid-column field="CostPerUnit" editor="numeric" title="Cost Per Unit"></kendo-grid-column>
            <kendo-grid-column field="EquipmentQuantity" editor="numeric" title="Qty"></kendo-grid-column>
            <kendo-grid-column field="CostSubTotal" [editable]="false" title="Cost Sub Total"></kendo-grid-column>
            <kendo-grid-column field="Margin" title="Margin(%)"  editor="numeric" ></kendo-grid-column>
            <kendo-grid-column field="PricePerUnit" [editable]="false" title="Price Per Unit"></kendo-grid-column>
            <kendo-grid-column field="PriceSubTotal" [editable]="false" title="Price Sub Total"></kendo-grid-column>
            <kendo-grid-column field="Reference" title="Reference"></kendo-grid-column>
             <kendo-grid-column field="Creditor" width="120" title="Creditor">
                    <ng-template kendoGridCellTemplate let-dataItem>
                    {{dataItem.Creditor}} 
                    </ng-template>
                    <ng-template kendoGridEditTemplate 
                    let-dataItem="dataItem"
                    let-formGroup="formGroup">
                    <kendo-combobox [data]="creditors"
                    [allowCustom]="true"
                   
                    [popupSettings]="{ width: 'auto' }"
                    [valuePrimitive]="true"
                    [formControl]="formGroup.get('Creditor')">
                    </kendo-combobox>
                    </ng-template>
               </kendo-grid-column>
            <kendo-grid-command-column title="command" width="220">      
                <ng-template kendoGridCellTemplate let-isNew="isNew">
                    <button kendoGridEditCommand class="k-primary">Edit</button>
                    <button kendoGridRemoveCommand>Remove</button>
                    <button kendoGridSaveCommand [disabled]="formGroup?.invalid">{{ isNew ? 'Add' : 'Update' }}</button>
                    <button kendoGridCancelCommand>{{ isNew ? 'Discard changes' : 'Cancel' }}</button>
                </ng-template>
            </kendo-grid-command-column>
        </kendo-grid>`
})

export class EquipmentSubSectionEditor implements OnInit {
 
    public view: Equipment[];
    public errorMessage: string;
    public formGroup: FormGroup = formGroup(new Equipment());
    private editedRowIndex: number;
    private seq: number = 1.00;
    public gridState: State = {
        sort: [],
        skip: 0,
        take: 10
    };
    creditors: Array<String> = [];
    @Input() selectedSectionEqp: Section;
    @Input() type: string;
    @Output() notify: EventEmitter<Section> = new EventEmitter<Section>();
    @ViewChild(GridComponent) private grid: GridComponent;

    constructor(private coreDataService: CoreDataService, private sharedDataService: SharedDataService) {
    }

    public ngOnInit(): void {
        //this.selectedSectionEqp = new Section();
        this.creditors = this.sharedDataService.getCreditors();
    }

    //set creditor value based on dropdown template
    //public CreditorList(id: any): any {
    //    if (id != undefined) {
    //        return this.creditors.find(x => x.Code === id);
    //    }
    //}


    protected editHandler({ sender, rowIndex, dataItem }) {
        this.closeEditor(sender);

        this.formGroup = formGroup(dataItem);
        this.formGroup.valueChanges.subscribe(data => {
           
            this.formGroup.value.CostSubTotal = this.formGroup.value.CostPerUnit * data.EquipmentQuantity;
            this.formGroup.value.PricePerUnit = this.formGroup.value.CostPerUnit;
            if (this.formGroup.value.Margin > 0) {
                this.formGroup.value.PricePerUnit = parseInt(this.formGroup.value.CostPerUnit) + ((this.formGroup.value.CostPerUnit * this.formGroup.value.Margin) / 100);
            }
            this.formGroup.value.PriceSubTotal = this.formGroup.value.PricePerUnit * this.formGroup.value.EquipmentQuantity;
           
        })
        this.editedRowIndex = rowIndex;
        sender.editRow(rowIndex, this.formGroup);
    }

    protected cancelHandler({ sender, rowIndex }) {
        this.closeEditor(sender, rowIndex);
    }

    private closeEditor(grid, rowIndex = this.editedRowIndex) {
        grid.closeRow(rowIndex);
        this.editedRowIndex = undefined;
        this.formGroup = undefined;
    }
    //save labour   
    protected saveHandler({ sender, rowIndex, formGroup, isNew }) {
        const labour: Equipment = formGroup.value;
        //this.editService.save(product, isNew);
        if (isNew)
        { this.selectedSectionEqp.Equipments.push(this.formGroup.value); }
        else {
            Object.assign(
                this.selectedSectionEqp.Equipments.find(({ SEQ }) => SEQ === this.formGroup.value.SEQ),
                this.formGroup.value
            );
        }
        //sum of labour costs for selected section
        this.selectedSectionEqp.PriceAdditional += this.selectedSectionEqp.Equipments.map(i => i.PriceSubTotal).reduce((a, b) => a + b, 0);
        sender.closeRow(rowIndex);
        this.notify.emit(this.selectedSectionEqp);
    }

    //remove a labour list value from grid
    protected removeHandler({ dataItem }) {
        this.selectedSectionEqp.Equipments.splice(this.selectedSectionEqp.Equipments.indexOf(dataItem), 1);
    }

    //add new labour class row in grid
    protected addHandler({ sender }) {
        this.closeEditor(sender);
        if (this.selectedSectionEqp.Equipments==undefined) {
            this.selectedSectionEqp.Equipments = [];
        }
        if (this.selectedSectionEqp.Equipments.length > 0) {
            this.seq = this.selectedSectionEqp.Equipments[this.selectedSectionEqp.Equipments.length - 1].SEQ + 1;
        }
        this.formGroup = formGroup(new Equipment(this.seq, "", 0, 0, 0, this.selectedSectionEqp.MarginActualPercentage, 0, 0, ""));
        this.formGroup.valueChanges.subscribe(data => {
          
            this.formGroup.value.CostSubTotal = this.formGroup.value.CostPerUnit * data.EquipmentQuantity;
            this.formGroup.value.PricePerUnit = this.formGroup.value.CostPerUnit;
            if (this.formGroup.value.Margin > 0) {
                this.formGroup.value.PricePerUnit = parseInt(this.formGroup.value.CostPerUnit) + ((this.formGroup.value.CostPerUnit * this.formGroup.value.Margin) / 100);
            }
            this.formGroup.value.PriceSubTotal = this.formGroup.value.PricePerUnit * this.formGroup.value.EquipmentQuantity;
          
        })
        sender.addRow(this.formGroup);
    }
}
