﻿import { Component, ViewChild, OnInit,Input,Output, EventEmitter } from '@angular/core';
import { Section } from './../Model/section';
import { Observable } from 'rxjs/Rx';
import { Quote, ProductDropDown} from './../Model/quote';
import { GridComponent, GridDataResult, DataStateChangeEvent} from '@progress/kendo-angular-grid';
import { SortDescriptor } from '@progress/kendo-data-query';
import { CoreDataService} from './../Service/core-data.service';
@Component({
    
    selector: 'quote-section',
    templateUrl: `./../../App/Templates/quote-section.component.html`
})
export class QuoteSectionComponent  implements OnInit{
    public view: Observable<GridDataResult>;
    public sort: Array<SortDescriptor> = [];
    public pageSize: number = 10;   
    public skip: number = 0;    
    section: Section;   
    seq: number = 1;
    @Input() quote: Quote;
    @Output() notify: EventEmitter<Quote> = new EventEmitter<Quote>();
    @ViewChild(GridComponent) grid: GridComponent;
    errorMessage: string;
    public selectedSectionRow: Section;  
    constructor(private coreDataService: CoreDataService) {
        
    }
    public ngOnInit(): void {
       
    
        if (this.quote.Sections.length == 0) {
            this.section = new Section(this.seq, this.quote.QuoteID, "", true, this.quote.LocationID, "", "", this.quote.WorkStateCd
                , 0, 0, 0, 0, 0, 0, this.quote.Margin, 0);
            this.quote.Sections.push(this.section);
        }
        this.selectedSectionRow = this.quote.Sections[0];
      
    }
    public dataStateChange({ skip, take, sort }: DataStateChangeEvent): void {
        // Save the current state of the Grid component
        this.skip = skip;
        this.pageSize = take;
        this.sort = sort;

        // Reload the data with the new state
        //this.loadData();
    }
    public onDetailExpand(e):void
    {
        this.selectedSectionRow = this.quote.Sections[e.index];
    }

    public ngAfterViewInit(): void {
        // Expand the first row initially
    
        this.grid.expandRow(0);
    }

    AddSection(): void {
        if (this.quote.Sections != undefined)
        {
            if (this.quote.Sections.length > 0)
            {
                this.seq = this.quote.Sections[this.quote.Sections.length - 1].QuoteSectionID + 1;
            }           
        }
        else
        {
            this.quote.Sections = [];             
        }
        this.section = new Section(this.seq, this.quote.QuoteID, "",true, this.quote.LocationID, "","", this.quote.WorkStateCd
            , 0, 0, 0, 0, 0, 0, this.quote.Margin, 0);
        this.quote.Sections.push(this.section); 
        this.grid.expandRow(this.grid.view.length - 1);
    }
    RemoveSection(): void {
        this.quote.Sections.pop();
    }
    //on selection change update subsection tab
    public onSelect(e) {
        console.log(this.quote.Sections[e.index])
        this.selectedSectionRow = this.quote.Sections[e.index];
    }

    onNotify(newSection: Section): void {
        this.section = newSection;
        this.saveSectionGeneralInfo(this.section);
      //assign new section to quote
        this.grid.collapseRow(this.section.QuoteSectionID - 1);
            Object.assign(
                this.quote.Sections.find(({ QuoteSectionID }) => QuoteSectionID === this.section.QuoteSectionID),
                this.section
            );
            this.notify.emit(this.quote);
    }

    saveSectionGeneralInfo(section) {
        this.coreDataService.saveQuoteSectionInfo(section)
            .subscribe(
            quoteID => { this.quote.QuoteID = quoteID },
            error => this.errorMessage = <any>error);
    }
   
   
}