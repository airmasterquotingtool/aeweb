﻿import { Component, Input,Output, ViewChild, OnInit,EventEmitter } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { Quote, EquipmentDropDown } from './../Model/quote';
import { Section } from './../Model/section';
import { CoreDataService } from './../Service/core-data.service';
import { SharedDataService } from './../Service/shared-data.service';
import { Observable } from 'rxjs/Rx';
import { GridDataResult, GridComponent, PageChangeEvent } from '@progress/kendo-angular-grid';

@Component(
    {
        selector: 'section-general-info',
        templateUrl: './../../App/Templates/section-general-info-input.component.html',
    }
)
export class SectionGeneralInfo {
    @Input() section: Section;
    equipments: Array<String>=[];
    errorMessage: string = '';
    @Output() notify: EventEmitter<Section> = new EventEmitter<Section>();
    txtEditorConfig = { width:"90%", toolbar: 'Basic', disableNativeSpellChecker: true, scayt_autoStartup: true, filebrowserImageUploadUrl: '~/NodeModules' };
    constructor(private coreDataService: CoreDataService, private sharedDataService: SharedDataService
    ) {
       
    }

    public ngOnInit(): void {
        if (this.section != undefined)
        {
            this.equipments = this.sharedDataService.getEquipments();
           
        }

    }
   


    onClick() {
        this.saveSection(this.section);
        this.notify.emit(this.section);
    }
    saveSection(section)
    {
        this.coreDataService.saveQuoteSectionInfo(section)
            .subscribe(
            QuoteSectionID => {
                this.section.QuoteSectionID = QuoteSectionID;

            },
            error => this.errorMessage = <any>error);
    }

    onChange() {

    }
    onFocus() {

    }
    onReady()
    {

    }
    onBlur() {

    }
}
const LabourColDefs: string[] = ["SEQ", "Labour Class", "Cost Per Unit", "Labour", "Cost Sub Total",
    "Margin %", "Price Per Unit", "Price Sub Total", "Description"];

