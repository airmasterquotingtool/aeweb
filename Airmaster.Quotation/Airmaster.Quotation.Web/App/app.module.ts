﻿import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule, Routes } from '@angular/router';  //routes packages live here
import { NgbModule } from '@ng-bootstrap/ng-bootstrap'; //bootstrap components live here
import { FormsModule } from '@angular/forms'; //<-- NgModel lives here
import { HttpModule, JsonpModule } from '@angular/http';
import { NgHttpLoaderModule } from 'ng-http-loader/ng-http-loader.module';
import { ButtonsModule } from '@progress/kendo-angular-buttons';
import { GridModule } from '@progress/kendo-angular-grid';
import { DateInputsModule } from '@progress/kendo-angular-dateinputs';
import { InputsModule } from '@progress/kendo-angular-inputs';
import { CKEditorModule } from 'ng2-ckeditor';
import { AppComponent } from './Components/app.component';
import { MenuBarComponent } from './Components/menu-bar.component';
import { CreateQuoteComponent } from './Components/create-quote.component';
import { QuoteGeneralInfoComponent } from './Components/quote-general-info-input.component';
import { QuoteSectionComponent } from './Components/quote-section.component';
import { SectionGeneralInfo } from './Components/section-general-info-input.component';
import { DialogModule } from '@progress/kendo-angular-dialog';
import { ReactiveFormsModule } from '@angular/forms';
import { DropDownListModule,AutoCompleteModule,ComboBoxModule } from '@progress/kendo-angular-dropdowns';
import { LabourSubSectionEditor } from './Components/labour-sub-section-editor.component';
import { MaterialSubSectionEditor } from './Components/material-sub-section-editor.component';
import { EquipmentSubSectionEditor } from './Components/equipment-sub-section-editor';
import { SubContractorSubSectionEditor } from './Components/subcontractor-sub-section-editor';
import { OtherSubSectionEditor } from './Components/others-sub-section-editor';
import { QuoteList } from './Components/quote-list.component';
import { QuoteSummary } from './Components/quote-summary.component';
import { SharedDataService } from './Service/shared-data.service';
import { CoreDataService } from './Service/core-data.service';
@NgModule({
    imports: [BrowserModule, FormsModule, HttpModule, NgHttpLoaderModule,  JsonpModule, BrowserAnimationsModule,
        ButtonsModule, GridModule, DialogModule, InputsModule,
        DateInputsModule,
        CKEditorModule, DropDownListModule, AutoCompleteModule, ComboBoxModule, ReactiveFormsModule,

        NgbModule.forRoot(), RouterModule.forRoot([
            {
                path: 'CreateQuote',
                component: CreateQuoteComponent
               
            },
            {
                path: 'ListOfQuotes',
                component: QuoteList
            }
           
           

        ])],
    declarations: [
        AppComponent,
        CreateQuoteComponent,
        QuoteGeneralInfoComponent,
        QuoteSectionComponent,
        SectionGeneralInfo,
        MenuBarComponent,
        QuoteList, QuoteSummary,
        LabourSubSectionEditor,
        SubContractorSubSectionEditor,
        OtherSubSectionEditor,
        MaterialSubSectionEditor,
        EquipmentSubSectionEditor,
        
      
    ],
    providers: [
        SharedDataService, CoreDataService
    ],
    bootstrap: [AppComponent, MenuBarComponent]
})
export class AppModule { }

