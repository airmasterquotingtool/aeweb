"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var QuotationLabourEntity = (function () {
    function QuotationLabourEntity(QuoteLabourID, QuoteSectionID, Class, CostPerUnit, Quantity, CostSubTotal, Margin, PricePerUnit, PriceSubTotal, Description) {
        this.QuoteLabourID = QuoteLabourID;
        this.QuoteSectionID = QuoteSectionID;
        this.Class = Class;
        this.CostPerUnit = CostPerUnit;
        this.Quantity = Quantity;
        this.CostSubTotal = CostSubTotal;
        this.Margin = Margin;
        this.PricePerUnit = PricePerUnit;
        this.PriceSubTotal = PriceSubTotal;
        this.Description = Description;
    }
    return QuotationLabourEntity;
}());
exports.QuotationLabourEntity = QuotationLabourEntity;
