"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
//export class QuotationSectonEntity {
//    /*[Key]*/
//    public QuoteSectionID: number;
//    public QuoteID: number;
//    public Title: string;
//    public Preferred: number;
//    public LocationID: number;
//    public Equipment: string;
//    public Description: string;
//    public CreationDate: Date;
//    public CreatedBy: number;
//    public UpdationDate: Date;
//    public UpdatedBy: number;
//}
var Section = (function () {
    function Section(QuoteSectionID, QuoteID, Title, Preferred, LocationID, Equipment, Description, WorkState, 
        /**calucated values**/
        PriceLabour, PriceMaterial, PriceAdditional, PriceTotal_Ex_GST, GST_Total, PriceTotal_Inc_GST, MarginActualPercentage, MarginActualAmount, 
        /**end caculated values**/
        Labours, Materials, Equipments, SubContractors, Others, CreationDate, CreatedBy, UpdationDate, UpdatedBy) {
        if (PriceLabour === void 0) { PriceLabour = 0; }
        if (PriceMaterial === void 0) { PriceMaterial = 0; }
        if (PriceAdditional === void 0) { PriceAdditional = 0; }
        if (Labours === void 0) { Labours = []; }
        if (Materials === void 0) { Materials = []; }
        if (Equipments === void 0) { Equipments = []; }
        if (SubContractors === void 0) { SubContractors = []; }
        if (Others === void 0) { Others = []; }
        this.QuoteSectionID = QuoteSectionID;
        this.QuoteID = QuoteID;
        this.Title = Title;
        this.Preferred = Preferred;
        this.LocationID = LocationID;
        this.Equipment = Equipment;
        this.Description = Description;
        this.WorkState = WorkState;
        this.PriceLabour = PriceLabour;
        this.PriceMaterial = PriceMaterial;
        this.PriceAdditional = PriceAdditional;
        this.PriceTotal_Ex_GST = PriceTotal_Ex_GST;
        this.GST_Total = GST_Total;
        this.PriceTotal_Inc_GST = PriceTotal_Inc_GST;
        this.MarginActualPercentage = MarginActualPercentage;
        this.MarginActualAmount = MarginActualAmount;
        this.Labours = Labours;
        this.Materials = Materials;
        this.Equipments = Equipments;
        this.SubContractors = SubContractors;
        this.Others = Others;
        this.CreationDate = CreationDate;
        this.CreatedBy = CreatedBy;
        this.UpdationDate = UpdationDate;
        this.UpdatedBy = UpdatedBy;
    }
    return Section;
}());
exports.Section = Section;
