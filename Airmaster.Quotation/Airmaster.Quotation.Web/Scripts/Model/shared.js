"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
//generic class to handle list values for drop down throughout application's typescript
var DropDownModel = (function () {
    function DropDownModel(itemList, id, cssClass, SelectedItem) {
        this.itemList = itemList;
        this.id = id;
        this.cssClass = cssClass;
        this.SelectedItem = SelectedItem;
    }
    return DropDownModel;
}());
exports.DropDownModel = DropDownModel;
var Item = (function () {
    function Item(value, text) {
        this.value = value;
        this.text = text;
    }
    return Item;
}());
exports.Item = Item;
//status of a quote under draft
exports.QuoteStatus = new DropDownModel([
    new Item("Draft", ""),
    new Item("Awaiting Approval", ""),
    new Item("Canceled", ""),
    new Item("Revised", ""),
    new Item("Won", ""),
    new Item("Lost", "")
], "ddlQuoteStatus", "", "");
//status of lost quote
exports.LostQuoteStatus = new DropDownModel([
    new Item("Price", "Price"),
    new Item("Relationship", "Relationship"),
    new Item("Reduction in price", "Reduction in price"),
    new Item("Contract Review", "Contract Review - Not resale"),
    new Item("Canceled", "Canceled"),
    new Item("Revised", "Revised")
], "ddlLostQuoteStatus", "", "");
//priority levels of quote
exports.QuotePriority = new DropDownModel([
    new Item("L", "Low"),
    new Item("M", "Medium"),
    new Item("H", "High"),
    new Item("C", "Compliance"),
], "ddlQuotePriority", "", "");
//memo type 
exports.MemoType = new DropDownModel([
    new Item("RFI", "Response for Information"),
    new Item("RFQ", "Response for Quote")
], "ddlMemoType", "", "");
//probability percentage
exports.Probability = new DropDownModel([
    new Item("0 %", "0 %"),
    new Item("10 %", "10 %"),
    new Item("25 %", "25 %"),
    new Item("50 %", "50 %"),
    new Item("75 %", "75 %"),
    new Item("90 %", "90 %"),
    new Item("100 %", "100 %"),
], "ddlProbability", "", "");
//quote stages
exports.Stage = new DropDownModel([
    new Item("TN", ""),
    new Item("10 %", "Tender Notification"),
    new Item("SB", "Site Briefing"),
    new Item("RFI", "Response for information (RFI)"),
    new Item("QT", "Quotation (Maitnenance Proposal)"),
    new Item("PS", "Proposal Submission"),
    new Item("PC", "Procurement Clarifications"),
    new Item("FN", "Final Negotiations"),
    new Item("NL", "Notification Letter (Win / Lost)"),
], "ddlStages", "", "");
//Location
exports.Location = new DropDownModel([
    new Item("RMIT", "RMIT"),
    new Item("AU", "AU"),
], "ddlLocation", "", "");
//States
exports.States = new DropDownModel([
    new Item("SampleState1", "SampleState1"),
    new Item("SampleState2", "SampleState2"),
], "ddlStates", "", "");
//quote type
exports.Types = new DropDownModel([
    new Item("1", "Service Quoted Work"),
    new Item("2", "Maintance Related"),
], "ddlStates", "", "");
