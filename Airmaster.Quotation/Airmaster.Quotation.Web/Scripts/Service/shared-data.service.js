"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var core_1 = require("@angular/core");
var quote_1 = require("./../Model/quote");
var SharedDataService = (function () {
    function SharedDataService() {
        this.sharingData = new quote_1.Quote();
        this.equipments = [];
        this.productList = Array();
    }
    SharedDataService.prototype.saveData = function (quote) {
        this.sharingData = quote;
    };
    SharedDataService.prototype.setProductList = function (products) {
        this.productList = products;
    };
    SharedDataService.prototype.setCreditors = function (creditors) {
        this.creditors = creditors.map(function (obj) { return obj.Code; });
    };
    SharedDataService.prototype.getCreditors = function () {
        return this.creditors;
    };
    SharedDataService.prototype.setEquipments = function (equipments) {
        this.equipments = equipments.map(function (obj) { return obj.Code; });
    };
    SharedDataService.prototype.getEquipments = function () {
        return this.equipments;
    };
    SharedDataService.prototype.getProductList = function () {
        return this.productList;
    };
    SharedDataService.prototype.getData = function () {
        return this.sharingData;
    };
    SharedDataService = tslib_1.__decorate([
        core_1.Injectable()
    ], SharedDataService);
    return SharedDataService;
}());
exports.SharedDataService = SharedDataService;
