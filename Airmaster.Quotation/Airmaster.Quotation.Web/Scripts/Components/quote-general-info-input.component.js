"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var core_1 = require("@angular/core");
var core_data_service_1 = require("./../Service/core-data.service");
var shared_data_service_1 = require("./../Service/shared-data.service");
var quote_1 = require("./../Model/quote");
var shared_1 = require("./../Model/shared");
var QuoteGeneralInfoComponent = (function () {
    //inject coredataservice
    function QuoteGeneralInfoComponent(coreDataService, sharedDataService) {
        this.coreDataService = coreDataService;
        this.sharedDataService = sharedDataService;
        this.mode = 'Observable';
        this.notify = new core_1.EventEmitter();
        //assign form constants to local variables to be used  in input form
        this.creditors = [];
        this.customerData = Array();
        this.contacts = Array();
        this.technicians = Array();
        this.customers = Array();
        this.divisions = Array();
        this.priority = Array();
        this.stage = Array();
        this.status = Array();
        this.quoteTypes = Array();
        this.probability = Array();
        this.memoType = Array();
        this.states = Array();
        this.min = new Date();
        this.clickMessage = '';
        this.value = new Date();
        this.model = new quote_1.Quote();
        this.type = shared_1.Types;
        this.submitted = false;
        this.isDisabledLocations = false;
        this.defaultLocation = { Code: null, LocationID: null, State: null, CustomerCode: null, Name: "Select Location", Country: null, AddressLine1: null, AddressLine2: null, Pincocde: null };
        this.getCustomers();
        this.getCoreData();
        this.getTechnicians();
    }
    ;
    QuoteGeneralInfoComponent.prototype.ngOnInit = function () {
        //if (this.quote.CustomerID != undefined)
        //{
        //    this.getLocationByCustomerID(this.quote.CustomerID);
        //}
    };
    //on customer change fetch location by customer code using core-data service
    QuoteGeneralInfoComponent.prototype.onCustomerChange = function (value) {
        if (value != undefined) {
            this.quote.CustomerID = value;
            this.quote.LocationID = undefined;
            if (value == this.defaultLocation.LocationID) {
                // this.isDisabledLocations = true;
                this.locations = [];
            }
            else {
                // this.isDisabledLocations = false;
                this.getLocationByCustomerID(value);
            }
        }
    };
    QuoteGeneralInfoComponent.prototype.onContactChange = function (value) {
        if (value != undefined) {
            this.quote.ContactID = value;
        }
    };
    QuoteGeneralInfoComponent.prototype.onLocationChange = function (value) {
        if (value != undefined) {
            this.getContacts(value);
            var locState_1 = this.locations.filter(function (o) { return o.LocationID == value; })[0].State;
            var wState = this.states.filter(function (obj) { return obj.Code == locState_1; })[0];
            if (wState != undefined) {
                this.quote.WorkStateCd = wState.CodeID;
                this.getDivisions(this.quote.WorkStateCd);
            }
            this.getEquipments(value);
        }
    };
    //filter change 
    QuoteGeneralInfoComponent.prototype.onCustomerFilterChange = function (value) {
        if (value == "" || value == undefined) {
            this.customerData = this.customers;
        }
        else {
            this.customerData = this.customers.filter(function (s) { return s.Name.toLowerCase().indexOf(value.toLowerCase()) !== -1; });
        }
    };
    QuoteGeneralInfoComponent.prototype.onWorkStateChange = function (value) {
        if (value != undefined) {
            this.getDivisions(value);
            var stateCode = this.states.filter(function (obj) { return obj.CodeID === value; })[0].Code;
            this.getCreditors(stateCode);
        }
    };
    QuoteGeneralInfoComponent.prototype.onDivisionChange = function (value) {
        if (value != undefined) {
            this.quote.DivisionID = value;
            this.quote.Margin = this.divisions.filter(function (obj) { return obj.DivisionID === value; })[0].DefaultMargin;
            this.quote.Preferred = this.quote.Margin;
        }
    };
    //fill up common dropdown form fields
    QuoteGeneralInfoComponent.prototype.getCustomers = function () {
        var _this = this;
        this.coreDataService.getCustomers()
            .subscribe(function (customers) {
            _this.customers = customers;
            _this.customerData = _this.customers;
            if (_this.quote.CustomerID != undefined) {
                _this.getLocationByCustomerID(_this.quote.CustomerID);
            }
        }, function (error) { return _this.errorMessage = error; });
    };
    QuoteGeneralInfoComponent.prototype.getEquipments = function (locationID) {
        var _this = this;
        this.coreDataService.getEquipments(locationID)
            .subscribe(function (equipments) {
            _this.sharedDataService.setEquipments(equipments);
        }, function (error) { return _this.errorMessage = error; });
    };
    QuoteGeneralInfoComponent.prototype.getTechnicians = function () {
        var _this = this;
        this.coreDataService.getTechnicians()
            .subscribe(function (technicians) { return _this.technicians = technicians; }, function (error) { return _this.errorMessage = error; });
    };
    QuoteGeneralInfoComponent.prototype.getContacts = function (locationID) {
        var _this = this;
        this.coreDataService.getContacts(locationID)
            .subscribe(function (contacts) { return _this.contacts = contacts; }, function (error) { return _this.errorMessage = error; });
    };
    QuoteGeneralInfoComponent.prototype.getDivisions = function (state) {
        var _this = this;
        if (state != undefined) {
            var stateCode = this.states.filter(function (obj) { return obj.CodeID == state; })[0].Code;
            this.coreDataService.getDivisions(stateCode)
                .subscribe(function (divisions) { return _this.divisions = divisions; }, function (error) { return _this.errorMessage = error; });
        }
    };
    //get dropdown list values for quote creation
    QuoteGeneralInfoComponent.prototype.getCoreData = function () {
        var _this = this;
        this.coreDataService.getCoreData()
            .subscribe(function (codeData) {
            _this.codeData = codeData;
            _this.stage = _this.codeData.filter(function (obj) { return obj.CodeTypeID == quote_1.CodeName.QuoteStage; });
            _this.priority = _this.codeData.filter(function (obj) { return obj.CodeTypeID == quote_1.CodeName.QuotePriority; });
            _this.status = _this.codeData.filter(function (obj) { return obj.CodeTypeID == quote_1.CodeName.QuoteStatus; });
            _this.probability = _this.codeData.filter(function (obj) { return obj.CodeTypeID == quote_1.CodeName.QuoteProbability; });
            _this.memoType = _this.codeData.filter(function (obj) { return obj.CodeTypeID == quote_1.CodeName.MemoType; });
            _this.states = _this.codeData.filter(function (obj) { return obj.CodeTypeID == quote_1.CodeName.State; });
            _this.quoteTypes = _this.codeData.filter(function (obj) { return obj.CodeTypeID == quote_1.CodeName.QuoteType; });
            if (_this.quote.QuoteID != undefined) {
                _this.getDivisions(_this.quote.WorkStateCd);
                var stateID_1 = _this.quote.WorkStateCd;
                var stateCode = _this.states.filter(function (obj) { return obj.CodeID === stateID_1; })[0].Code;
                _this.getCreditors(stateCode);
            }
        }, function (error) { return _this.errorMessage = error; });
    };
    //get location based on customer code 
    QuoteGeneralInfoComponent.prototype.getLocationByCustomerID = function (customerID) {
        var _this = this;
        this.coreDataService.getLocationByCustomerID(customerID)
            .subscribe(function (locations) {
            _this.locations = locations;
            if (_this.quote.LocationID != undefined) {
                _this.getContacts(_this.quote.LocationID);
            }
        }, function (error) { return _this.errorMessage = error; });
    };
    QuoteGeneralInfoComponent.prototype.getCreditors = function (state) {
        var _this = this;
        this.coreDataService.getCreditors(state)
            .subscribe(function (creditors) {
            _this.sharedDataService.setCreditors(creditors);
        }, function (error) { return _this.errorMessage = error; });
    };
    //emmit quote to parent component
    QuoteGeneralInfoComponent.prototype.onQuoteGeneralInfoSubmit = function () {
        //this.quote.QuoteID = "A12145";
        this.saveQuoteGeneralInfo(this.quote);
        this.notify.emit(this.quote);
    };
    QuoteGeneralInfoComponent.prototype.saveQuoteGeneralInfo = function (quote) {
        var _this = this;
        this.coreDataService.saveQuoteGeneralInfo(quote)
            .subscribe(function (quoteID) {
            if (_this.quote.QuoteID == undefined)
                _this.quote.QuoteID = quoteID;
        }, function (error) { return _this.errorMessage = error; });
    };
    Object.defineProperty(QuoteGeneralInfoComponent.prototype, "diagnostic", {
        // TODO: Remove this when we're done
        get: function () { return JSON.stringify(this.quote); },
        enumerable: true,
        configurable: true
    });
    tslib_1.__decorate([
        core_1.Input(),
        tslib_1.__metadata("design:type", quote_1.Quote)
    ], QuoteGeneralInfoComponent.prototype, "quote", void 0);
    tslib_1.__decorate([
        core_1.Output(),
        tslib_1.__metadata("design:type", core_1.EventEmitter)
    ], QuoteGeneralInfoComponent.prototype, "notify", void 0);
    QuoteGeneralInfoComponent = tslib_1.__decorate([
        core_1.Component({
            selector: 'quote-general-info-input',
            styles: ['.customers { width: 300px; }'],
            templateUrl: './../../App/Templates/quote-general-info-input.component.html'
        }),
        tslib_1.__metadata("design:paramtypes", [core_data_service_1.CoreDataService, shared_data_service_1.SharedDataService])
    ], QuoteGeneralInfoComponent);
    return QuoteGeneralInfoComponent;
}());
exports.QuoteGeneralInfoComponent = QuoteGeneralInfoComponent;
