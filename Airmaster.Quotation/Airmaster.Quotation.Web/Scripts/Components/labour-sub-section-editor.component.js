"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var core_1 = require("@angular/core");
var forms_1 = require("@angular/forms");
var kendo_angular_grid_1 = require("@progress/kendo-angular-grid");
var core_data_service_1 = require("./../Service/core-data.service");
var labour_1 = require("./../Model/Section/labour");
var section_1 = require("./../Model/section");
var formGroup = function (dataItem) { return new forms_1.FormGroup({
    'QuoteLabourID': new forms_1.FormControl(dataItem.QuoteLabourID, forms_1.Validators.required),
    'Class': new forms_1.FormControl(dataItem.Class, forms_1.Validators.required),
    'CostPerUnit': new forms_1.FormControl(dataItem.CostPerUnit),
    'Quantity': new forms_1.FormControl(dataItem.Quantity, forms_1.Validators.required),
    'CostSubTotal': new forms_1.FormControl(dataItem.CostSubTotal),
    'Margin': new forms_1.FormControl(dataItem.Margin, forms_1.Validators.required),
    'PricePerUnit': new forms_1.FormControl(dataItem.PricePerUnit),
    'PriceSubTotal': new forms_1.FormControl(dataItem.PriceSubTotal),
    'Description': new forms_1.FormControl(dataItem.Description)
}); };
var LabourSubSectionEditor = (function () {
    function LabourSubSectionEditor(coreDataService) {
        this.coreDataService = coreDataService;
        this.labourClassList = [];
        this.formGroup = formGroup(new labour_1.QuotationLabourEntity());
        this.seq = 1.00;
        this.gridState = {
            sort: [],
            skip: 0,
            take: 10
        };
        this.notify = new core_1.EventEmitter();
    }
    LabourSubSectionEditor.prototype.ngOnInit = function () {
        //this.selectedSection = new Section();
        this.getLabourClasses(this.selectedSection.WorkState);
    };
    LabourSubSectionEditor.prototype.getLabourClasses = function (state) {
        var _this = this;
        this.coreDataService.getLabourClasses(state)
            .subscribe(function (labourClasses) { return _this.labourClassList = labourClasses; }, function (error) { return _this.errorMessage = error; });
    };
    //provide template data  for selection from dropdown
    LabourSubSectionEditor.prototype.LabourClassList = function (id) {
        return this.labourClassList.find(function (x) { return x.Code === id; });
    };
    LabourSubSectionEditor.prototype.editHandler = function (_a) {
        var _this = this;
        var sender = _a.sender, rowIndex = _a.rowIndex, dataItem = _a.dataItem;
        this.closeEditor(sender);
        this.formGroup = formGroup(dataItem);
        this.formGroup.valueChanges.subscribe(function (data) {
            if (data.LabourClass != "") {
                _this.formGroup.value.CostPerUnit = _this.labourClassList.find(function (x) { return x.Code === _this.formGroup.value.Class; }).Rate;
            }
            _this.formGroup.value.CostSubTotal = _this.formGroup.value.CostPerUnit * data.Quantity;
            _this.formGroup.value.PricePerUnit = _this.formGroup.value.CostPerUnit;
            if (_this.formGroup.value.Margin > 0) {
                _this.formGroup.value.PricePerUnit = parseInt(_this.formGroup.value.CostPerUnit) + ((_this.formGroup.value.CostPerUnit * _this.formGroup.value.Margin) / 100);
            }
            _this.formGroup.value.PriceSubTotal = _this.formGroup.value.PricePerUnit * _this.formGroup.value.Quantity;
        });
        this.editedRowIndex = rowIndex;
        sender.editRow(rowIndex, this.formGroup);
    };
    LabourSubSectionEditor.prototype.cancelHandler = function (_a) {
        var sender = _a.sender, rowIndex = _a.rowIndex;
        this.closeEditor(sender, rowIndex);
    };
    LabourSubSectionEditor.prototype.closeEditor = function (grid, rowIndex) {
        if (rowIndex === void 0) { rowIndex = this.editedRowIndex; }
        grid.closeRow(rowIndex);
        this.editedRowIndex = undefined;
        this.formGroup = undefined;
    };
    //save labour   
    LabourSubSectionEditor.prototype.saveHandler = function (_a) {
        var _this = this;
        var sender = _a.sender, rowIndex = _a.rowIndex, formGroup = _a.formGroup, isNew = _a.isNew;
        var labour = formGroup.value;
        this.saveLabour(labour);
        //this.editService.save(product, isNew);
        if (isNew) {
            this.selectedSection.Labours.push(this.formGroup.value);
        }
        else {
            Object.assign(this.selectedSection.Labours.find(function (_a) {
                var QuoteLabourID = _a.QuoteLabourID;
                return QuoteLabourID === _this.formGroup.value.QuoteLabourID;
            }), this.formGroup.value);
        }
        //sum of labour costs for selected section
        this.selectedSection.PriceLabour = this.selectedSection.Labours.map(function (i) { return i.PriceSubTotal; }).reduce(function (a, b) { return a + b; }, 0);
        sender.closeRow(rowIndex);
        this.notify.emit(this.selectedSection);
    };
    //remove a labour list value from grid
    LabourSubSectionEditor.prototype.removeHandler = function (_a) {
        var dataItem = _a.dataItem;
        this.selectedSection.Labours.splice(this.selectedSection.Labours.indexOf(dataItem), 1);
    };
    //add new labour class row in grid
    LabourSubSectionEditor.prototype.addHandler = function (_a) {
        var _this = this;
        var sender = _a.sender;
        this.closeEditor(sender);
        if (this.selectedSection.Labours == undefined) {
            this.selectedSection.Labours = [];
        }
        if (this.selectedSection.Labours.length > 0) {
            this.seq = this.selectedSection.Labours[this.selectedSection.Labours.length - 1].QuoteLabourID + 1;
        }
        this.formGroup = formGroup(new labour_1.QuotationLabourEntity(this.seq, this.selectedSection.QuoteSectionID, "", 0, 0, 0, this.selectedSection.MarginActualPercentage, 0, 0, ""));
        this.formGroup.valueChanges.subscribe(function (data) {
            if (data.LabourClass != "") {
                _this.formGroup.value.CostPerUnit = _this.labourClassList.find(function (x) { return x.Code === _this.formGroup.value.Class; }).Rate;
            }
            _this.formGroup.value.CostSubTotal = _this.formGroup.value.CostPerUnit * data.Quantity;
            _this.formGroup.value.PricePerUnit = _this.formGroup.value.CostPerUnit;
            if (_this.formGroup.value.Margin > 0) {
                _this.formGroup.value.PricePerUnit = parseInt(_this.formGroup.value.CostPerUnit) + ((_this.formGroup.value.CostPerUnit * _this.formGroup.value.Margin) / 100);
            }
            _this.formGroup.value.PriceSubTotal = _this.formGroup.value.PricePerUnit * _this.formGroup.value.Quantity;
        });
        sender.addRow(this.formGroup);
    };
    LabourSubSectionEditor.prototype.saveLabour = function (labour) {
        var _this = this;
        this.coreDataService.saveLabour(labour)
            .subscribe(function (seq) {
        }, function (error) { return _this.errorMessage = error; });
    };
    tslib_1.__decorate([
        core_1.Input(),
        tslib_1.__metadata("design:type", section_1.Section)
    ], LabourSubSectionEditor.prototype, "selectedSection", void 0);
    tslib_1.__decorate([
        core_1.Input(),
        tslib_1.__metadata("design:type", String)
    ], LabourSubSectionEditor.prototype, "type", void 0);
    tslib_1.__decorate([
        core_1.Output(),
        tslib_1.__metadata("design:type", core_1.EventEmitter)
    ], LabourSubSectionEditor.prototype, "notify", void 0);
    tslib_1.__decorate([
        core_1.ViewChild(kendo_angular_grid_1.GridComponent),
        tslib_1.__metadata("design:type", kendo_angular_grid_1.GridComponent)
    ], LabourSubSectionEditor.prototype, "grid", void 0);
    LabourSubSectionEditor = tslib_1.__decorate([
        core_1.Component({
            selector: 'labour-sub-section-grid',
            template: "\n            <kendo-grid\n                      [data]=\"selectedSection.Labours \"\n                      [height]=\"300\"\n                      [pageSize]=\"gridState.take\" [skip]=\"gridState.skip\" [sort]=\"gridState.sort\"\n                      [pageable]=\"true\" [sortable]=\"true\"\n                      (dataStateChange)=\"onStateChange($event)\"\n                      (edit)=\"editHandler($event)\" (cancel)=\"cancelHandler($event)\"\n                      (save)=\"saveHandler($event)\" (remove)=\"removeHandler($event)\"\n                      (add)=\"addHandler($event)\"\n            >\n            <ng-template kendoGridToolbarTemplate>\n                <button kendoGridAddCommand>Add new</button>\n            </ng-template>    \n            <kendo-grid-column field=\"QuoteLabourID\" title=\"SEQ\" [editable]=\"false\" width=\"50\">              \n            </kendo-grid-column>\n            <kendo-grid-column field=\"Class\"  title=\"Labour Class\" width=\"200\">\n              <ng-template kendoGridCellTemplate let-dataItem>\n                {{LabourClassList(dataItem.Class)?.Description}}\n              </ng-template>\n              <ng-template kendoGridEditTemplate \n                let-dataItem=\"dataItem\"\n                let-formGroup=\"formGroup\">\n                <kendo-dropdownlist \n                  [data]=\"labourClassList\"\n                  textField=\"Description\"\n                  valueField=\"Code\"\n                  [valuePrimitive]=\"true\"\n                  [formControl]=\"formGroup.get('Class')\">                \n                </kendo-dropdownlist>\n              </ng-template>\n            </kendo-grid-column>\n            <kendo-grid-column field=\"CostPerUnit\" [editable]=\"false\" title=\"Cost Per Unit\">\n                <ng-template kendoGridCellTemplate let-dataItem>\n                {{dataItem.CostPerUnit}}\n              </ng-template>\n            <ng-template kendoGridEditTemplate \n                let-dataItem=\"dataItem\"\n                let-formGroup=\"formGroup\">\n                    <kendo-numerictextbox \n                    [value]=\"0\"\n                    [min]=\"0\"\n                    [max]=\"100\"\n                    [autoCorrect]=\"false\"\n                    name=\"CostPerUnit\"\n                    [formControl]=\"formGroup.get('CostPerUnit')\"\n                    >\n                    </kendo-numerictextbox>\n              </ng-template>\n            </kendo-grid-column>\n            <kendo-grid-column field=\"Quantity\" editor=\"numeric\" title=\"Labour Qty\"></kendo-grid-column>\n            <kendo-grid-column field=\"CostSubTotal\" [editable]=\"false\"  title=\"Cost Sub Total\">   \n            </kendo-grid-column>\n            <kendo-grid-column field=\"Margin\" title=\"Margin(%)\" editor=\"numeric\"></kendo-grid-column>\n            <kendo-grid-column field=\"PricePerUnit\" [editable]=\"false\" title=\"Price Per Unit\"></kendo-grid-column>\n            <kendo-grid-column field=\"PriceSubTotal\"[editable]=\"false\" title=\"Price Sub Total\"></kendo-grid-column>\n            <kendo-grid-column field=\"Description\" title=\"Description\"></kendo-grid-column>\n            <kendo-grid-command-column title=\"command\" width=\"220\">\n              <ng-template kendoGridCellTemplate let-isNew=\"isNew\">\n                <button kendoGridEditCommand class=\"k-primary\">Edit</button>\n                <button kendoGridRemoveCommand>Remove</button>\n                <button kendoGridSaveCommand [disabled]=\"formGroup?.invalid\">{{ isNew ? 'Add' : 'Update' }}</button>\n                <button kendoGridCancelCommand>{{ isNew ? 'Discard changes' : 'Cancel' }}</button>\n              </ng-template>\n            </kendo-grid-command-column>             \n        </kendo-grid> "
        }),
        tslib_1.__metadata("design:paramtypes", [core_data_service_1.CoreDataService])
    ], LabourSubSectionEditor);
    return LabourSubSectionEditor;
}());
exports.LabourSubSectionEditor = LabourSubSectionEditor;
