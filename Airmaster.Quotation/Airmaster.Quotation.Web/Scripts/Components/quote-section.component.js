"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var core_1 = require("@angular/core");
var section_1 = require("./../Model/section");
var quote_1 = require("./../Model/quote");
var kendo_angular_grid_1 = require("@progress/kendo-angular-grid");
var core_data_service_1 = require("./../Service/core-data.service");
var QuoteSectionComponent = (function () {
    function QuoteSectionComponent(coreDataService) {
        this.coreDataService = coreDataService;
        this.sort = [];
        this.pageSize = 10;
        this.skip = 0;
        this.seq = 1;
        this.notify = new core_1.EventEmitter();
    }
    QuoteSectionComponent.prototype.ngOnInit = function () {
        if (this.quote.Sections.length == 0) {
            this.section = new section_1.Section(this.seq, this.quote.QuoteID, "", true, this.quote.LocationID, "", "", this.quote.WorkStateCd, 0, 0, 0, 0, 0, 0, this.quote.Margin, 0);
            this.quote.Sections.push(this.section);
        }
        this.selectedSectionRow = this.quote.Sections[0];
    };
    QuoteSectionComponent.prototype.dataStateChange = function (_a) {
        var skip = _a.skip, take = _a.take, sort = _a.sort;
        // Save the current state of the Grid component
        this.skip = skip;
        this.pageSize = take;
        this.sort = sort;
        // Reload the data with the new state
        //this.loadData();
    };
    QuoteSectionComponent.prototype.onDetailExpand = function (e) {
        this.selectedSectionRow = this.quote.Sections[e.index];
    };
    QuoteSectionComponent.prototype.ngAfterViewInit = function () {
        // Expand the first row initially
        this.grid.expandRow(0);
    };
    QuoteSectionComponent.prototype.AddSection = function () {
        if (this.quote.Sections != undefined) {
            if (this.quote.Sections.length > 0) {
                this.seq = this.quote.Sections[this.quote.Sections.length - 1].QuoteSectionID + 1;
            }
        }
        else {
            this.quote.Sections = [];
        }
        this.section = new section_1.Section(this.seq, this.quote.QuoteID, "", true, this.quote.LocationID, "", "", this.quote.WorkStateCd, 0, 0, 0, 0, 0, 0, this.quote.Margin, 0);
        this.quote.Sections.push(this.section);
        this.grid.expandRow(this.grid.view.length - 1);
    };
    QuoteSectionComponent.prototype.RemoveSection = function () {
        this.quote.Sections.pop();
    };
    //on selection change update subsection tab
    QuoteSectionComponent.prototype.onSelect = function (e) {
        console.log(this.quote.Sections[e.index]);
        this.selectedSectionRow = this.quote.Sections[e.index];
    };
    QuoteSectionComponent.prototype.onNotify = function (newSection) {
        var _this = this;
        this.section = newSection;
        this.saveSectionGeneralInfo(this.section);
        //assign new section to quote
        this.grid.collapseRow(this.section.QuoteSectionID - 1);
        Object.assign(this.quote.Sections.find(function (_a) {
            var QuoteSectionID = _a.QuoteSectionID;
            return QuoteSectionID === _this.section.QuoteSectionID;
        }), this.section);
        this.notify.emit(this.quote);
    };
    QuoteSectionComponent.prototype.saveSectionGeneralInfo = function (section) {
        var _this = this;
        this.coreDataService.saveQuoteSectionInfo(section)
            .subscribe(function (quoteID) { _this.quote.QuoteID = quoteID; }, function (error) { return _this.errorMessage = error; });
    };
    tslib_1.__decorate([
        core_1.Input(),
        tslib_1.__metadata("design:type", quote_1.Quote)
    ], QuoteSectionComponent.prototype, "quote", void 0);
    tslib_1.__decorate([
        core_1.Output(),
        tslib_1.__metadata("design:type", core_1.EventEmitter)
    ], QuoteSectionComponent.prototype, "notify", void 0);
    tslib_1.__decorate([
        core_1.ViewChild(kendo_angular_grid_1.GridComponent),
        tslib_1.__metadata("design:type", kendo_angular_grid_1.GridComponent)
    ], QuoteSectionComponent.prototype, "grid", void 0);
    QuoteSectionComponent = tslib_1.__decorate([
        core_1.Component({
            selector: 'quote-section',
            templateUrl: "./../../App/Templates/quote-section.component.html"
        }),
        tslib_1.__metadata("design:paramtypes", [core_data_service_1.CoreDataService])
    ], QuoteSectionComponent);
    return QuoteSectionComponent;
}());
exports.QuoteSectionComponent = QuoteSectionComponent;
