"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var core_1 = require("@angular/core");
var quotation_menus_1 = require("./../Model/quotation-menus");
var MenuBarComponent = (function () {
    function MenuBarComponent() {
        this.menuViewModel = new quotation_menus_1.QuotationMenus(menus);
    }
    MenuBarComponent = tslib_1.__decorate([
        core_1.Component({
            selector: 'menu-bar',
            templateUrl: "./../../App/Templates/menu-bar.component.html",
        })
    ], MenuBarComponent);
    return MenuBarComponent;
}());
exports.MenuBarComponent = MenuBarComponent;
var menus = [
    { MenuText: "List", MenuUrl: 'ListOfQuotes' },
    { MenuText: "New", MenuUrl: 'CreateQuote' },
    { MenuText: "Delete", MenuUrl: 'Quotation/CreateQuote' },
    { MenuText: "Activate Quote", MenuUrl: 'Quotation/CreateQuote' },
    { MenuText: "Copy Or Revise Quote", MenuUrl: 'Quotation/CreateQuote' },
    { MenuText: "MultiQuote", MenuUrl: 'Quotation/CreateQuote' },
    { MenuText: "Set Margin Actual(%)", MenuUrl: 'Quotation/CreateQuote' },
    { MenuText: "Set Price Total(%)", MenuUrl: 'Quotation/CreateQuote' },
    { MenuText: "Quote Document", MenuUrl: 'Quotation/CreateQuote' },
    { MenuText: "Refresh Totals", MenuUrl: 'Quotation/CreateQuote' },
];
