"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var core_1 = require("@angular/core");
var quote_1 = require("./../Model/quote");
var QuoteSummary = (function () {
    function QuoteSummary() {
    }
    QuoteSummary.prototype.ngOnInit = function () {
    };
    tslib_1.__decorate([
        core_1.Input(),
        tslib_1.__metadata("design:type", quote_1.Quote)
    ], QuoteSummary.prototype, "quote", void 0);
    QuoteSummary = tslib_1.__decorate([
        core_1.Component({
            selector: 'quote-summary',
            template: "\n <table class=\"table table-striped priceSummary\" *ngIf=\"quote.Pricing\">\n           \n            <tbody>\n                <tr>\n                    <td>Price Labour</td>\n                    <td>{{quote.Pricing.PriceLabours |  currency:'USD':true}}</td>\n                </tr>\n                <tr>\n                    <td>Price Materials</td>\n                    <td>{{quote.Pricing.PriceMaterials |  currency:'USD':true}}</td>\n                </tr>\n                <tr>\n                    <td>Price Additional</td>\n                    <td> {{quote.Pricing.PriceAdditional |  currency:'USD':true}}</td>\n                </tr>\n                <tr>\n                    <td>Price Total(Ex GST)</td>\n                    <td> {{quote.Pricing.PriceTotal_Ex_GST |  currency:'USD':true}}</td>\n                </tr>\n                <tr>\n                    <td>Weighted Total</td>\n                    <td> {{quote.Pricing.WeightedTotal |  currency:'USD':true}}</td>\n                </tr>\n                <tr>\n                    <td>GST Total</td>\n                    <td> {{quote.Pricing.GST_Total |  currency:'USD':true}}</td>\n                </tr>\n                <tr>\n                    <td>Price Total(Inc GST)</td>\n                    <td> {{quote.Pricing.PriceTotal_Inc_GST |  currency:'USD':true}}</td>\n                </tr>\n                <tr>\n                    <td>Margin Actual(%)</td>\n                    <td> {{quote.Pricing.MarginActual_Percentage}}</td>\n                </tr>\n                <tr>\n                    <td>Margin Actual($)</td>\n                    <td> {{quote.Pricing.MarginActual |  currency:'USD':true}}</td>\n                </tr>\n                <tr>\n                    <td>Markup Actual(%)</td>\n                    <td> {{quote.Pricing.MarkUpActual}}</td>\n                </tr>\n            </tbody>\n        </table>\n"
        })
    ], QuoteSummary);
    return QuoteSummary;
}());
exports.QuoteSummary = QuoteSummary;
