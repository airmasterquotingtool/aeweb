"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var kendo_data_query_1 = require("@progress/kendo-data-query");
var shared_data_service_1 = require("./../Service/shared-data.service");
var core_data_service_1 = require("./../Service/core-data.service");
var quote_1 = require("./../Model/quote");
var QuoteList = (function () {
    function QuoteList(coreDataService, _router, sharedDataService) {
        this.coreDataService = coreDataService;
        this._router = _router;
        this.sharedDataService = sharedDataService;
        this.state = {
            skip: 0,
            take: 10
        };
        this.selectedQuote = new quote_1.Quote();
        this.quoteList = [];
        this.gridData = kendo_data_query_1.process(this.quoteList, this.state);
    }
    QuoteList.prototype.ngOnInit = function () {
        this.sharedDataService.sharingData = null;
        this.getQuoteList();
    };
    QuoteList.prototype.getQuoteList = function () {
        var _this = this;
        this.coreDataService.getQuotationList(0)
            .subscribe(function (quoteList) {
            _this.quoteList = quoteList;
            _this.gridData = kendo_data_query_1.process(_this.quoteList, _this.state);
        }, function (error) { return _this.errorMessage = error; });
    };
    QuoteList.prototype.EditQuote = function (val) {
        //alert("Edit");
        //let price= new QuotePrice(0,0,0,0,0,0,0,0,0,0);
        //val.Pricing = price;
        //let sections = [new Section(1)];
        //val.Sections = sections;
        //this.sharedDataService.saveData(val);
        this.getQuoteDetail(val.QuoteID);
    };
    QuoteList.prototype.getQuoteDetail = function (quoteID) {
        var _this = this;
        this.coreDataService.getQuoteDetail(quoteID)
            .subscribe(function (quote) {
            quote.Sections = [];
            _this.sharedDataService.saveData(quote);
            _this._router.navigate(["CreateQuote"]);
        }, function (error) { return _this.errorMessage = error; });
    };
    QuoteList.prototype.dataStateChange = function (state) {
        this.state = state;
        this.gridData = kendo_data_query_1.process(this.quoteList, this.state);
    };
    QuoteList = tslib_1.__decorate([
        core_1.Component({
            selector: 'quote-list',
            template: "\n<div class=\"row no-gutter quoteList\">\n<kendo-grid\n        [data]=\"gridData\"\n        [pageSize]=\"state.take\"\n        [skip]=\"state.skip\"\n        [sort]=\"state.sort\"\n        [filter]=\"state.filter\"\n        [sortable]=\"true\"\n        [pageable]=\"true\"\n        [filterable]=\"true\"\n        [selectable]=\"true\"       \n        (dataStateChange)=\"dataStateChange($event)\">\n\n    <kendo-grid-column field=\"Subject\"     title=\"Subject\" width=\"240\" >\n        <ng-template kendoGridCellTemplate let-dataItem let-rowIndex=\"rowIndex\">\n             <span (click)=\"EditQuote(dataItem)\" \n            style=\"color:blue;text-decoration:underline;cursor:pointer;\"\n            title={{dataItem.Subject}}>{{dataItem.Subject}}</span>\n        </ng-template>\n\n      \n    </kendo-grid-column>\n    <kendo-grid-column field=\"Customer\" title=\"Customer\"  width=\"180\">\n    </kendo-grid-column>\n    <kendo-grid-column field=\"Location\" title=\"Location\" width=\"100\" >\n    </kendo-grid-column>\n    <kendo-grid-column field=\"QuoteID\" title=\"QuoteID\" width=\"70\" >\n    </kendo-grid-column>\n    <kendo-grid-column field=\"Revision\" title=\"Revision\" width=\"70\">\n    </kendo-grid-column>\n    <kendo-grid-column field=\"EstimatedCloseDate\" title=\"Est.Close Date\" width=\"100\" filter=\"date\" format=\"{0:d}\" >\n    </kendo-grid-column>\n    <kendo-grid-column field=\"EstimatedCloseDate_Quote\" title=\"Est.CloseDate(Quote)\" width=\"100\" filter=\"date\" format=\"{0:d}\">\n    </kendo-grid-column>\n    <kendo-grid-column field=\"Probability\" title=\"Probability\" width=\"70\" filter=\"numeric\" format=\"{0:c}\">\n    </kendo-grid-column>\n    <kendo-grid-column field=\"QuoteWeightedValue\" title=\"Quote Weighted Value\" width=\"100\" filter=\"numeric\" format=\"{0:c}\">\n    </kendo-grid-column>\n    <kendo-grid-column field=\"SaleType\" title=\"SaleType\" width=\"70\" >\n    </kendo-grid-column>\n    <kendo-grid-column field=\"Variation\" title=\"Variation\" width=\"50\" >\n    </kendo-grid-column>\n    <kendo-grid-column field=\"WorkState\" title=\"WorkState\" width=\"80\">\n    </kendo-grid-column>\n    <kendo-grid-column field=\"QuotePriceTotal\" title=\"Quote Price Total\" width=\"100\" filter=\"numeric\" format=\"{0:c}\">\n    </kendo-grid-column>\n    <kendo-grid-column field=\"StatusReason\" title=\"Status Reason\" width=\"70\" >\n    </kendo-grid-column>\n    <kendo-grid-column field=\"Owner\" title=\"Owner\" width=\"80\" >\n    </kendo-grid-column>\n    <kendo-grid-column field=\"CreatedOn\" title=\"Created On\" width=\"100\" filter=\"date\" format=\"{0:d}\">\n    </kendo-grid-column>\n    <kendo-grid-column field=\"ModifiedOn\" title=\"Modified On\" width=\"100\" filter=\"date\" format=\"{0:d}\">\n    </kendo-grid-column>    \n    </kendo-grid>\n</div>\n"
        }),
        tslib_1.__metadata("design:paramtypes", [core_data_service_1.CoreDataService, router_1.Router, shared_data_service_1.SharedDataService])
    ], QuoteList);
    return QuoteList;
}());
exports.QuoteList = QuoteList;
