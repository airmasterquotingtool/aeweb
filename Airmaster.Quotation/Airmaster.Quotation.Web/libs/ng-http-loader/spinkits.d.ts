export declare const Spinkit: {
    skChasingDots: string;
    skCubeGrid: string;
    skDoubleBounce: string;
    skRotatingPlane: string;
    skSpinnerPulse: string;
    skThreeBounce: string;
    skWanderingCubes: string;
    skWave: string;
};
