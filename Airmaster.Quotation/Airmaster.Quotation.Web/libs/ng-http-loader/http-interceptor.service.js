/*
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
var Rx_1 = require("rxjs/Rx");
var HttpInterceptorService = (function (_super) {
    __extends(HttpInterceptorService, _super);
    function HttpInterceptorService(backend, defaultOptions) {
        var _this = _super.call(this, backend, defaultOptions) || this;
        _this._pendingRequests = 0;
        _this._pendingRequestsStatus = new Rx_1.Subject();
        return _this;
    }
    Object.defineProperty(HttpInterceptorService.prototype, "pendingRequestsStatus", {
        get: function () {
            return this._pendingRequestsStatus.asObservable();
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(HttpInterceptorService.prototype, "pendingRequests", {
        get: function () {
            return this._pendingRequests;
        },
        enumerable: true,
        configurable: true
    });
    HttpInterceptorService.prototype.request = function (url, options) {
        var _this = this;
        this._pendingRequests++;
        if (1 === this._pendingRequests) {
            this._pendingRequestsStatus.next(true);
        }
        return _super.prototype.request.call(this, url, options)
            .map(function (result) {
            return result;
        })
            .catch(function (error) {
            return Rx_1.Observable.throw(error);
        })
            .finally(function () {
            _this._pendingRequests--;
            if (0 === _this._pendingRequests) {
                _this._pendingRequestsStatus.next(false);
            }
        });
    };
    return HttpInterceptorService;
}(http_1.Http));
HttpInterceptorService.decorators = [
    { type: core_1.Injectable },
];
/** @nocollapse */
HttpInterceptorService.ctorParameters = function () { return [
    { type: http_1.ConnectionBackend, },
    { type: http_1.RequestOptions, },
]; };
exports.HttpInterceptorService = HttpInterceptorService;
function HttpInterceptorServiceFactory(backend, defaultOptions) {
    return new HttpInterceptorService(backend, defaultOptions);
}
exports.HttpInterceptorServiceFactory = HttpInterceptorServiceFactory;
exports.HttpInterceptorServiceFactoryProvider = {
    provide: HttpInterceptorService,
    useFactory: HttpInterceptorServiceFactory,
    deps: [http_1.XHRBackend, http_1.RequestOptions]
};
//# sourceMappingURL=http-interceptor.service.js.map