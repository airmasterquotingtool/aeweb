/*
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var spinner_component_1 = require("./spinner/spinner.component");
var http_interceptor_service_1 = require("./http-interceptor.service");
var common_1 = require("@angular/common");
var http_1 = require("@angular/http");
var NgHttpLoaderModule = (function () {
    function NgHttpLoaderModule() {
    }
    return NgHttpLoaderModule;
}());
NgHttpLoaderModule.decorators = [
    { type: core_1.NgModule, args: [{
                declarations: [
                    spinner_component_1.SpinnerComponent,
                ],
                imports: [
                    common_1.CommonModule,
                    http_1.HttpModule
                ],
                exports: [
                    spinner_component_1.SpinnerComponent,
                ],
                providers: [
                    http_interceptor_service_1.HttpInterceptorServiceFactoryProvider,
                ]
            },] },
];
/** @nocollapse */
NgHttpLoaderModule.ctorParameters = function () { return []; };
exports.NgHttpLoaderModule = NgHttpLoaderModule;
//# sourceMappingURL=ng-http-loader.module.js.map