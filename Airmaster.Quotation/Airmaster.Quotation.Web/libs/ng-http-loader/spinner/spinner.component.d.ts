import { OnDestroy } from '@angular/core';
import { HttpInterceptorService } from '../http-interceptor.service';
export declare class SpinnerComponent implements OnDestroy {
    private http;
    isSpinnerVisible: boolean;
    private subscription;
    Spinkit: {
        skChasingDots: string;
        skCubeGrid: string;
        skDoubleBounce: string;
        skRotatingPlane: string;
        skSpinnerPulse: string;
        skThreeBounce: string;
        skWanderingCubes: string;
        skWave: string;
    };
    backgroundColor: string;
    spinner: string;
    constructor(http: HttpInterceptorService);
    ngOnDestroy(): void;
}
