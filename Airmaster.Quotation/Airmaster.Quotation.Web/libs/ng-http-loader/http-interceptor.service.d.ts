import { ConnectionBackend, Http, Request, RequestOptions, RequestOptionsArgs, Response, XHRBackend } from '@angular/http';
import { Observable } from 'rxjs/Rx';
export declare class HttpInterceptorService extends Http {
    private _pendingRequests;
    private _pendingRequestsStatus;
    constructor(backend: ConnectionBackend, defaultOptions: RequestOptions);
    readonly pendingRequestsStatus: Observable<boolean>;
    readonly pendingRequests: number;
    request(url: string | Request, options?: RequestOptionsArgs): Observable<Response>;
}
export declare function HttpInterceptorServiceFactory(backend: XHRBackend, defaultOptions: RequestOptions): HttpInterceptorService;
export declare let HttpInterceptorServiceFactoryProvider: {
    provide: typeof HttpInterceptorService;
    useFactory: (backend: XHRBackend, defaultOptions: RequestOptions) => HttpInterceptorService;
    deps: (typeof XHRBackend | typeof RequestOptions)[];
};
