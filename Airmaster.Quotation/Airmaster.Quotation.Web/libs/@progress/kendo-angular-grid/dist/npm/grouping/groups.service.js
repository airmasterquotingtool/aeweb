"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var core_1 = require("@angular/core");
var expand_state_service_1 = require("../expand-state.service");
var removeLast = function (groupIndex) { return groupIndex.lastIndexOf("_") > -1
    ? groupIndex.slice(0, groupIndex.lastIndexOf("_"))
    : ""; };
/**
 * @hidden
 */
var GroupsService = (function (_super) {
    tslib_1.__extends(GroupsService, _super);
    function GroupsService() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    GroupsService.prototype.isInExpandedGroup = function (groupIndex, skipSelf) {
        if (skipSelf === void 0) { skipSelf = true; }
        if (skipSelf) {
            groupIndex = removeLast(groupIndex);
        }
        var expanded = true;
        while (groupIndex && expanded) {
            expanded = this.isExpanded(groupIndex);
            groupIndex = removeLast(groupIndex);
        }
        return expanded;
    };
    GroupsService.prototype.isExpanded = function (index) {
        return !_super.prototype.isExpanded.call(this, index);
    };
    return GroupsService;
}(expand_state_service_1.ExpandStateService));
GroupsService.decorators = [
    { type: core_1.Injectable },
];
/** @nocollapse */
GroupsService.ctorParameters = function () { return []; };
exports.GroupsService = GroupsService;
