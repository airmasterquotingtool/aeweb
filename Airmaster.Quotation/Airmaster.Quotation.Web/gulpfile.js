/// <binding />
var ts = require('gulp-typescript');
var gulp = require('gulp');
var clean = require('gulp-clean');
var destPath = './libs/';

// Delete the dist directory
gulp.task('clean', function() {
    return gulp.src(destPath)
        .pipe(clean());
});
gulp.task("scriptsNStyles", function() {
    gulp.src([
        'core-js/client/*.js',
        'systemjs/dist/*.js',
        'reflect-metadata/*.js',
        'rxjs/**',
        'ng-http-loader/**',
        'zone.js/dist/*.js',
        '@angular/**/bundles/*.js',
        'ng2-ckeditor/lib/*.js',
        '@progress/**/dist/npm/**/*.js',
        '@telerik/**/dist/npm/**/*.js',
        '@ng-bootstrap/ng-bootstrap/bundles/ng-bootstrap.js',
        'bootstrap/dist/js/*.js',
        'jszip/dist/jszip.js'
       
    ], {
            cwd: "node_modules/**"
        }).pipe(gulp.dest("./libs"));
});

var tsProject = ts.createProject('tsconfig.json', {
    typescript: require('typescript')
});

gulp.task('ts', function(done) {
    //var tsResult = tsProject.src()
    var tsResult = gulp.src([
        "App/**/*.ts"
    ])
    .pipe(tsProject(), undefined, ts.reporter.fullReporter());
    return tsResult.js.pipe(gulp.dest('./Scripts'));
});

//var appBundles = [
//    { scripts: ['libs/**/*.js'], output: 'bundle.js' },

//];
//gulp.task('bundle',

//    function bundle() {
//        appBundles.forEach(function(appBundle) {
//            console.log('Creating bundle and sourcemaps: ' + appBundle.output);
//            gulp.src(appBundle.scripts)
//                .pipe(concat(appBundle.output))
//                .pipe(sourcemaps.init())
//                .pipe(sourcemaps.write(outFolder + '\\maps'))
//                .pipe(gulp.dest(outFolder));

//        });
//    });

//gulp.task('minify', function minify() {
//    appBundles.forEach(function (appBundle) {
//        console.log('Creating minified bundle for: ' + appBundle.output);
//        gulp.src(appBundle.scripts)
//            .pipe(concat(appBundle.output))
//            .pipe(rename({ extname: '.min.js' }))
//            .pipe(uglify())
//            .pipe(gulp.dest(outFolder));

//    });
//});


gulp.task('watch', ['watch.ts']);

gulp.task('watch.ts', ['ts'], function() {
    return gulp.watch('App/**/*.ts', ['ts']);
});

gulp.task('default', ['scriptsNStyles',  'watch']);