﻿(function (global) {
    System.config({
        paths: {
            // paths serve as alias
            'npm:': '/libs/'
        },
        // map tells the System loader where to look for things
        map: {
            // our app is within the app folder
            app: '/Scripts',
            // angular bundles
            '@angular/animations': 'npm:@angular/animations/bundles/animations.umd.js',
            '@angular/animations/browser': 'npm:@angular/animations/bundles/animations-browser.umd.js',
            '@angular/core': 'npm:@angular/core/bundles/core.umd.js',
            '@angular/common': 'npm:@angular/common/bundles/common.umd.js',
            '@angular/compiler': 'npm:@angular/compiler/bundles/compiler.umd.js',
            '@angular/platform-browser': 'npm:@angular/platform-browser/bundles/platform-browser.umd.js',
            '@angular/platform-browser/animations': 'npm:@angular/platform-browser/bundles/platform-browser-animations.umd.js',
            '@angular/platform-browser-dynamic': 'npm:@angular/platform-browser-dynamic/bundles/platform-browser-dynamic.umd.js',
            '@angular/http': 'npm:@angular/http/bundles/http.umd.js',
            '@angular/router': 'npm:@angular/router/bundles/router.umd.js',
            '@angular/forms': 'npm:@angular/forms/bundles/forms.umd.js',
            '@ng-bootstrap': 'npm:@ng-bootstrap',
            '@ng-bootstrap/ng-bootstrap': 'npm:@ng-bootstrap/ng-bootstrap/bundles/ng-bootstrap.js',
            '@progress': 'npm:@progress',
            '@telerik': 'npm:@telerik',
            'rxjs': 'npm:rxjs',
            'jszip': 'npm:jszip',

            "ng2-ckeditor": "npm:ng2-ckeditor",

            'ng-http-loader/ng-http-loader.module': "npm:ng-http-loader",
            'ng-http-loader': "npm:ng-http-loader",
                'tslib':"npm:tslib"
            //'traceur': 'npm:traceur',
            // 'require': 'npm:require'

        },
        meta: {
            '*.json': {
                loader: 'systemjs-plugin-json'
            }
        },
        packages: {

            app: {
                main: './App/main.js',
                defaultExtension: 'js'
            },
            rxjs: {
                defaultExtension: 'js'
            },
            'ng-http-loader/ng-http-loader.module': {
                defaultExtension: 'js',
                main: './ng-http-loader.module.js'
            }
            ,
            "tslib": {
                main: "tslib.js",
                defaultExtension: "js"
            },
            'systemjs-plugin-json': {
                defaultExtension: 'js',
                main: 'json.js'
            },
            "ng2-ckeditor": {
                main: "lib/index.js",
                defaultExtension: "js"
            },

            // Kendo UI for Angular packages
            'npm:@progress/kendo-angular-buttons': {
                main: './dist/npm/main.js',
                defaultExtension: 'js'
            },

            'npm:@progress/kendo-angular-charts': {
                main: './dist/npm/main.js',
                defaultExtension: 'js'
            },

            'npm:@progress/kendo-angular-dateinputs': {
                main: './dist/npm/main.js',
                defaultExtension: 'js'
            },

            'npm:@progress/kendo-angular-dropdowns': {
                main: './dist/npm/main.js',
                defaultExtension: 'js'
            },

            'npm:@progress/kendo-angular-dialog': {
                main: './dist/npm/main.js',
                defaultExtension: 'js'
            },

            'npm:@progress/kendo-angular-grid': {
                main: './dist/npm/main.js',
                defaultExtension: 'js'
            },

            'npm:@progress/kendo-angular-inputs': {
                main: './dist/npm/main.js',
                defaultExtension: 'js'
            },

            'npm:@progress/kendo-angular-intl': {
                main: './dist/npm/main.js',
                defaultExtension: 'js'
            },

            'npm:@progress/kendo-angular-l10n': {
                main: './dist/npm/main.js',
                defaultExtension: 'js'
            },

            'npm:@progress/kendo-angular-excel-export': {
                main: './dist/npm/main.js',
                defaultExtension: 'js'
            },

            'npm:@progress/kendo-angular-layout': {
                main: './dist/npm/main.js',
                defaultExtension: 'js'
            },

            'npm:@progress/kendo-angular-scrollview': {
                main: './dist/npm/main.js',
                defaultExtension: 'js'
            },

            'npm:@progress/kendo-angular-sortable': {
                main: './dist/npm/main.js',
                defaultExtension: 'js'
            },

            'npm:@progress/kendo-angular-popup': {
                main: './dist/npm/main.js',
                defaultExtension: 'js'
            },

            'npm:@progress/kendo-angular-resize-sensor': {
                main: './dist/npm/main.js',
                defaultExtension: 'js'
            },

            'npm:@progress/kendo-angular-upload': {
                main: './dist/npm/main.js',
                defaultExtension: 'js'
            },

            'npm:@progress/kendo-charts': {
                main: './dist/npm/main.js',
                defaultExtension: 'js'
            },

            'npm:@progress/kendo-data-query': {
                main: './dist/npm/main.js',
                defaultExtension: 'js'
            },

            'npm:@progress/kendo-date-math': {
                main: './dist/npm/main.js',
                defaultExtension: 'js'
            },

            'npm:@progress/kendo-drawing': {
                main: './dist/npm/main.js',
                defaultExtension: 'js'
            },

            'npm:@progress/kendo-file-saver': {
                main: './dist/npm/main.js',
                defaultExtension: 'js'
            },

            'npm:@progress/kendo-intl': {
                main: './dist/npm/main.js',
                defaultExtension: 'js'
            },

            'npm:@progress/kendo-ooxml': {
                main: './dist/npm/main.js',
                defaultExtension: 'js'
            },

            'npm:@progress/kendo-popup-common': {
                main: './dist/npm/main.js',
                defaultExtension: 'js'
            },

            'npm:@telerik/kendo-draggable': {
                main: './dist/npm/main.js',
                defaultExtension: 'js'
            },

            'npm:@telerik/kendo-dropdowns-common': {
                main: './dist/npm/main.js',
                defaultExtension: 'js'
            },

            'npm:@telerik/kendo-intl': {
                main: './dist/npm/main.js',
                defaultExtension: 'js'
            },

            'npm:@telerik/kendo-inputs-common': {
                main: './dist/npm/main.js',
                defaultExtension: 'js'
            },

            'npm:jszip': {
                main: './dist/jszip.js',
                defaultExtension: 'js'
            }

        }
    });
})(this);