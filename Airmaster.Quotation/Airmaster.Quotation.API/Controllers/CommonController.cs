﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Threading.Tasks;
using Airmaster.Quotation.BusinessEntities.Common;
using Airmaster.Quotation.BusinessServices;

namespace Airmaster.Quotation.API.Controllers
{
    public class CommonController : ApiController 
    {
        private readonly CommonData _commondata ;

        public CommonController()
        {
            _commondata = new CommonData();
        }

        [Route("api/Common/GetCodeData")]
        [HttpGet]
        public IHttpActionResult GetCodeData()
        {
            var response = _commondata.GetCodes();
            if (response != null)
            {
                return Json(response);
            }
            else
            {
                return Json("Data not found.");
            }
        }

        [Route("api/Common/GetDivisions")]
        [HttpGet]
        public IHttpActionResult GetDivisions(string state=null)
        {
            var response = _commondata.GetDivisions(state);
            if (response != null)
            {
                return Json(response);
            }
            else
            {
                return Json("Data not found.");
            }
        }

        [Route("api/Common/GetLabourClass")]
        [HttpGet]
        public IHttpActionResult GetLabourClass(string state = null)
        {

            var response = _commondata.GetLabourClass(state);
            if (response != null)
            {
                return Json(response);
            }
            else
            {
                return Json("Data not found.");
            }
        }

        [Route("api/Common/GetVendors")]
        [HttpGet]
        public IHttpActionResult GetVendors(string state = null)
        {

            var response = _commondata.GetVendors(state);
            if (response != null)
            {
                return Json(response);
            }
            else
            {
                return Json("Data not found.");
            }
        }

        [Route("api/Common/GetTechnicians")]
        [HttpGet]
        public IHttpActionResult GetTechnicians()
        {

            var response = _commondata.GetTechnicians();
            if (response != null)
            {
                return Json(response);
            }
            else
            {
                return Json("Data not found.");
            }
        }

        [Route("api/Common/GetContractors")]
        [HttpGet]
        public IHttpActionResult GetContractors(string state = null)
        {

            var response = _commondata.GetContractors(state);
            if (response != null)
            {
                return Json(response);
            }
            else
            {
                return Json("Data not found.");
            }
        }

        [Route("api/Common/GetEquipments")]
        [HttpGet]
        public IHttpActionResult GetEquipments(int? LocationID=null)
        {

            var response = _commondata.GetEquipments(LocationID);
            if (response != null)
            {
                return Json(response);
            }
            else
            {
                return Json("Data not found.");
            }
        }

        [Route("api/Common/GetCustomers")]
        [HttpGet]
        public IHttpActionResult GetCustomers()
        {

            var response = _commondata.GetCustomers();
            if (response != null)
            {
                return Json(response);
            }
            else
            {
                return Json("Data not found.");
            }
        }

        [Route("api/Common/GetLocations")]
        [HttpGet]
        public IHttpActionResult GetLocations(int CustomerID)
        {

            var response = _commondata.GetLocations(CustomerID);
            if (response != null)
            {
                return Json(response);
            }
            else
            {
                return Json("Data not found.");
            }
        }


        [Route("api/Common/GetContacts")]
        [HttpGet]
        public IHttpActionResult GetContacts(int LocationID)
        {

            var response = _commondata.GetContacts(LocationID);
            if (response != null)
            {
                return Json(response);
            }
            else
            {
                return Json("Data not found.");
            }
        }

        [Route("api/Common/GetProducts")]
        [HttpGet]
        public IHttpActionResult GetProducts()
        {

            var response = _commondata.GetProducts();
            if (response != null)
            {
                return Json(response);
            }
            else
            {
                return Json("Data not found.");
            }
        }
    }
}
