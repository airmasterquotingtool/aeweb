﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Threading.Tasks;
using Airmaster.Quotation.BusinessEntities;
using Airmaster.Quotation.BusinessServices;

namespace Airmaster.Quotation.API.Controllers
{
    public class QuotationController : ApiController
    {
        private readonly QuotationManagement _QuotationContext;

        public QuotationController()
        {
            _QuotationContext = new QuotationManagement();
        }

        [Route("api/Quote/GetQuoteDetail")]
        [HttpGet]
        public async Task<IHttpActionResult> GetQuoteDetail(int QuoteID)
        {
            var response = await _QuotationContext.GetQuoteDetail(QuoteID);
            if (response != null)
            {
                return Json(response);
            }
            else
            {
                return Json("Data not found.");
            }
        }

        [Route("api/Quote/GetQuotes")]
        [HttpGet]
        public async Task<IHttpActionResult> GetQuotes(int PageSize,int PageNumber,string SortBy, string OrderBy)
        {
            var response = await _QuotationContext.GetQuotes(PageNumber,PageSize,SortBy,OrderBy);
            if (response != null)
            {
                return Json(response);
            }
            else
            {
                return Json("Data not found.");
            }
        }

        [Route("api/Quote/GetSections")]
        [HttpGet]
        public async Task<IHttpActionResult> GetSections(int QuoteID)
        {
            var response = await _QuotationContext.GetSections(QuoteID);
            if (response != null)
            {
                return Json(response);
            }
            else
            {
                return Json("Data not found.");
            }
        }

        [Route("api/Quote/GetLabours")]
        [HttpGet]
        public async Task<IHttpActionResult> GetLabours(int SectionID)
        {
            var response = await _QuotationContext.GetLabours(SectionID);
            if (response != null)
            {
                return Json(response);
            }
            else
            {
                return Json("Data not found.");
            }
        }


        [Route("api/Quote/GetEquipments")]
        [HttpGet]
        public async Task<IHttpActionResult> GetEquipments(int SectionID)
        {
            var response = await _QuotationContext.GetEquipments(SectionID);
            if (response != null)
            {
                return Json(response);
            }
            else
            {
                return Json("Data not found.");
            }
        }

        [Route("api/Quote/GetMaterials")]
        [HttpGet]
        public async Task<IHttpActionResult> GetMaterials(int SectionID)
        {
            var response = await _QuotationContext.GetMaterials(SectionID);
            if (response != null)
            {
                return Json(response);
            }
            else
            {
                return Json("Data not found.");
            }
        }

        [Route("api/Quote/GetContractors")]
        [HttpGet]
        public async Task<IHttpActionResult> GetContractors(int SectionID)
        {
            var response = await _QuotationContext.GetContractors(SectionID);
            if (response != null)
            {
                return Json(response);
            }
            else
            {
                return Json("Data not found.");
            }
        }


        [Route("api/Quote/GetOthers")]
        [HttpGet]
        public async Task<IHttpActionResult> GetOthers(int SectionID)
        {
            var response = await _QuotationContext.GetOthers(SectionID);
            if (response != null)
            {
                return Json(response);
            }
            else
            {
                return Json("Data not found.");
            }
        }

        [Route("api/Quote/SaveQuote")]
        [System.Web.Http.HttpPost]
        // POST: api/Quotation
        public IHttpActionResult SaveQuote(QuotationEntity Quotation)
        {
            var response = _QuotationContext.SaveQuote(Quotation);
            if (response != null)
            {
                return Json(response);
            }
            else
            {
                return Json("Data not found.");
            }
        }

        [Route("api/Quote/SaveSection")]
        [HttpPost]
        public IHttpActionResult SaveSection(QuotationSectonEntity Section)
        {
            var response = _QuotationContext.SaveSection(Section);
            if (response != null)
            {
                return Json(response);
            }
            else
            {
                return Json("Data not found.");
            }
        }

        [Route("api/Quote/SaveLabour")]
        [HttpPost]
        public IHttpActionResult SaveLabour(QuotationLabourEntity Labour)
        {
            var response = _QuotationContext.SaveLabour(Labour);
            if (response != null)
            {
                return Json(response);
            }
            else
            {
                return Json("Data not found.");
            }
        }


        [Route("api/Quote/SaveMaterial")]
        [HttpPost]
        public IHttpActionResult SaveMaterial(QuotationMaterialEntity Material)
        {
            var response = _QuotationContext.SaveMaterial(Material);
            if (response != null)
            {
                return Json(response);
            }
            else
            {
                return Json("Data not found.");
            }
        }


        [Route("api/Quote/SaveEquipment")]
        [HttpPost]
        public IHttpActionResult SaveEquipment(QuotationEquipmentEntity Equipment)
        {
            var response = _QuotationContext.SaveEquipment(Equipment);
            if (response != null)
            {
                return Json(response);
            }
            else
            {
                return Json("Data not found.");
            }
        }

        [Route("api/Quote/SaveContractor")]
        [HttpPost]
        [HttpPatch]
        public IHttpActionResult SaveContractor(QuotationContractorEntity Contractor)
        {
            var response = _QuotationContext.SaveContractor(Contractor);
            if (response != null)
            {
                return Json(response);
            }
            else
            {
                return Json("Data not found.");
            }
        }


        [Route("api/Quote/SaveOther")]
        [HttpPost]
        public IHttpActionResult SaveOther(QuotationOtherEntity Other)
        {
            var response = _QuotationContext.SaveOther(Other);
            if (response != null)
            {
                return Json(response);
            }
            else
            {
                return Json("Data not found.");
            }
        }
    }
}
