﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Airmaster.Quotation.BusinessEntities;
using Airmaster.Quotation.BusinessServices;

namespace Airmaster.Quotation.API.Controllers
{
    public class CustomerController : ApiController
    {
        private readonly CustomerManagement _CustomerData;

        public CustomerController()
        {
            _CustomerData = new CustomerManagement();
        }

        [Route("api/Customer/Add")]
        [HttpPost]

        public IHttpActionResult AddCustomer(CustomerEntity Customer)
        {
            var response = _CustomerData.Save(Customer);
            if (response != null)
            {
                return Json(response);
            }
            else
            {
                return Json("Data not found.");
            }
        }

        [Route("api/Customer/Update")]
        [HttpPost]
        public IHttpActionResult UpdateCustomer(CustomerEntity Customer)
        {
            var response = _CustomerData.Update(Customer);
            if (response != null)
            {
                return Json(response);
            }
            else
            {
                return Json("Data not found.");
            }
        }
    }
}