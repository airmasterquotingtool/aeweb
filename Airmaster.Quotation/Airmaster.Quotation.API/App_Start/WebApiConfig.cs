﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using System.Web.Http;
using Airmaster.Quotation.BusinessEntities.Common;

namespace Airmaster.Quotation.API
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services

            // Web API routes
            config.MapHttpAttributeRoutes();

            //Mapper.Initialize(mapconfig=> {
            //    mapconfig.CreateMap<GetCodeDataResponse, CodeDataEntity>();
            //    mapconfig.CreateMap<GetDivisionResponse, DivisionResponseEntity>()
            //            .ForMember(s => s.Divisions, c => c.MapFrom(m => m.Divisions));
            //    mapconfig.CreateMap<Division, DivisionEntity>();
            //    mapconfig.CreateMap<GetVendorResponse, VendorEntity>();
            //});

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
        }
    }
}
