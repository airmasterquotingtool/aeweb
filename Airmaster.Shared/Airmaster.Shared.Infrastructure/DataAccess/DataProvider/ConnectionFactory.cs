﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Data;
using System.Data.Common;

namespace Airmaster.Shared.Infrastructure.DataAccess.DataProvider
{
    public interface IConnectionFactory
    {
        IDbConnection GetConnection { get;}
    }
    
    public class ConnectionFactory : IConnectionFactory
    {
        private readonly string connectionString;

        public ConnectionFactory(string Connection)
        {
            connectionString= ConfigurationManager.ConnectionStrings[Connection].ConnectionString;
        }

        public IDbConnection GetConnection
        {
            get
            {
                var factory = DbProviderFactories.GetFactory("System.Data.SqlClient");
                var conn = factory.CreateConnection();
                conn.ConnectionString = connectionString;
                conn.Open();
                return conn;
            }
        }
    }
}